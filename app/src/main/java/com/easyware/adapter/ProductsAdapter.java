package com.easyware.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easyware.activity.ProductDetail;
import com.easyware.bean.ProductBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.service.Webservice;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyware.activity.ProductList.listpress;


/**
 * Created by user on 7/10/2017.
 */

public class ProductsAdapter extends BaseAdapter implements Filterable {
    private List<ProductBean> stringList;
    private Context context;
    private LayoutInflater inflater;
    String img_url;
    private List<ProductBean> plist;
    ValueFilter valueFilter;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;

    public ProductsAdapter(List<ProductBean> proGridBean, Context activity) {
        this.stringList = proGridBean;
        this.plist = proGridBean;
        this.context = activity;
        detector = new ConnectionDetector(context);
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.inflater = LayoutInflater.from(context);

        manager = new UserSessionManager(context);
        map = manager.getUserDetail();

    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        /*if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_layout, null);
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.wishlist = (ImageView) convertView.findViewById(R.id.wishlist);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (stringList.get(position) != null) {
            holder.pName.setText(stringList.get(position).getPtname());
            holder.pimage.setImageResource(stringList.get(position).getPtimage());
        }
        if (ProductActivity.listpress){
            Log.e("gride view",""+ProductActivity.listpress);
        }else {
            Log.e("listview",""+ProductActivity.listpress);
        }*/
        if (convertView == null) {
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (listpress) {
            //Log.e("gride view", "" + listpress);
            convertView = this.inflater.inflate(R.layout.list_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);
            convertView.setTag(holder);
        } else if (listpress == true) {
            //Log.e("listview", "" + listpress);
            convertView = this.inflater.inflate(R.layout.grid_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            convertView.setTag(holder);
        } else {
            //Log.e("listview", "" + listpress);
            convertView = this.inflater.inflate(R.layout.grid_layout, null);
            holder = new ViewHolder();
            holder.pName = (TextView) convertView.findViewById(R.id.tv1);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.pimage = (ImageView) convertView.findViewById(R.id.photoView);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);
            convertView.setTag(holder);
        }


        holder.pName.setText((stringList.get(position).getTitle()));
        holder.price.setText(map.get(UserSessionManager.KEY_Curreny) + stringList.get(position).getPrice());
        if (stringList.get(position).getImage().equals("") || stringList.get(position).getImage().equals("null") || stringList.get(position).getImage() == null) {
            Glide.with(context).load(R.drawable.not_available)
                    .thumbnail(0.5f)
                    .into(holder.pimage);
        } else {
            Glide.with(context).load(Webservice.img_path + stringList.get(position).getImage())
                    .thumbnail(0.5f)
                    .into(holder.pimage);
        }
        //holder.pimage.setImageResource(stringList.get(position).getPtimage());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detector.isConnectingToInternet()) {
                    String newimag = "";
                    if (stringList.get(position).getImage().equals("") || stringList.get(position).getImage().equals("na") || stringList.get(position).getImage().equals("null")) {
                        newimag = "null";
                    } else {
                        newimag = Webservice.img_path + stringList.get(position).getImage();
                    }
                    //Intent intent = new Intent(context, Test.class);
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("pname", stringList.get(position).getTitle());
                    intent.putExtra("product_id", stringList.get(position).getId());
                    intent.putExtra("imgurl", newimag);
                    intent.putExtra("qty", stringList.get(position).getQty());
                    intent.putExtra("price", stringList.get(position).getPrice());
                    intent.putExtra("description", stringList.get(position).getDescription());
                    intent.putExtra("video", stringList.get(position).getVideo());
                    context.startActivity(intent);
                } else {
                    Webservice.MakeToast("Please Connect your Device to Internet", context);
                }
            }
        });

        return convertView;
    }


    private static class ViewHolder {
        TextView pName, qty, price;
        ImageView pimage, wishlist;
        RelativeLayout container;
    }


    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                //Log.e("SearchFilter", " if");
                List<ProductBean> filterList = new ArrayList<ProductBean>();
                for (int i = 0; i < plist.size(); i++) {
                    //Log.e("size",""+plist.size());
                    if (plist.get(i).getTitle().toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                        ProductBean bean = new ProductBean();
                        bean.setId(plist.get(i).getId());
                        bean.setTitle(plist.get(i).getTitle());
                        bean.setDescription(plist.get(i).getDescription());
                        bean.setType(plist.get(i).getType());
                        //bean.setStatus(plist.get(i).getStatus());
                        bean.setCreated(plist.get(i).getCreated());
                        bean.setCategory_id(plist.get(i).getCategory_id());
                        bean.setProd_code(plist.get(i).getProd_code());
                        bean.setPrice(plist.get(i).getPrice());
                        bean.setProm_price(plist.get(i).getProm_price());
                        bean.setQty(plist.get(i).getQty());
                        bean.setImage(plist.get(i).getImage());
                        bean.setVideo(plist.get(i).getVideo());
                        filterList.add(bean);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = plist.size();
                results.values = plist;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //  plist1.clear();
            stringList = (ArrayList<ProductBean>) results.values;
           // Log.e("SearchFilter", stringList.size() + " size ");
            notifyDataSetChanged();
        }
    }

}
