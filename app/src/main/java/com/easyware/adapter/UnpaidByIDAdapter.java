package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.UnpaidByIDBean;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class UnpaidByIDAdapter extends RecyclerView.Adapter<UnpaidByIDAdapter.ViewHolder>{

    private List<UnpaidByIDBean> unpaidByidList;
    private Context context;
    UserSessionManager manager;
    HashMap<String,String> map;


    public UnpaidByIDAdapter(List<UnpaidByIDBean> unpaidByidList, Context context) {
        this.unpaidByidList = unpaidByidList;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();

    }

    @Override
    public UnpaidByIDAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.unpaid_by_id, parent, false);
        return new UnpaidByIDAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if(unpaidByidList.get(position).getProd_name()!=null)
        {
            holder.name.setText(unpaidByidList.get(position).getProd_name());
        }
        if(unpaidByidList.get(position).getProd_price()!=null)
        {
            holder.price.setText(map.get(UserSessionManager.KEY_Curreny)+unpaidByidList.get(position).getProd_price());
        }
        if(unpaidByidList.get(position).getQty()!=null)
        {
            holder.qty.setText("Quantity : "+unpaidByidList.get(position).getQty());
        }
        if(unpaidByidList.get(position).getBrand()!=null)
        {
            holder.brand.setText("Brand : "+unpaidByidList.get(position).getBrand());
        }else {holder.brand.setVisibility(View.GONE);}
        if(unpaidByidList.get(position).getBeautician()!=null)
        {
            holder.beautician.setText("Beautician : "+unpaidByidList.get(position).getBeautician());
        }







    }


    @Override
    public int getItemCount() {
        return unpaidByidList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name,price,qty,brand,beautician;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            price=(TextView)itemView.findViewById(R.id.price);
            qty=(TextView)itemView.findViewById(R.id.qty);
            brand=(TextView)itemView.findViewById(R.id.brand);
            beautician=(TextView)itemView.findViewById(R.id.beautician);
        }
    }
}
