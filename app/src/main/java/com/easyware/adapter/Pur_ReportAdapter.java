package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.Pur_Report_Bean;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class Pur_ReportAdapter extends RecyclerView.Adapter<Pur_ReportAdapter.ViewHolder>{

    private List<Pur_Report_Bean> pur_report_bean;
    private Context context;
    UserSessionManager manager;
    HashMap<String,String> map;


    public Pur_ReportAdapter(List<Pur_Report_Bean> pur_report_bean, Context context) {
        this.pur_report_bean = pur_report_bean;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();

    }

    @Override
    public Pur_ReportAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchase_report, parent, false);
        return new Pur_ReportAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.ttitle.setText(pur_report_bean.get(position).getTitle());
        holder.tstatus.setText(pur_report_bean.get(position).getPayment_status());
        holder.ttotal.setText(map.get(UserSessionManager.KEY_Curreny)+pur_report_bean.get(position).getPrice());
        holder.ttaxid.setText("Txn Id : "+pur_report_bean.get(position).getTxn_id());

    }


    @Override
    public int getItemCount() {
        return pur_report_bean.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView ttitle,tstatus,ttotal,ttaxid;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            ttitle=(TextView)itemView.findViewById(R.id.ttitle);
            tstatus=(TextView)itemView.findViewById(R.id.tstatus);
            ttotal=(TextView)itemView.findViewById(R.id.ttotal);
            ttaxid=(TextView)itemView.findViewById(R.id.ttaxid);
container= (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}
