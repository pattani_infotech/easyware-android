package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.bean.VouchersListBean;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.ViewHolder>{

    private List<VouchersListBean> historyList;
    private Context context;
    UserSessionManager manager;
    HashMap<String,String> map;

    public VoucherAdapter(List<VouchersListBean> historyList, Context context) {
        this.historyList = historyList;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();
    }

    @Override
    public VoucherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vouchers_layout, parent, false);
        return new VoucherAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.ttitle.setText(historyList.get(position).getTitle());
        holder.tredeem.setText(historyList.get(position).getRedeem());
        holder.tdisc.setText(historyList.get(position).getDescription());
        holder.tredeemdate.setText(historyList.get(position).getRedeem_date());

    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView ttitle,tdisc,tredeemdate,tredeem;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            ttitle=(TextView)itemView.findViewById(R.id.ttitle);
            tredeem=(TextView)itemView.findViewById(R.id.tredeem);
            tdisc=(TextView)itemView.findViewById(R.id.tdisc);
            tredeemdate=(TextView)itemView.findViewById(R.id.tredeemdate);

container= (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}
