package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.PointReportListBean;

import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class PointReportListAdapter extends RecyclerView.Adapter<PointReportListAdapter.ViewHolder>{

    private List<PointReportListBean> pointReportListBeen;
    private Context context;


    public PointReportListAdapter(List<PointReportListBean> pointReportListBeen, Context context) {
        this.pointReportListBeen = pointReportListBeen;
        this.context = context;

    }

    @Override
    public PointReportListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pointreportlist, parent, false);
        return new PointReportListAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tid.setText("Points : "+pointReportListBeen.get(position).getPoints());
        holder.tname.setText("Name : "+pointReportListBeen.get(position).getName());
        holder.tremark.setText("Remarks : "+pointReportListBeen.get(position).getRemarks());
        holder.tdate.setText("Date : "+pointReportListBeen.get(position).getCreated());

    }


    @Override
    public int getItemCount() {
        return pointReportListBeen.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tid,tname,tdate,tremark;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            tid=(TextView)itemView.findViewById(R.id.tid);
            tname=(TextView)itemView.findViewById(R.id.tname);
            tdate=(TextView)itemView.findViewById(R.id.tdate);
            tremark=(TextView)itemView.findViewById(R.id.tremark);
container= (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}
