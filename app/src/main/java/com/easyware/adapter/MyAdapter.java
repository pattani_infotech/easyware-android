package com.easyware.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easyware.bean.BannerBean;
import com.easyware.easyware.R;
import com.easyware.service.Webservice;

import java.util.List;

/**
 * Created by user on 24-Feb-18.
 */
public class MyAdapter extends PagerAdapter {
    private List<BannerBean> bannerBean;
    private LayoutInflater inflater;
    private Context context;
    public MyAdapter(Context mContext, List<BannerBean> bannerBean) {
        this.context = mContext;
        this.bannerBean=bannerBean;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
    @Override
    public int getCount() {
        return bannerBean.size();
    }
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);
        ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
        TextView title = (TextView) myImageLayout.findViewById(R.id.title);
        TextView disc = (TextView) myImageLayout.findViewById(R.id.disc);
        if (bannerBean.get(position).getImage().equals("")||bannerBean.get(position).getImage().equals("na")||bannerBean.get(position).getImage()==null){
            Glide.with(context).load(R.drawable.not_available)
                    .thumbnail(0.5f)
                    .into(myImage);
        }else {
            Glide.with(context).load(Webservice.img_path + bannerBean.get(position).getImage())
                    .thumbnail(0.5f)
                    .into(myImage);
        }
        title.setText(bannerBean.get(position).getTitle());
        disc.setText(bannerBean.get(position).getDescription());
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }
    @Override
    public Parcelable saveState() {
        return null;
    }
}
