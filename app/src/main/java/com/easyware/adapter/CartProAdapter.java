package com.easyware.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easyware.activity.ProductList;
import com.easyware.bean.CartProductBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.SharedPreference;
import com.easyware.easyware.UserSessionManager;
import com.easyware.fragment.Cart;
import com.easyware.service.Webservice;

import java.util.HashMap;
import java.util.List;

import static com.easyware.fragment.Cart.nodata;
import static com.easyware.service.Webservice.paypalAmount;

/**
 * Created by user on 7/10/2017.
 */

public class CartProAdapter extends BaseAdapter {
    private List<CartProductBean> stringList;
    private Context context;
    private LayoutInflater inflater;
    String img_url;
    SharedPreference sharedPreference;
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    //LCManager lcManager;
    //HashMap<String,String>lcmap;

    public CartProAdapter(List<CartProductBean> cartProBeen, Context activity) {
        stringList = cartProBeen;
        this.context = activity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();
        sharedPreference = new SharedPreference(context);
        //lcManager=new LCManager(context);
        //lcmap=lcManager.getLC();
    }


    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_add_cart, null);
            holder.pname = (TextView) convertView.findViewById(R.id.pname);
            holder.qty = (TextView) convertView.findViewById(R.id.qty);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.totalprice = (TextView) convertView.findViewById(R.id.totalprice);
            holder.pimg = (ImageView) convertView.findViewById(R.id.pimg);
            holder.minus = (ImageView) convertView.findViewById(R.id.minus);
            holder.plus = (ImageView) convertView.findViewById(R.id.plus);
            holder.remove = (Button) convertView.findViewById(R.id.remove);
            holder.container = (RelativeLayout) convertView.findViewById(R.id.container);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (stringList.get(position) != null) {
            /*try {
                JSONArray namearray = new JSONArray(stringList.get(position).getName());
                JSONObject o = namearray.getJSONObject(0);
                holder.pname.setText(o.getString("value"));
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            holder.pname.setText(stringList.get(position).getName());

            holder.price.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(Double.parseDouble(stringList.get(position).getPrice()))));
            holder.qty.setText(stringList.get(position).getQty());
            //img_url = WebService.image_url + stringList.get(position).getId() + "/" + stringList.get(position).getId_default_image() + "&ws_key=" + WebService.api_key;
            if (stringList.get(position).getId_default_image().equals("") || stringList.get(position).getId_default_image().equals("null") || stringList.get(position).getId_default_image() == null) {
                Glide.with(context).load(R.drawable.not_available)
                        .thumbnail(0.5f)
                        .into(holder.pimg);
            } else {
                Glide.with(context).load(stringList.get(position).getId_default_image())
                        .thumbnail(0.5f)
                        .into(holder.pimg);
            }
        }

        holder.totalprice.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(ByDefaultcosting(stringList.get(position).getPrice(), stringList.get(position).getQty()))));

        NewTotalAmount(Integer.parseInt(holder.qty.getText().toString()));
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.qty.getText().toString());
                int tqty = qty + 1;
                stringList.get(position).setQty(String.valueOf(tqty));
                holder.qty.setText("" + tqty);
               // Log.e("plus",holder.qty.getText().toString()+"");
                sharedPreference.SaveLocalStore(context,stringList);
                notifyDataSetChanged();
                holder.totalprice.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(ByDefaultcosting(stringList.get(position).getPrice(), holder.qty.getText().toString()))));
            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.qty.getText().toString());
                if (qty < 2) {
                } else {
                    int tqty = qty - 1;
                    stringList.get(position).setQty(String.valueOf(tqty));
                    holder.qty.setText("" + tqty);
                    //Log.e("minus",holder.qty.getText().toString()+"");
                    sharedPreference.SaveLocalStore(context,stringList);
                    notifyDataSetChanged();
                    holder.totalprice.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(ByDefaultcosting(stringList.get(position).getPrice(), holder.qty.getText().toString()))));
                }
            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("Remove Item");
                alertDialog.setMessage("Are you sure you want to Remove it?");
                alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        stringList.remove(stringList.get(position));
                        sharedPreference.SaveLocalStore(context,stringList);
                        notifyDataSetChanged();
                        Webservice.MakeToast( "Item Remove Sucessfully",context);
                        if (stringList.size() == 0) {
                            Cart.ordertotal.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
                            Cart.cancel.setText("Your cart is empty...");
                            Cart.confirm.setVisibility(View.GONE);
                            nodata.setVisibility(View.VISIBLE);
                            Cart.withshipping.setVisibility(View.GONE);
                            Cart.gst_layout.setVisibility(View.GONE);
                            Cart.otnew1.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                ProductList.counttext.setBackgroundColor(ContextCompat.getColor(context,R.color.trans));
                            }else {
                                ProductList.counttext.setBackgroundColor(ContextCompat.getColor(context,R.color.trans));
                            }
                            //ProductList.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans,null));
                            ProductList.counttext.setText("");
                            //paypalAmount="0.00";
                        } else {
                            Cart.cancel.setText("cancel Order");
                            Cart.confirm.setVisibility(View.VISIBLE);
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                ProductList.counttext.setBackground(context.getResources().getDrawable(R.drawable.roundc,null));
                            }else {
                                ProductList.counttext.setBackground(ContextCompat.getDrawable(context,R.drawable.roundc));
                            }
                            //ProductList.counttext.setBackground(context.getResources().getDrawable(R.drawable.roundc,null));
                            ProductList.counttext.setText(""+stringList.size());
                        }

                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });
        return convertView;
    }

    private void NewTotalAmount(int qty) {
        double ttt = 0;
        double sip = 15;
        for (int i = 0; i < stringList.size(); i++) {
            ttt = ttt + (ByDefaultcosting(stringList.get(i).getPrice(), stringList.get(i).getQty()));
        }

         //gstCalculation(String.valueOf(ttt),map.get(UserSessionManager.KEY_GST).toString());
        //Cart.ordertotal.setText(map.get(UserSessionManager.KEY_Curreny) + (Webservice.GetDecimalFormate(String.valueOf(ttt))));
        Cart.gstcost.setText(map.get(UserSessionManager.KEY_Curreny)+Webservice.GetDecimalFormate(String.valueOf(gstCalculation(String.valueOf(ttt),map.get(UserSessionManager.KEY_GST).toString()))));
        Cart.ordertotal.setText(map.get(UserSessionManager.KEY_Curreny) + (Webservice.GetDecimalFormate(String.valueOf(ttt + gstCalculation(String.valueOf(ttt),map.get(UserSessionManager.KEY_GST).toString())))));
        Cart.ordertotal1.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(ttt)));
        if (map.get(UserSessionManager.KEY_GSTFORMAT).toString().equalsIgnoreCase("inc")) {
            Cart.otnew1.setText(map.get(UserSessionManager.KEY_Curreny) + (Webservice.GetDecimalFormate(String.valueOf(ttt))));
        }else {
            Cart.otnew1.setText(map.get(UserSessionManager.KEY_Curreny) + (Webservice.GetDecimalFormate(String.valueOf(ttt + gstCalculation(String.valueOf(ttt), map.get(UserSessionManager.KEY_GST).toString())))));
        }
        Cart.gstratec.setText("SR"+" "+map.get(UserSessionManager.KEY_GST).toString()+"%");
        //Cart.amountc.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(ttt)));
        Cart.amountc.setText(map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(String.valueOf(ttt-gstCalculation(String.valueOf(ttt),map.get(UserSessionManager.KEY_GST).toString()))));
        Cart.taxc.setText(map.get(UserSessionManager.KEY_Curreny)+Webservice.GetDecimalFormate(String.valueOf(gstCalculation(String.valueOf(ttt),map.get(UserSessionManager.KEY_GST).toString()))));

        if(map.get(UserSessionManager.KEY_GSTFORMAT).toString().equalsIgnoreCase("inc"))
        {
            paypalAmount=(Webservice.GetDecimalFormate(String.valueOf(ttt)));
        }else {
            paypalAmount = (Webservice.GetDecimalFormate(String.valueOf(ttt + gstCalculation(String.valueOf(ttt), map.get(UserSessionManager.KEY_GST).toString()))));
        }
    }

    private double gstCalculation(String ttt, String gst) {
        double Itotal;
        Itotal=(Double.parseDouble(ttt)*Double.parseDouble(gst))/100;
        return Itotal;
    }

    /*private double DecrementCosting(String price, String qty) {
        double Iprice, Iqty, Itotal;
        Iprice = Double.parseDouble(price);
        Iqty = Integer.parseInt(qty);
        Itotal = Iprice * Iqty;
        return Itotal;
    }

    private double IncrementCosting(String price, String qty) {
        double Iprice, Iqty, Itotal;
        Iprice = Double.parseDouble(price);
        Iqty = Integer.parseInt(qty);
        Itotal = Iprice * Iqty;
        return Itotal;
    }*/

    public double ByDefaultcosting(String price, String qty) {
        double Iprice, Iqty, Itotal;
        Iprice = Double.parseDouble(price);
        Iqty = Integer.parseInt(qty);
        Itotal = Iprice * Iqty;
        return Itotal;
    }

    public void Remove() {
        stringList.removeAll(stringList);
        sharedPreference.SaveLocalStore(context,stringList);
        notifyDataSetChanged();
        //WebService.CountAdapter(stringList.size(), context);
        if (stringList.size() == 0) {
            Cart.ordertotal.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
            Cart.cancel.setText("Your cart is empty...");
            Cart.confirm.setVisibility(View.GONE);
            Cart.nodata.setVisibility(View.VISIBLE);
            Cart.withshipping.setVisibility(View.GONE);
            Cart.gst_layout.setVisibility(View.GONE);

            Cart.otnew1.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //ProductList.counttext.setBackgroundColor(context.getResources().getColor(R.color.trans,null));
                ProductList.counttext.setBackgroundColor(ContextCompat.getColor(context,R.color.trans));
            }else {
                ProductList.counttext.setBackgroundColor(ContextCompat.getColor(context,R.color.trans));
            }

            ProductList.counttext.setText("");
            //paypalAmount="0.00";
        } else {
            Cart.cancel.setText("cancel Order");
            Cart.confirm.setVisibility(View.VISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ProductList.counttext.setBackground(context.getResources().getDrawable(R.drawable.roundc,null));
            }else {
                ProductList.counttext.setBackground(ContextCompat.getDrawable(context,R.drawable.roundc));
            }

            ProductList.counttext.setText(""+stringList.size());
        }
    }

    private static class ViewHolder {
        RelativeLayout container;
        TextView pname, qty, price, totalprice;
        ImageView pimg, minus, plus;
        Button remove;
    }



}
