package com.easyware.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.activity.ClaimsList;
import com.easyware.activity.UnpaidByID;
import com.easyware.bean.ClaimsBean;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class ClaimsListAdapter extends RecyclerView.Adapter<ClaimsListAdapter.ViewHolder>{
    UserSessionManager manager;
    HashMap<String,String> map;
    private List<ClaimsBean> claimsBean;
    private Context context;


    public ClaimsListAdapter(List<ClaimsBean> claimsBean, Context context) {
        this.claimsBean = claimsBean;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();
    }

    @Override
    public ClaimsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.claim_list, parent, false);
        return new ClaimsListAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.ttitle.setText(claimsBean.get(position).getRec_no());
        if(claimsBean.get(position).getTotal()!=null)
        {
            holder.ttotal.setText(map.get(UserSessionManager.KEY_Curreny)+claimsBean.get(position).getTotal());
        }else{
            holder.ttotal.setText(claimsBean.get(position).getDate());
        }


        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent=new Intent(context, UnpaidByID.class);
                    intent.putExtra("id",claimsBean.get(position).getId());
                    intent.putExtra("claim",ClaimsList.activity_title);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return claimsBean.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView ttitle,ttotal;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            ttitle=(TextView)itemView.findViewById(R.id.ttitle);
            ttotal=(TextView)itemView.findViewById(R.id.ttotal);
container= (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}
