package com.easyware.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.easyware.easyware.R;
import com.easyware.activity.PromotionDetail;
import com.easyware.bean.PromotionBean;
import com.easyware.service.Webservice;

import java.util.List;



/**
 * Created by user on 7/22/2017.
 */

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.ViewHolder>{

    private List<PromotionBean> prmo_list;
    private Context context;


    public PromotionAdapter(List<PromotionBean> prmo_list, Context context) {
        this.prmo_list = prmo_list;
        this.context = context;

    }

    @Override
    public PromotionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promotion_layout, parent, false);
        return new PromotionAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.promo_title.setText(prmo_list.get(position).getTitle());



            Glide.with(context).load(Webservice.img_path + prmo_list.get(position).getPromotion_img())
                    .placeholder(R.drawable.pronoimg).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    holder.pbar.setVisibility(View.VISIBLE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    holder.pbar.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.promo_img);




        if(prmo_list.get(position).getDescription().equals( "null" )) {
            holder.promo_desc.setText("");
        }
        else {
            holder.promo_desc.setText(prmo_list.get(position).getDescription());
        }

        holder.promo_created.setText(prmo_list.get(position).getCreated());

        holder.prmo_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent( context, PromotionDetail.class );
                Bundle bundle=new Bundle(  );
                bundle.putString( "id",prmo_list.get( position ).getPromotion_id() );
                bundle.putString( "title",prmo_list.get( position ).getTitle() );
                bundle.putString( "des",prmo_list.get( position ).getDescription() );
                bundle.putString( "date",prmo_list.get( position ).getCreated() );
                bundle.putString( "image",Webservice.img_path+prmo_list.get(position).getPromotion_img());
                intent.putExtras( bundle );
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                context.startActivity(intent);
            }
        });


    }



    @Override
    public int getItemCount() {
        return prmo_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView promo_title,promo_desc,promo_created;
        private final ImageView promo_img;
        private final RelativeLayout prmo_container;
        private final ProgressBar pbar;

        public ViewHolder(View itemView) {
            super(itemView);
            promo_title=(TextView)itemView.findViewById(R.id.promo_title);
            promo_desc=(TextView)itemView.findViewById(R.id.promo_desc);
            promo_created=(TextView)itemView.findViewById(R.id.promo_datetime);
            promo_img=(ImageView)itemView.findViewById(R.id.promo_img);
            prmo_container=(RelativeLayout)itemView.findViewById(R.id.prmo_container);
            pbar = (ProgressBar)itemView.findViewById(R.id.pbar);

        }
    }
}
