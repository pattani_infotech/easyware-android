package com.easyware.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.VideoBean;
import com.easyware.service.Webservice;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeThumbnailLoader;

import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private List<VideoBean> videoBean;
    private Context context;


    public VideoAdapter(List<VideoBean> videoBean, Context context) {
        this.videoBean = videoBean;
        this.context = context;

    }

    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videolayout, parent, false);
        return new VideoAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Bitmap bmThumbnail;
        holder.title.setText(videoBean.get(position).getTitle());
        holder.desc.setText(videoBean.get(position).getDescription());

        holder.onInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                youTubePlayer.loadVideo(videoBean.get(position).getUrl());
                youTubePlayer.play();
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
            }
        };
        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.youTubePlayerView.initialize(Webservice.API_KEY,holder.onInitializedListener);
            }
        });
    }


    @Override
    public int getItemCount() {
        return videoBean.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title, desc;
        ImageView Thumbnail;
        private final RelativeLayout prmo_container;
        YouTubePlayerView youTubePlayerView;
        ImageButton play;
        private YouTubeThumbnailLoader youTubeThumbnailLoader;
        YouTubePlayer.OnInitializedListener onInitializedListener;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            desc = (TextView) itemView.findViewById(R.id.desc);
            Thumbnail = (ImageView) itemView.findViewById(R.id.Thumbnail);
            play= (ImageButton) itemView.findViewById(R.id.play);
            youTubePlayerView = (YouTubePlayerView) itemView.findViewById(R.id.youtube_player);
            prmo_container = (RelativeLayout) itemView.findViewById(R.id.prmo_container);
        }
    }
}
