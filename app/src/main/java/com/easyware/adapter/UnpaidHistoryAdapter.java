package com.easyware.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.activity.UnpaidByID;
import com.easyware.bean.UnpaidHistoryBean;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class UnpaidHistoryAdapter extends RecyclerView.Adapter<UnpaidHistoryAdapter.ViewHolder>{

    private List<UnpaidHistoryBean> historyList;
    private Context context;
    UserSessionManager manager;
    HashMap<String,String> map;

    public UnpaidHistoryAdapter(List<UnpaidHistoryBean> historyList, Context context) {
        this.historyList = historyList;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();
    }

    @Override
    public UnpaidHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.unpaid_history_layout, parent, false);
        return new UnpaidHistoryAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.ttitle.setText(historyList.get(position).getUnpaid_title());
        holder.ttotal.setText(map.get(UserSessionManager.KEY_Curreny)+historyList.get(position).getUnpaid_total());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, UnpaidByID.class);
                intent.putExtra("id",historyList.get(position).getUnpaid_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tno,ttitle,tdis,ttotal;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            ttitle=(TextView)itemView.findViewById(R.id.ttitle);
            ttotal=(TextView)itemView.findViewById(R.id.ttotal);
container= (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}
