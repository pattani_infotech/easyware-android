package com.easyware.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.activity.UnpaidByID;
import com.easyware.bean.PurchaseHistoryBean;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class PurchaseHistoryAdapter extends RecyclerView.Adapter<PurchaseHistoryAdapter.ViewHolder>{

    private List<PurchaseHistoryBean> historyList;
    private Context context;
    UserSessionManager manager;
    HashMap<String,String> map;

    public PurchaseHistoryAdapter(List<PurchaseHistoryBean> historyList, Context context) {
        this.historyList = historyList;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();

    }

    @Override
    public PurchaseHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchase_history_layout, parent, false);
        return new PurchaseHistoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if(historyList.get(position).getName()!=null)
        {
            holder.rec_no.setText(historyList.get(position).getRec_no());
            holder.branc.setText("Branch : "+historyList.get(position).getName());
        }else{
            holder.branc.setVisibility(View.GONE);
            holder.rec_no.setText(historyList.get(position).getPur_title());
            holder.rec_no.setTextColor(context.getResources().getColor(R.color.libhtblue));
        }


        if(historyList.get(position).getTotal()!=null)
        {
            holder.ttotal.setText(map.get(UserSessionManager.KEY_Curreny)+historyList.get(position).getTotal());
        }else{
            holder.ttotal.setText(historyList.get(position).getRec_no());
            holder.ttotal.setTextColor(context.getResources().getColor(R.color.black));
        }

        holder.pur_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, UnpaidByID.class);
                intent.putExtra("id",historyList.get(position).getPur_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView branc,rec_no,ttotal;
        private final RelativeLayout pur_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            branc=(TextView)itemView.findViewById(R.id.branc);
            rec_no=(TextView)itemView.findViewById(R.id.rec_no);
            ttotal=(TextView)itemView.findViewById(R.id.ttotal);
            pur_layout=(RelativeLayout)itemView.findViewById(R.id.pur_layout);
        }
    }
}
