package com.easyware.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.easyware.easyware.R;
import com.easyware.bean.HappyHoursBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class HappyHoursAdapter extends RecyclerView.Adapter<HappyHoursAdapter.ViewHolder> {
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String,String> map;
    private List<HappyHoursBean> happyHoursBean;
    private Context context;
    private Handler handler;
    private Runnable runnable;
    String datef = null;
    Date date1 = null;
    String datenew = null;
    Date date2 = null;

    public HappyHoursAdapter(List<HappyHoursBean> happyHoursBean, Context context) {
        this.happyHoursBean = happyHoursBean;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();
    }

    @Override
    public HappyHoursAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.happyhours, parent, false);
        return new HappyHoursAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        String currentdate = null;
        final String companyid = "20";
        holder.title.setText(happyHoursBean.get(position).getTitle().toString().replace("&#039;s", "'s "));


        Glide.with(context).load(Webservice.img_path + happyHoursBean.get(position).getImage())
                .placeholder(R.drawable.pronoimg).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.pbr.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.pbr.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.img);


        SimpleDateFormat mSDF = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat formattercurrent = new SimpleDateFormat("yyyy-MM-dd");
        try {
            datef = mSDF.format(formatter.parse(happyHoursBean.get(position).getExpiry_date()));
            datenew = formattercurrent.format(formatter.parse(happyHoursBean.get(position).getExpiry_date()));
            date1 = mSDF.parse(datef);

            currentdate = mSDF.format(formatter.parse(Webservice.ChatCurrentDate()));
            date2 = formatter.parse(Webservice.ChatCurrentDate());

           /* Log.e("date1 ",""+date1);
            Log.e("date2 ",""+date2);
            Log.e("dateformate 1", "" +formatter.parse(Webservice.ChatCurrentDate()));*/
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date1.compareTo(date2) < 0) {
            holder.countdown.setText("Timer : ");
        } else {
            long startDate = date1.getTime();
            long currentdatetime = date2.getTime();
            long diff = 0;
            long currentTimeinLong= Calendar.getInstance().getTimeInMillis();
            long newdiff=(long)(date1.getTime()-currentTimeinLong);

            if (newdiff > 0) {
                new CountDownTimer(newdiff, 1000) {
                    public void onTick(long millisUntilFinished) {
                       /*int seconds = (int) (leftTimeInMilliseconds / 1000) % 60;
                        int minutes = (int) ((leftTimeInMilliseconds / (1000 * 60)) % 60);
                        int hours = (int) ((leftTimeInMilliseconds / (1000 * 60 * 60)) % 24);
                        int days = (int) ((leftTimeInMilliseconds / (1000 * 60 * 60 * 24)));
                       *//* Log.e("hours", "" + ((millisUntilFinished / (1000 * 60 * 60)) % 24));
                        Log.e("day", "" + ((millisUntilFinished / (1000 * 60 * 60 * 24))));*//*
                        *//*long days= TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.DAYS.toMillis(days);
                        long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);
                        long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                        millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);*//*
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd hh:mm:ss");
                        Date date = new Date(leftTimeInMilliseconds);
                        String dateText = simpleDateFormat.format(date);
                        Log.e("dateText",""+dateText);
                        Log.e("remainingday:",days + ":" + hours + ":" + minutes + ":" + seconds);
                        holder.countdown.setText(days + " days " + hours + " hrs " + minutes + " mins " + seconds + " sec");*/
                        //holder.countdown.setText("seconds remaining: " + millisUntilFinished / 1000);

                        long diffDay=millisUntilFinished/(24*60*60 * 1000);
                        millisUntilFinished=millisUntilFinished-(diffDay*24*60*60 * 1000); //will give you remaining milli seconds relating to hours,minutes and seconds
                        long diffHours=millisUntilFinished/(60*60 * 1000);
                        millisUntilFinished=millisUntilFinished-(diffHours*60*60 * 1000);
                        long diffMinutes = millisUntilFinished / (60 * 1000);
                        millisUntilFinished=millisUntilFinished-(diffMinutes*60*1000);
                        long diffSeconds = millisUntilFinished / 1000;
                        millisUntilFinished=millisUntilFinished-(diffSeconds*1000);

                        //Log.e("time","day-"+diffDay+"-hours-"+diffHours+"-minit-"+diffMinutes+"-second-"+diffSeconds);
                        holder.countdown.setText("Time : "+diffDay+"day "+diffHours+"hours "+diffMinutes+"minute "+diffSeconds+"seconds ");
                    }
                    public void onFinish() {
                        holder.countdown.setText("done!");
                    }
                }.start();
            } else {
                holder.countdown.setText("Expired!!");
            }
        }

        if (happyHoursBean.get(position).getDescription().equals("null")) {
            holder.desc.setText("");
        } else {
            holder.desc.setText(happyHoursBean.get(position).getDescription());
        }

        holder.price.setText(map.get(UserSessionManager.KEY_Curreny) + happyHoursBean.get(position).getPrice());
        holder.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (detector.isConnectingToInternet()) {
                    if (companyid.toString().equals("20")) {
                        Intent intent=new Intent(context,Order.class);
                        intent.putExtra("title",happyHoursBean.get(position).getTitle());
                        intent.putExtra("description",happyHoursBean.get(position).getDescription());
                        intent.putExtra("image",Webservice.img_path+happyHoursBean.get(position).getImage());
                        intent.putExtra("expiry_date",happyHoursBean.get(position).getExpiry_date());
                        intent.putExtra("price",happyHoursBean.get(position).getPrice());
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }else {
                       // Webservice.MakeToast("no match com id",context);
                        new GetBuy().execute(happyHoursBean.get(position).getId());
                    }
                }*/
                if (detector.isConnectingToInternet()) {
                    new GetBuy().execute(happyHoursBean.get(position).getId());
                }else {
                    Toast.makeText(context,"Please Connect your Device to Internet",Toast.LENGTH_SHORT).show();
                }
                //new GetBuy().execute(happyHoursBean.get(position).getId());
            }
        });

    }

    private HappyHoursBean getItem(int position) {
        return happyHoursBean.get(position);
    }

   /* private void countDownStart() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                    Date eventDate = dateFormat.parse(datenew);
                    Log.e("555555",""+eventDate);
                    Date currentDate = new Date();
                    if (!currentDate.after(eventDate)) {
                        long diff = eventDate.getTime()
                                - currentDate.getTime();
                        long days = diff / (24 * 60 * 60 * 1000);
                        diff -= days * (24 * 60 * 60 * 1000);
                        long hours = diff / (60 * 60 * 1000);
                        diff -= hours * (60 * 60 * 1000);
                        long minutes = diff / (60 * 1000);
                        diff -= minutes * (60 * 1000);
                        long seconds = diff / 1000;
                        *//*tvDay.setText("" + String.format("%02d", days));
                        tvHour.setText("" + String.format("%02d", hours));
                        tvMinute.setText("" + String.format("%02d", minutes));
                        tvSecond.setText("" + String.format("%02d", seconds));*//*
                       *//* Log.e("dd ",String.format("%02d", days));
                        Log.e("hh ",String.format("%02d", hours));
                        Log.e("mm ",String.format("%02d", minutes));
                        Log.e("ss ",String.format("%02d", seconds));*//*
                        Webservice.countdate="timer : "+String.format("%02d", days)+":"+String.format("%02d", hours)+":"+String.format("%02d", minutes)+":"+String.format("%02d", seconds);
                        Log.e("countdown :",Webservice.countdate);
                    } else {
                        handler.removeCallbacks(runnable);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(runnable, 0);
    }*/

    @Override
    public int getItemCount() {
        return happyHoursBean.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView title, desc, price, countdown;
        private final ImageView img;
        private final ProgressBar pbr;
        Button buy;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            desc = (TextView) itemView.findViewById(R.id.desc);
            price = (TextView) itemView.findViewById(R.id.price);
            countdown = (TextView) itemView.findViewById(R.id.countdown);
            img = (ImageView) itemView.findViewById(R.id.img);
            buy = (Button) itemView.findViewById(R.id.buy);
            pbr = (ProgressBar) itemView.findViewById(R.id.pbr);
        }
    }

    private class GetBuy extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "id=" + params[0];
            responce = JsonParser.GetJsonFromURL("", Webservice.HappyHoursOrder + param);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("error");
                    if (data_code.equals("200")) {
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (responce != null) {
                if (data_code.equals("200")) {
                    Webservice.MakeToast("Successfull", context);
                } else {
                    Webservice.MakeToast("Not Successfull", context);
                }
            }
        }
    }
}

