package com.easyware.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.CommentBean;

import java.util.List;

import static com.easyware.service.Webservice.FormatDate;

/**
 * Created by user on 7/24/2017.
 */

public class CommentAdapter extends BaseAdapter {


    private final Context context;
    List<CommentBean> messages;
    private static LayoutInflater inflater=null;

    public CommentAdapter(List<CommentBean> messages, Context context) {

        this.context = context;
        this.messages = messages;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String newdate = null;
        ViewHolder holder = null;
        View vi=convertView;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.comment_layout, null);
            holder = new ViewHolder();
            holder.msg = (TextView) convertView.findViewById(R.id.comment);
            holder.commment_date = (TextView) convertView.findViewById(R.id.commment_date);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.msg.setText(messages.get( position ).getComment());

        holder.commment_date.setText(FormatDate(messages.get( position ).getCreated()));


        //Log.e("aaaa",""+messages.get( position ).getCreated());


        return convertView;

    }


    private class ViewHolder {
        private TextView msg,commment_date;


    }

}
