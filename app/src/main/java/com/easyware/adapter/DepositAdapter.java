package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.bean.DepositListBean;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class DepositAdapter extends RecyclerView.Adapter<DepositAdapter.ViewHolder>{

    private List<DepositListBean> historyList;
    private Context context;
    UserSessionManager manager;
    HashMap<String,String> map;

    public DepositAdapter(List<DepositListBean> historyList, Context context) {
        this.historyList = historyList;
        this.context = context;
        manager=new UserSessionManager(context);
        map=manager.getUserDetail();
    }

    @Override
    public DepositAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deposit_layout, parent, false);
        return new DepositAdapter.ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tdate.setText(historyList.get(position).getCreate());
        holder.tamount.setText(map.get(UserSessionManager.KEY_Curreny)+historyList.get(position).getAmount());

    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tdate,tamount;
LinearLayout container;
        public ViewHolder(View itemView) {
            super(itemView);
            tdate=(TextView)itemView.findViewById(R.id.tdate);
            tamount=(TextView)itemView.findViewById(R.id.tamount);
container= (LinearLayout) itemView.findViewById(R.id.container);
        }
    }
}
