package com.easyware.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.util.ChatBubble;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 7/24/2017.
 */

public class NewChatAdapter extends BaseAdapter {
    private Context context;
    List<ChatBubble> messages= new ArrayList<ChatBubble>();
    private LayoutInflater myInflater;

    public NewChatAdapter(List<ChatBubble> messages, Context context) {

        this.context = context;
        this.messages = messages;
        myInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (messages.get(position).getIsme()==true){
            convertView = myInflater.inflate(R.layout.right_chat_bubble, null);
            holder = new ViewHolder();
            holder.commentleft = (TextView) convertView.findViewById(R.id.txt_msg);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            convertView.setTag(holder);
        }else {
            convertView = myInflater.inflate(R.layout.left_chat_bubble, null);
            holder = new ViewHolder();
            holder.commentleft = (TextView) convertView.findViewById(R.id.txt_msg);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            convertView.setTag(holder);
        }
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = dateFormatter.parse(messages.get(position).getDateTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
// Get time from date
        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String displayValue = timeFormatter.format(date);
        String displaydate = dateFormat.format(date);

        holder.commentleft.setText(messages.get(position).getMessage());
        holder.time.setText(displaydate+","+displayValue);
        return convertView;
    }

    private class ViewHolder {
        private TextView commentleft,time;

    }
}
