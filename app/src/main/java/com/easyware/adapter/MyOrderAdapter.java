package com.easyware.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.activity.OrderDetail;
import com.easyware.bean.MyOrderBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.service.Webservice;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 16-Dec-17.
 */

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.ViewHolder> {

    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    private List<MyOrderBean> stringList;
    private List<MyOrderBean> plist;
    private Context context;


    public MyOrderAdapter(List<MyOrderBean> orderBeansList, Context context) {
        this.stringList = orderBeansList;
        this.plist = orderBeansList;
        this.context = context;
        detector = new ConnectionDetector(context);
        manager = new UserSessionManager(context);
        map = manager.getUserDetail();

    }

    @Override
    public MyOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_history_layout, parent, false);
        return new MyOrderAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        String Currency_code = map.get(UserSessionManager.KEY_Curreny);

        String order_no = "<b><font size=15 color=black>Order Number</font></b> :-<font color='#ff0000'> " + stringList.get(position).getOrder_no() + "</font>";
        String total_price;
        if (Currency_code != null) {
            total_price = "<b><font size=15 color=black>Payment</font></b> :-<b> " + map.get(UserSessionManager.KEY_Curreny) + Webservice.GetDecimalFormate(stringList.get(position).getTotal()) + "</b>";
        } else {
            total_price = "<b><font size=15 color=black>Payment</font></b> :-<b> " + Webservice.GetDecimalFormate(stringList.get(position).getTotal()) + "</b>";
        }
        String Order_date = "<b><font size=15 color=black>Order Date</font></b> :-<b> " + stringList.get(position).getDate() + "</b>";
        String status;
        if (stringList.get(position).getStatus().equals("0")){
             status="Pending";
        }else {
            status="Completed";
        }
        String Order_status = "<b><font size=15 color=black>Status</font></b> :-<b> " + status + "</b>";
        if (stringList.get(position).getPoint_used()==null||stringList.get(position).getPoint_used().equals("")||stringList.get(position).getPoint_used().equals("null")){
            holder.points.setVisibility(View.GONE);
        }else {
            holder.points.setVisibility(View.VISIBLE);
        }
        String points_used = "<b><font size=15 color=black>Used Points</font></b> :-<b> " + stringList.get(position).getPoint_used() + "</b>";
        if (Build.VERSION.SDK_INT >= 24) {
            holder.order_no.setText(Html.fromHtml(order_no, Html.FROM_HTML_MODE_LEGACY));
            holder.order_date.setText(Html.fromHtml(Order_date, Html.FROM_HTML_MODE_LEGACY));
            holder.order_date.setText(Html.fromHtml(Order_date, Html.FROM_HTML_MODE_LEGACY));
            holder.order_total_price.setText(Html.fromHtml(total_price, Html.FROM_HTML_MODE_LEGACY));
            holder.order_status.setText(Html.fromHtml(Order_status, Html.FROM_HTML_MODE_LEGACY));
            holder.points.setText(Html.fromHtml(points_used, Html.FROM_HTML_MODE_LEGACY));
            // for 24 api and more
        } else {
            holder.order_no.setText(Html.fromHtml(order_no));
            holder.order_date.setText(Html.fromHtml(Order_date));
            holder.order_total_price.setText(Html.fromHtml(total_price));
            holder.order_status.setText(Html.fromHtml(Order_status));
            holder.points.setText(Html.fromHtml(points_used));

        }
        holder.order_layoout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detector.isConnectingToInternet()) {
                    Intent intent = new Intent(context, OrderDetail.class);
                    intent.putExtra("orderno", stringList.get(position).getOrder_no());
                    intent.putExtra("data", stringList.get(position).getItem());
                    context.startActivity(intent);
                    //Log.e("data",stringList.get(position).getItem());
                } else {
                    Webservice.MakeToast(context.getString(R.string.check_internet),context);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView order_no, order_date,points, order_total_price, order_status;
        LinearLayout order_layoout;

        public ViewHolder(View itemView) {
            super(itemView);

            order_no = (TextView) itemView.findViewById(R.id.order_no);
            order_date = (TextView) itemView.findViewById(R.id.order_date);
            points = (TextView) itemView.findViewById(R.id.points);
            order_total_price = (TextView) itemView.findViewById(R.id.order_total_price);
            order_status = (TextView) itemView.findViewById(R.id.order_status);
            order_layoout = (LinearLayout) itemView.findViewById(R.id.order_layoout);
        }
    }
}
