package com.easyware.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyware.bean.ServiceHistoryBean;
import com.easyware.easyware.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 7/22/2017.
 */

public class SHAdapter extends BaseAdapter {
    ImageView image;
    Context context;
    LayoutInflater inflater;
    List<ServiceHistoryBean> oplist;
    String list1;
    int count;

    String dot, more;

    public SHAdapter(Context context, ArrayList<ServiceHistoryBean> arrayList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        oplist = arrayList;
        count = oplist.size();
        if (count >= 1) {
            count = oplist.size();
        } else {
            count = 1;
        }
    }

    public SHAdapter(List<ServiceHistoryBean> oplist, Context context) {

        this.context = context;
        this.oplist = oplist;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        count = oplist.size();

    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
int newone=0;
        ViewHolder holder = null;
        if (convertView == null) {

        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        holder = new ViewHolder();
        convertView = inflater.inflate(R.layout.service_historynew, null);
        holder.layout = (LinearLayout)convertView.findViewById(R.id.type2);
        //holder.rg = (RadioGroup) convertView.findViewById(R.id.rg);

        holder.name = (TextView) convertView.findViewById(R.id.name);
        holder.date = (TextView) convertView.findViewById(R.id.date);
        holder.usage = (TextView) convertView.findViewById(R.id.usage);
        holder.used = (TextView) convertView.findViewById(R.id.used);

        //Log.e("length",""+oplist.size());
        for (int k = 0; k < Integer.parseInt(oplist.get(position).getUsed()); k++) {
            //Log.e("newone-->", "" +oplist.get(position).getUsage());
            image = new ImageView(context);
            image.setLayoutParams(new android.view.ViewGroup.LayoutParams(35, 35));
            image.setMaxHeight(10);
            image.setMaxWidth(10);
            image.setImageResource(R.drawable.ic_checkedn);
            holder.layout.addView(image);
        }
        for (int k = 0; k < Integer.parseInt(oplist.get(position).getUsage())-Integer.parseInt(oplist.get(position).getUsed()); k++) {
            //Log.e("newone-->", "" +oplist.get(position).getUsage());
            image = new ImageView(context);
            image.setLayoutParams(new android.view.ViewGroup.LayoutParams(35, 35));
            image.setMaxHeight(10);
            image.setMaxWidth(10);
            image.setImageResource(R.drawable.ic_uncheck);
            holder.layout.addView(image);
        }

        convertView.setTag(holder);
        holder.name.setText(oplist.get(position).getTitle());
        holder.date.setText(oplist.get(position).getDate());
        holder.usage.setText("Total Usage : "+oplist.get(position).getUsage());
        holder.used.setText("Total Used : " + oplist.get(position).getUsed());
        holder.layout.setTag(position);



        return convertView;
    }

    public class ViewHolder {
        private LinearLayout layout;
        private TextView name, date, usage, used;
    }


}
