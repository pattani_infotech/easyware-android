package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.DayAppointmentBean;

import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class DayAppoinmentAdapter extends RecyclerView.Adapter<DayAppoinmentAdapter.ViewHolder>{

    private List<DayAppointmentBean> dayAppointmentBean;
    private Context context;


    public DayAppoinmentAdapter(List<DayAppointmentBean> dayAppointmentBean, Context context) {
        this.dayAppointmentBean = dayAppointmentBean;
        this.context = context;

    }

    @Override
    public DayAppoinmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.day_appoinment_layout, parent, false);
        return new DayAppoinmentAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.staff.setText("Beautician : "+dayAppointmentBean.get(position).getStaff());
        holder.notes.setText("Notes : "+dayAppointmentBean.get(position).getNotes().replace("\n",""));
        holder.date.setText(dayAppointmentBean.get(position).getDate());
        holder.time.setText("Duration :"+dayAppointmentBean.get(position).getTime());

    }
    @Override
    public int getItemCount() {
        return dayAppointmentBean.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView staff,date,time,notes;
        private final RelativeLayout container;

        public ViewHolder(View itemView) {
            super(itemView);
            staff=(TextView)itemView.findViewById(R.id.staff);
            date=(TextView)itemView.findViewById(R.id.date);
            time=(TextView)itemView.findViewById(R.id.time);
            notes=(TextView)itemView.findViewById(R.id.notes);

            container=(RelativeLayout)itemView.findViewById(R.id.container);
        }
    }
}
