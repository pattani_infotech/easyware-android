package com.easyware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.ServiceHistoryBean;

import java.util.List;

/**
 * Created by user on 7/22/2017.
 */

public class ServiceHistoryAdapter extends RecyclerView.Adapter<ServiceHistoryAdapter.ViewHolder>{

    private List<ServiceHistoryBean> claimlist;
    private Context context;


    public ServiceHistoryAdapter(List<ServiceHistoryBean> claimlist, Context context) {
        this.claimlist = claimlist;
        this.context = context;

    }

    @Override
    public ServiceHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_history, parent, false);
        return new ServiceHistoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        holder.name.setText(claimlist.get(position).getTitle());
        holder.date.setText(claimlist.get(position).getDate());
        holder.usage.setText("Total Usage : "+claimlist.get(position).getUsage());
        holder.used.setText("Total Used : "+claimlist.get(position).getUsed());

    }


    @Override
    public int getItemCount() {
        return claimlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name,date,usage,used;


        public ViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            date=(TextView)itemView.findViewById(R.id.date);
            usage=(TextView)itemView.findViewById(R.id.usage);
            used=(TextView)itemView.findViewById(R.id.used);
        }
    }
}
