package com.easyware.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.activity.AddNewAppointment;
import com.easyware.activity.Appointments;
import com.easyware.bean.AppointMentBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by user on 8/15/2017.
 */

public class AppointMentAdapter extends RecyclerView.Adapter<AppointMentAdapter.ViewHolder> {


    private List<AppointMentBean> appointment_list;
    private Context context;
    ConnectionDetector detector;

    public AppointMentAdapter(List<AppointMentBean> appointment_list,Context context)
    {
        this.appointment_list=appointment_list;
        this.context=context;
        detector=new ConnectionDetector(context);
    }
    @Override
    public AppointMentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_data_layout, parent, false);
        return new AppointMentAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.app_date.setText(appointment_list.get(position).getDate());
        holder.staff_name.setText(appointment_list.get(position).getStaff());
        holder.app_time.setText(appointment_list.get(position).getTime());
        holder.app_descrition.setText(appointment_list.get(position).getDescrption().replaceAll("&amp;","&"));
Webservice.staff_id=appointment_list.get(position).getStaffid();
        if(appointment_list.get(position).getStatus().equals("0")) {
            holder.app_status.setText("Pending");
            holder.cancel.setVisibility(View.VISIBLE);
            holder.edit.setVisibility(View.VISIBLE);
        }else if(appointment_list.get(position).getStatus().equals("1")) {
            holder.app_status.setText("Approved");
            holder.cancel.setVisibility(View.VISIBLE);
            holder.edit.setVisibility(View.VISIBLE);
        }else if(appointment_list.get(position).getStatus().equals("2")) {
            holder.app_status.setText("Canceled");
            holder.cancel.setVisibility(View.INVISIBLE);
            holder.edit.setVisibility(View.GONE);
        }
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Cancel Appointment");
                alert.setMessage("Are you sure you want to cancel appointment?");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String params="id="+appointment_list.get(position).getId();
                        if(detector.isConnectingToInternet())
                        {
                            new CancelAppoint().execute(params, Webservice.CancelAppoint);

                            holder.app_status.setText("Canceled");
                        }
                        dialog.dismiss();
                    }
                });

                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, AddNewAppointment.class);
                intent.putExtra("edit","edit");
                intent.putExtra("id",appointment_list.get(position).getId());
                intent.putExtra("date",appointment_list.get(position).getDate());
                intent.putExtra("time",appointment_list.get(position).getTime());
                intent.putExtra("note",appointment_list.get(position).getDescrption());
                intent.putExtra("staffName",appointment_list.get(position).getStaff());
                intent.putExtra("staffid",appointment_list.get(position).getStaffid());

               // Log.e("iddddd",appointment_list.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return appointment_list.size();
    }
    public void clear() {
        appointment_list.clear();
        notifyDataSetChanged();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView app_date,staff_name,app_time,app_descrition,app_status;
        private final Button cancel,edit;
        public ViewHolder(View itemView) {
            super(itemView);
            app_date=(TextView)itemView.findViewById(R.id.app_date);
            staff_name=(TextView)itemView.findViewById(R.id.staff_name);
            app_time=(TextView)itemView.findViewById(R.id.app_time);
            app_descrition=(TextView)itemView.findViewById(R.id.app_descrition);
            app_status=(TextView)itemView.findViewById(R.id.app_status);
            cancel=(Button)itemView.findViewById(R.id.cancel);
            edit=(Button)itemView.findViewById(R.id.edit);

        }
    }



    class CancelAppoint extends AsyncTask<String,String,String>
    {

        String  response;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(context);
            progressDialog.setMessage("Please wait");
            progressDialog.show();;
        }

        @Override
        protected String doInBackground(String... params) {
            response= JsonParser.GetJsonFromURL(params[0],params[1]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(response!=null)
            {
                try {
                    JSONObject object=new JSONObject(response);
                    if(object.getString("data_code").equals("200")) {
                        Webservice.MakeToast("Appointment Cancelled",context);
                        context.startActivity(new Intent(context, Appointments.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
