package com.easyware.service;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vimalkumar on 3/7/2017.
 */

public class Webservice {


    //public static String base_url="http://fitzroyasolutions.com/Vimal/api/";
    public static String base_url = "https://easyware.asia/mobile_api/";

    public static String login = base_url + "login.php?";
    public static String promotion = base_url + "promotion.php?";
    public static String promotion_detail = base_url + "promotion_details.php?";
    public static String img_path = "https://easyware.asia/uploads/";
    public static String message = base_url + "messages.php?";
    public static String SendMessaqe = base_url + "add_message.php";
    public static String AddComment = base_url + "promotion_comment.php";
    public static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    public static String UpdateProfile = base_url + "updateprofile.php?";
    public static String UpdateProfilePic = base_url + "Change_Profile_picture.php";
    public static String ChangePwd = base_url + "easyware_change_pwd.php";
    public static String GetCurrentMonthAppointMent = base_url + "appointment.php";
    public static String GetAppointmentBuyDate = base_url + "get_dayappointment.php";
    public static String CancelAppoint = base_url + "cancel_appointment.php";
    public static String GetStaff = base_url + "staff_list.php?";
    public static String unpaid_history = base_url + "purchase_unpaid.php?";
    public static String Add_Appointment = base_url + "add_appointment.php?";
    public static String Update_Appointment = base_url + "edit_appointment.php?";
    public static String unpaid_ByID = base_url + "purchase_detailsbyid.php?";
    public static String claims_details = base_url + "claims_details.php?";
    public static String course_claimsdetails = base_url + "course_claimsdetails.php?";

    public static String Service_history = base_url + "service_history.php?";
    public static String ClaimsList = base_url + "claims_list.php?";
    public static String course_ClaimsList = base_url + "course_claimslist.php?";
    public static String product_history = base_url + "product_history.php";
    public static String purchase_history = base_url + "purchase_history.php";
    public static String purchase_Report = base_url + "purchase_report.php?";
    public static String points_Report = base_url + "points_report.php?";
    public static String HappyHours = base_url + "happy_hours.php?";
    public static String HappyHoursOrder = base_url + "happyhrs_order.php?";
    public static String VideoList = base_url + "video.php?";
    public static String Appoinment = base_url + "home_dayapt.php?";
    public static String After_share_view = base_url + "share_viewinsert.php?";
    public static String Deposit_List = base_url + "get_deposit.php?";
    public static String Vouchers_List = base_url + "get_vouchers.php?";
    public static String Emall_Products = base_url + "emall_products.php?";
    public static String Emall_Brands = base_url + "emall_brands.php?";
    public static String Emall_Banner = base_url + "banner_list.php?";
    public static String Admin_Banner = base_url + "admin_banner.php?";
    public static String Emall_Geterate_Order = base_url + "emall_generate_order.php";
    public static String Emall_CheckOut = base_url + "emall_checkout.php";
    public static String Emall_User_Order = base_url + "emall_user_orders.php";
    public static String Check_existing_email = base_url + "check_existing_email.php";
    public static String Check_existing_username = base_url + "check_existing_username.php";
    public static String Check_branch_code = base_url + "check_branch_code.php";
    public static String Register = base_url + "register.php";
    public static String emall_brands = base_url + "emall_brands.php";

    public static String type ="username" ;
    public static String staff_id,newdate,countdate;
    public static String paypalAmount;
    public static String OrderId;
   public static final String API_KEY = "AIzaSyBTZOxUh678ZiWZ6MvbD-p94VJhvkVHAjs";
    //ARD-JHeO6BRTM1N5LVCnMSn5vjzV0ET6BvKKH0Dycm6J-2MFKmihezjLs37zXEkZhT3YA4XX1ShJtpsu
    //public static final String API_KEY = "ARD-JHeO6BRTM1N5LVCnMSn5vjzV0ET6BvKKH0Dycm6J-2MFKmihezjLs37zXEkZhT3YA4XX1ShJtpsu";
    public static final String YOUTUBE_VIDEO_CODE = "_oEA18Y8gM0";
    public static String gcm;
public static String currency;
    public static String comanid="";
    public final static String SENDER_ID="726211850693";
    public static String device_type="Android";
   // public static boolean listpress = false;
    public static void MakeToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String FormatDate(String date1) {
        DateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            newdate= mSDF.format(formatter.parse(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }

       return newdate;
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static String CurrentDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return simpleDateFormat.format(date);
    }
    public static String ChatCurrentDate() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss");
            Date date = new Date();
        return simpleDateFormat.format(date);
    }

    public static boolean checkPermission(Context context) {

        int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE);
        int result1 = ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION);
        int call = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE);
        int write = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && call == PackageManager.PERMISSION_GRANTED && write == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    public static String GetDecimalFormate(String data) {
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        String newdata = decimalFormat.format(Double.parseDouble(data));
        //String newdat=new DecimalFormat("#0.00").format(Double.parseDouble(data));
        return newdata;
    }
    public static boolean validateEmail(String email) {
        boolean isValid = false;
        //    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        String expression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (email.matches(expression)) {
            isValid = true;
        }
        return isValid;
    }
}
