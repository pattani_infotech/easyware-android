package com.easyware.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.easyware.adapter.CartProAdapter;
import com.easyware.bean.CartProductBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.SharedPreference;
import com.easyware.easyware.UserSessionManager;
import com.easyware.fragment.Cart;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyware.fragment.Cart.cpBean;
import static com.easyware.service.Webservice.comanid;

public class ProductDetail extends YouTubeBaseActivity /*implements YouTubeThumbnailView.OnInitializedListener ,YouTubePlayer.OnInitializedListener */ {
    private Toolbar toolbar;
    private TextView tprofile, proname, price, description;
    ImageButton back;
    ImageView proimg;
    Button addtocart, buynow;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    Context context;
    TextView title, desc;
    ImageView Thumbnail;
    RelativeLayout container, prmo_container;
    YouTubePlayerView youTubePlayerView;
    private YouTubeThumbnailLoader youTubeThumbnailLoader;
    private YouTubeThumbnailView thumbnailView;
    ImageButton play;

    RelativeLayout maindesc;

    private static final String VIDEO_ID = "fhWaJi1Hsfo";

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public static CartProAdapter cpAdapter;
    private static final int RECOVERY_REQUEST = 1;
    SharedPreference sharedPreference;
    Gson gson;
    public List<CartProductBean> BEan = new ArrayList<>();
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    //private static final String CONFIG_CLIENT_ID = "AZOXVhKXVQ6mDtmAO_npRcKuNFJulIWJ4Xi_L642s1yNZWIXdip-dZ79TirBDHlQ-AP-6SkzPdFqDu5t";
    private static final String CONFIG_CLIENT_ID = "Ab0cRI7pftF0wtgMnSECI4mrub22atffVf2m0tOp9iO8UFiZ1O9x9F4mRSdQNE_lLY0T28EzOtrcCa3_";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("Easyware")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    static PayPalPayment thingToBuy;
    String productPrice = "";
    private RelativeLayout cl;
    private TextView counttext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        manager = new UserSessionManager(ProductDetail.this);
        detector = new ConnectionDetector(ProductDetail.this);
        map = manager.getUserDetail();
        context = getApplicationContext();
        Intent intents = new Intent(ProductDetail.this, PayPalService.class);
        intents.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intents);
        sharedPreference = new SharedPreference(ProductDetail.this);
        SetUpViews();
        final Intent intent = getIntent();
        if (intent.hasExtra("pname")) {
            price.setText(map.get(UserSessionManager.KEY_Curreny) + intent.getStringExtra("price"));
            if (intent.getStringExtra("imgurl").toString().equals("") || intent.getStringExtra("imgurl").toString().equals("na") || intent.getStringExtra("imgurl").toString().equals("null")) {
                Glide.with(ProductDetail.this).load(R.drawable.not_available)
                        .thumbnail(0.5f)
                        .into(proimg);
            } else {
                Glide.with(ProductDetail.this).load(intent.getStringExtra("imgurl").toString())
                        .thumbnail(0.5f)
                        .into(proimg);
            }



            proname.setText(intent.getStringExtra("pname"));
            tprofile.setText(intent.getStringExtra("pname"));
            description.setText(intent.getStringExtra("description"));


            if (description.length() <= 0){
                maindesc.setVisibility(View.GONE);
            }else{
                maindesc.setVisibility(View.VISIBLE);
            }

        }
        if (sharedPreference.loadProduct(ProductDetail.this)!=null) {
            cpBean = sharedPreference.loadProduct(ProductDetail.this);
            cpAdapter = new CartProAdapter(cpBean, ProductDetail.this);
            cpAdapter.notifyDataSetChanged();

            if (cpAdapter.getCount() != 0) {
                //Log.e("123", cpAdapter.getCount() + "");
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    counttext.setBackground(getResources().getDrawable(R.drawable.roundc, null));
                }else{
                    counttext.setBackground(ContextCompat.getDrawable(ProductDetail.this,R.drawable.roundc));
                }
                counttext.setText("" + cpAdapter.getCount());
            } else {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    counttext.setBackgroundColor(getResources().getColor(R.color.trans, null));
                }else {
                    counttext.setBackgroundColor(ContextCompat.getColor(ProductDetail.this,R.color.trans));
                }

            }
        }

    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        //tprofile.setText("Products Details");
        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        proname = (TextView) findViewById(R.id.proname);
        price = (TextView) findViewById(R.id.price);
        proimg = (ImageView) findViewById(R.id.proimg);
        addtocart = (Button) findViewById(R.id.addtocart);
        buynow = (Button) findViewById(R.id.buynow);
        description = (TextView) findViewById(R.id.description);

        maindesc = (RelativeLayout) findViewById(R.id.maindesc);



        counttext = (TextView) findViewById(R.id.counttext);

        cl= (RelativeLayout) findViewById(R.id.cl);
        cl.setVisibility(View.VISIBLE);
        cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(ProductDetail.this, ProductList.class);
                intent.putExtra("cart","cart");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        /*title = (TextView) findViewById(R.id.title);
        desc = (TextView) findViewById(R.id.desc);
        play = (ImageButton) findViewById(R.id.play);*/
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
        container = (RelativeLayout) findViewById(R.id.container);
        //youTubePlayerView.initialize(Webservice.API_KEY, this);
        Thumbnail = (ImageView) findViewById(R.id.Thumbnail);
        /*thumbnailView = (YouTubeThumbnailView) findViewById(R.id.thumbnailview);
        thumbnailView.initialize(Webservice.API_KEY, this);*/
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player);
        //youTubePlayerView.initialize(Webservice.API_KEY, this);

        youTubePlayerView.initialize(Webservice.API_KEY,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {
                        /*if(getIntent().getStringExtra("video").toString().equals("")){
                                youTubePlayerView.setVisibility(View.GONE);
                                Thumbnail.setVisibility(View.VISIBLE);
                            Glide.with(ProductDetail.this).load(R.drawable.not_available)
                                    .thumbnail(0.5f)
                                    .into(Thumbnail);
                        }else{
                            youTubePlayerView.setVisibility(View.VISIBLE);
                            Thumbnail.setVisibility(View.GONE);
                            youTubePlayer.cueVideo(getIntent().getStringExtra("video").toString());
                        }*/
                        youTubePlayer.cueVideo("5xVh-7ywKpE");
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {
                        try {
                            Intent intent = YouTubeStandalonePlayer.createVideoIntent(ProductDetail.this, Webservice.API_KEY, "5xVh-7ywKpE");
                            startActivity(intent);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.addtocart:
                //UserLoginCheck();
                AddtoCart();

                if (sharedPreference.loadProduct(ProductDetail.this)!=null) {
                    cpBean = sharedPreference.loadProduct(ProductDetail.this);
                    cpAdapter = new CartProAdapter(cpBean, ProductDetail.this);
                    cpAdapter.notifyDataSetChanged();

                    if (cpAdapter.getCount() != 0) {
                        //Log.e("123", cpAdapter.getCount() + "");
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            counttext.setBackground(getResources().getDrawable(R.drawable.roundc, null));
                        }else{
                            counttext.setBackground(ContextCompat.getDrawable(ProductDetail.this,R.drawable.roundc));
                        }
                        counttext.setText("" + cpAdapter.getCount());
                    } else {
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            counttext.setBackgroundColor(getResources().getColor(R.color.trans, null));
                        }else {
                            counttext.setBackgroundColor(ContextCompat.getColor(ProductDetail.this,R.color.trans));
                        }

                    }
                }








                break;
            case R.id.buynow:
                //UserLoginCheck();
                BuyProduct();
                break;
        }
    }

    private void BuyProduct() {
        if (map.get(UserSessionManager.KEY_USERID) != null) {
            JSONObject object = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject objectnew = new JSONObject();
            try {
                object.put("product_id", getIntent().getStringExtra("product_id"));
                object.put("price", getIntent().getStringExtra("price"));
                object.put("name", getIntent().getStringExtra("pname"));
                object.put("qty", "1");
                object.put("id_default_image", getIntent().getStringExtra("imgurl"));
                jsonArray.put(object);
                objectnew.put("data", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Log.e("buy : ", objectnew.toString());
            productPrice = getIntent().getStringExtra("price");
            if (detector.isConnectingToInternet()) {
                new BuyOrder().execute(map.get(UserSessionManager.KEY_USERID), map.get(UserSessionManager.KEY_BRANCHID), objectnew.toString());
            } else {
                Webservice.MakeToast("Please Connect your Device to Internet", ProductDetail.this);
            }
        }
    }

    private void AddtoCart() {
        if (map.get(UserSessionManager.KEY_USERID) != null) {
            CartProductBean bean = new CartProductBean();
            bean.setId(getIntent().getStringExtra("product_id").toString());
            bean.setPrice(getIntent().getStringExtra("price"));
            bean.setName(getIntent().getStringExtra("pname"));
            bean.setQty("1");
            bean.setId_default_image(getIntent().getStringExtra("imgurl"));
            if (sharedPreference.loadProduct(ProductDetail.this) == null) {
                cpBean.add(bean);
                sharedPreference.SaveLocalStore(context, cpBean);
                Webservice.MakeToast("Add to Cart Sucessfully", ProductDetail.this);
                //startActivity(new Intent(ProductDetail.this, ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ProductList.counttext.setBackground(context.getResources().getDrawable(R.drawable.roundc, null));
                } else {
                    ProductList.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.roundc));
                }
                ProductList.counttext.setText("" + cpBean.size());
            } else {
                cpBean = sharedPreference.loadProduct(ProductDetail.this);
                for (int i = 0; i < cpBean.size(); i++) {
                    if (getIntent().getStringExtra("product_id").equals(cpBean.get(i).getId())) {
                        Webservice.comanid = cpBean.get(i).getId();
                    }
                }
                if (Webservice.comanid.toString().equals(getIntent().getStringExtra("product_id"))) {
                    Webservice.MakeToast("Products already exist...", ProductDetail.this);
                   // startActivity(new Intent(ProductDetail.this, ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                } else {
                    cpBean.add(bean);
                    sharedPreference.SaveLocalStore(context, cpBean);
                    Webservice.MakeToast("Add to Cart Sucessfully", ProductDetail.this);
                   // startActivity(new Intent(ProductDetail.this, ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ProductList.counttext.setBackground(context.getResources().getDrawable(R.drawable.roundc, null));
                    } else {
                        ProductList.counttext.setBackground(ContextCompat.getDrawable(context, R.drawable.roundc));
                    }
                    ProductList.counttext.setText("" + cpBean.size());
                }
            }
        }
    }

    private void UserLoginCheck() {
        if (map.get(UserSessionManager.KEY_USERID) != null) {
            CartProductBean bean = new CartProductBean();
            bean.setId(getIntent().getStringExtra("id").toString());
            bean.setPrice(getIntent().getStringExtra("price"));
            bean.setName(getIntent().getStringExtra("pname"));
            bean.setQty("1");
            bean.setId_default_image(getIntent().getStringExtra("imgurl"));
            if (cpBean.size() == 0) {
                cpBean.add(bean);
                Webservice.MakeToast("Add to Cart Sucessfully", ProductDetail.this);
            } else {
                for (int i = 0; i < cpBean.size(); i++) {
                   // Log.e("c id : ", cpBean.get(i).getId());
                    //Log.e("123", getIntent().getStringExtra("id"));
                    if (getIntent().getStringExtra("id").equals(cpBean.get(i).getId())) {
                      //  Log.e("if " + getIntent().getStringExtra("id"), cpBean.get(i).getId());
                        comanid = cpBean.get(i).getId();
                        //Log.e("456", comanid);
                    }
                }
                if (comanid.toString().equals(getIntent().getStringExtra("id"))) {
                    Webservice.MakeToast("Products already exist...", ProductDetail.this);
                } else {
                    cpBean.add(bean);
                    Webservice.MakeToast("Add to Cart Sucessfully", ProductDetail.this);
                }
            }
            cpAdapter = new CartProAdapter(cpBean, ProductDetail.this);

            cpAdapter.notifyDataSetChanged();
            if (cpAdapter.getCount() == 0) {
                /*Navigation.counttext.setBackgroundColor(getApplication().getResources().getColor(R.color.trans, null));
                Navigation.ordertotal.setText(WebService.GEtCurrecyById(lcmap.get(LCManager.currencyID), getApplication()) + "0.00");
                Navigation.cancel.setText("Your cart is empty...");
                Navigation.confirm.setVisibility(View.GONE);*/
                Cart.nodata.setVisibility(View.VISIBLE);
                Cart.withshipping.setVisibility(View.GONE);
                Cart.gst_layout.setVisibility(View.GONE);
                Cart.otnew1.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
                //paypalAmount = "0.00";
            } else {
                /*Navigation.counttext.setBackground(getApplication().getResources().getDrawable(R.drawable.round, null));
                Navigation.counttext.setText("" + cpAdapter.getCount());
                Navigation.cancel.setText("cancel Order");
                Navigation.confirm.setVisibility(View.VISIBLE);*/
            }
          //  Log.e("count", cpAdapter.getCount() + "");
            onBackPressed();
        }
    }

    /*@Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            *//*if(getIntent().getStringExtra("video").toString().equals("")){
                youTubePlayerView.setVisibility(View.GONE);
                Thumbnail.setVisibility(View.VISIBLE);
                Glide.with(ProductDetail.this).load(R.drawable.not_available)
                        .thumbnail(0.5f)
                        .into(Thumbnail);
            }else{
                youTubePlayerView.setVisibility(View.VISIBLE);
                Thumbnail.setVisibility(View.GONE);
                youTubePlayer.cueVideo(getIntent().getStringExtra("video").toString());
            }*//*
            youTubePlayer.cueVideo("DR1Z7Z2W9o0");

        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(ProductDetail.this, RECOVERY_REQUEST).show();
        } else {
            Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
        }
    }*/
    /*@Override
    public void onInitializationFailure(YouTubeThumbnailView thumbnailView,
                                        YouTubeInitializationResult errorReason) {

        String errorMessage =
                String.format("onInitializationFailure (%1$s)",
                        errorReason.toString());
        // Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubeThumbnailView thumbnailView,
                                        YouTubeThumbnailLoader thumbnailLoader) {

        // Toast.makeText(getApplicationContext(), "onInitializationSuccess", Toast.LENGTH_SHORT).show();

        youTubeThumbnailLoader = thumbnailLoader;
        thumbnailLoader.setOnThumbnailLoadedListener(new ThumbnailListener());

        youTubeThumbnailLoader.setVideo(VIDEO_ID);
    }*/

    private final class ThumbnailListener implements
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView thumbnail, String videoId) {
            //Toast.makeText(getApplicationContext(), "onThumbnailLoaded", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView thumbnail,
                                     YouTubeThumbnailLoader.ErrorReason reason) {
            // Toast.makeText(getApplicationContext(), "onThumbnailError", Toast.LENGTH_SHORT).show();
        }
    }

    private class BuyOrder extends AsyncTask<String, String, String> {
        android.app.AlertDialog pd;
        String responce, data_code;
        int reqPoints;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ProductDetail.this);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0] + "&branch_id=" + params[1] + "&data=" + params[2];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Emall_Geterate_Order);
            //Log.e("buy_generat_Order", responce + "");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {
                        if (!jsonObject.isNull("data")) {
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                           // Log.e("126556", "" + jsonObject1.getString("reqpoints"));
                            Webservice.OrderId = jsonObject1.getString("ordrid");
                            reqPoints = Integer.parseInt(jsonObject1.getString("reqpoints"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pd != null) {
                pd.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        initiatePopupWindow(reqPoints);
                    } else {
                        Webservice.MakeToast("Something went wrong. Please try again.", ProductDetail.this);
                    }
                }
            }
        }
    }

    private void initiatePopupWindow(final int reqPoints) {
        try {
            LayoutInflater inflater = (LayoutInflater) ProductDetail.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = inflater.inflate(R.layout.payment_option_layout, null);
            final PopupWindow mPopupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mPopupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
            mPopupWindow.update();
            TextView points1 = (TextView) popupView.findViewById(R.id.points1);
            TextView points2 = (TextView) popupView.findViewById(R.id.points2);
            ImageView id_dismiss = (ImageView) popupView.findViewById(R.id.id_dismiss);
            Button paypal = (Button) popupView.findViewById(R.id.paypalb);
            Button submit = (Button) popupView.findViewById(R.id.submit);
            points1.setText("" + map.get(UserSessionManager.KEY_POINTS));
            points2.setText("" + reqPoints);
            id_dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPopupWindow.dismiss();
                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (map.get(UserSessionManager.KEY_POINTS) != null) {
                        if (reqPoints > Integer.parseInt(map.get(UserSessionManager.KEY_POINTS))) {
                            Webservice.MakeToast("Please pay paypal...!", ProductDetail.this);
                        } else {
                            String type = "points";
                            String param = "user_id=" + map.get(UserSessionManager.KEY_USERID) + "&order_id=" + Webservice.OrderId + "&type=" + type + "&points_amount=" + reqPoints;
                            new CheckOut().execute(param);
                            Webservice.MakeToast("Order generate sucessfully", ProductDetail.this);
                            mPopupWindow.dismiss();
                        }
                    }
                }
            });
            paypal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GetPayment(productPrice);
                    mPopupWindow.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetPayment(String payment) {
       // Log.e("payment123", payment);
        String currency;
        if (map.get(UserSessionManager.KEY_Curreny).equals("RM")) {
            currency = "MYR";
        } else {
            currency = map.get(UserSessionManager.KEY_Curreny);
        }
        //Log.e("currency", currency);
        thingToBuy = new PayPalPayment(new BigDecimal(payment), "USD",
                "Easyware", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(ProductDetail.this, PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
               // Log.e("payment log :", "" + confirm);
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject().toString(4));
                        //Log.e("ProductDetail : ", "" + confirm.toJSONObject().toString(4));
                        //Log.e("ProductDetail : ", "" + confirm.getPayment().toJSONObject().toString(4));

                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.getString("response"));
                        String transactionID = jsonObject1.getString("id");
                        String type = "paypal";
                        String param = "user_id=" + map.get(UserSessionManager.KEY_USERID) + "&transaction_id=" + transactionID + "&order_id=" + Webservice.OrderId + "&type=" + type;
                        new CheckOut().execute(param);

                        Webservice.MakeToast("Order generate sucessfully", ProductDetail.this);
                        Webservice.MakeToast("Order placed", ProductDetail.this);
                        //startActivity(new Intent(ProductDetail.this, ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private class CheckOut extends AsyncTask<String, String, String> {
        android.app.AlertDialog pdc;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdc = new ProgressDialog(ProductDetail.this);
            pdc.setMessage("Please wait...");
            pdc.setCancelable(false);
            pdc.show();
        }

        @Override
        protected String doInBackground(String... params) {
            responce = JsonParser.GetJsonFromURL(params[0], Webservice.Emall_CheckOut);
           // Log.e("Emall_CheckOut", responce + "");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pdc != null) {
                pdc.dismiss();
            }
            if (responce != null) {
                if (data_code.equals("200")) {
                    startActivity(new Intent(ProductDetail.this, ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    Webservice.MakeToast("Order generate sucessfully", ProductDetail.this);
                    Webservice.MakeToast("Order placed", ProductDetail.this);
                } else if (data_code.equals("404")) {
                    Webservice.MakeToast("Error occured in no Emall Checkout Order List", ProductDetail.this);
                } else {
                    Webservice.MakeToast("something went wrong...!", ProductDetail.this);
                }
            }
        }
    }
}
