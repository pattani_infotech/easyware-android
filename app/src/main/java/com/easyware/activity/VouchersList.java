package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.adapter.VoucherAdapter;
import com.easyware.bean.VouchersListBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VouchersList extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile,nodata;
    ImageButton back;
    RecyclerView voucherslist;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    List<VouchersListBean> vouchersListBean=new ArrayList<>( );
    VoucherAdapter vAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vouchers_list);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        manager = new UserSessionManager( VouchersList.this );
        map = manager.getUserDetail();
        SetUpViews();
        new GetVouchersList().execute(map.get(UserSessionManager.KEY_USERID) );
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        tprofile.setText( "Vouchers List" );
        back = (ImageButton) findViewById( R.id.back );
        voucherslist= (RecyclerView) findViewById(R.id.voucherslist);
        nodata=(TextView)findViewById(R.id.nodata);
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }
    private class GetVouchersList extends AsyncTask<String, String, String> {
        private String response;
        AlertDialog progressDialog;
        String data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(VouchersList.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param ="user_id="+params[0];
            response = JsonParser.GetJsonFromURL("", Webservice.Vouchers_List+param);
            //Log.e("Voucher List",response);
            if (response != null){
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    data_code = jsonObject.getString( "data_code" );
                    if (data_code.equals( "202" )){
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject obj = jsonArray.getJSONObject( i );
                            VouchersListBean bean = new VouchersListBean();
                            bean.setId(obj.getString( "id" ));
                            bean.setTitle(obj.getString( "title" ));
                            bean.setDescription(obj.getString( "description" ));
                            bean.setRedeem(obj.getString( "redeem" ));
                            bean.setRedeem_date(obj.getString( "redeem_date" ));
                            vouchersListBean.add(bean);
                        }
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            if (response!=null){
                if (data_code.equals("202")){
                    vAdapter = new VoucherAdapter( vouchersListBean, VouchersList.this );
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( VouchersList.this );
                    voucherslist.setLayoutManager( mLayoutManager );
                    voucherslist.setAdapter(vAdapter);
                }else {
                    voucherslist.setVisibility(View.GONE);
                    nodata.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
