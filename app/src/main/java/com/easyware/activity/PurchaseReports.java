package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.Pur_ReportAdapter;
import com.easyware.bean.Pur_Report_Bean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PurchaseReports extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    RecyclerView pur_report;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private Pur_ReportAdapter pur_reportAdapter;
    private List<Pur_Report_Bean> pur_Report_Bean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_reports);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
        SetUpViews();
        manager = new UserSessionManager( PurchaseReports.this );
        detector=new ConnectionDetector(PurchaseReports.this);
        manager=new UserSessionManager(PurchaseReports.this);
        map=manager.getUserDetail();
        pur_Report_Bean=new ArrayList<>( );
        if(detector.isConnectingToInternet())
        {
            new GetPur_Report().execute( map.get( UserSessionManager.KEY_USERID ) );
        }
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        pur_report= (RecyclerView) findViewById(R.id.pur_report);
        tprofile.setText("Purchase Reports");
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }

    private class GetPur_Report extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( PurchaseReports.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0];
            responce = JsonParser.GetJsonFromURL("", Webservice.purchase_Report + param );
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject( responce );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "200" )) {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            Pur_Report_Bean bean=new Pur_Report_Bean();
                            bean.setId(obj.getString( "id" ));
                            bean.setTitle(obj.getString( "title" ));
                            bean.setTxn_id(obj.getString( "txn_id" ));
                            bean.setPrice(obj.getString( "price" ));
                            bean.setPayment_status(obj.getString( "payment_status" ));
                            bean.setCreated(obj.getString( "created" ));
                            pur_Report_Bean.add(bean);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            if (responce != null) {
                if (data_code.equals( "200" )) {
                   pur_reportAdapter = new Pur_ReportAdapter(pur_Report_Bean, PurchaseReports.this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PurchaseReports.this);
                    pur_report.setLayoutManager(mLayoutManager);
                    pur_report.setAdapter(pur_reportAdapter);
                }
            }
        }
    }
}
