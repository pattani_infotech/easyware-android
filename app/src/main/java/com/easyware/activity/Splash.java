package com.easyware.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.GCMClientManager;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.service.Webservice;
import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.HashMap;

public class Splash extends AppCompatActivity {
    //18/04/2018 kd2106
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String,String> map;
    private static final int PERMISSION_REQUEST_CODE = 1;


    private static int SPLASH_TIME_OUT = 1500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector = new ConnectionDetector(Splash.this);
        manager = new UserSessionManager(Splash.this);
        map = manager.getUserDetail();
        try {
            //ShortcutBadger.applyCount(Splash.this, 1); //for 1.1.4+
           // ShortcutBadger.with(getApplicationContext()).count(1); //for 1.1.3
            Badges.removeBadge(Splash.this);
            // Alternative wayCrossBadge.Current.SetBadge(10);
            //CrossBadge.Current.SetBadge(10);
           // Badges.setBadge(Splash.this, 0);
        } catch (BadgesNotSupportedException badgesNotSupportedException) {
            //Log.d(TAG, badgesNotSupportedException.getMessage());
        }

        runTask();

    }

    private void runTask() {

        if (!detector.isConnectingToInternet()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("No Internet");
            builder.setMessage("Internet is required. Please Retry.");

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });

            builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    runTask();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
            Toast.makeText(this, "Network Unavailable!", Toast.LENGTH_LONG).show();


        }else{
            if (checkGooglePlayServices()) {

                new GetGcm().execute();

            } else {
                Toast.makeText(Splash.this, "To receive the notification", Toast.LENGTH_SHORT).show();

            }

            Thread thread = new Thread() {
                public void run() {

                    try {
                        sleep(1500);
                        if (map.get(UserSessionManager.KEY_USERID)!=null){

                            Intent intent=new Intent( Splash.this,CustomerNavigation.class );
                            //intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity( intent );
                             finish();
                        }else{
                            Intent intent = new Intent(Splash.this, Login.class);
                            startActivity(intent);
                              finish();
                        }


                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            };
            thread.start();
        }
    }


    private boolean checkGooglePlayServices() {
        final int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            Webservice.MakeToast("Please update your google play version to use this app.",Splash.this);
           // Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, 1);
          //  dialog.show();
            return false;
        } else {
            return true;
        }
    }


    public class GetGcm extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String gcm = Gcm_id();
            return null;
        }
    }


    public String Gcm_id() {

        GCMClientManager pushClientManager = new GCMClientManager(Splash.this, Webservice.SENDER_ID);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Webservice.gcm = registrationId;
                //Log.e("Gcm_id_registrationId", Webservice.gcm);
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
        return Webservice.gcm;
    }



}

