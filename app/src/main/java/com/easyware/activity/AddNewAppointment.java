package com.easyware.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.easyware.adapter.StaffAdapter;
import com.easyware.bean.StaffBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class AddNewAppointment extends AppCompatActivity {

    private TextView tprofile;
    private ImageButton back;
    private Spinner staff_name;
    private List<StaffBean> staff_list;
    private Button app_date, app_time, book_appoint, update_appoint;
    private EditText notes;
    StaffAdapter staffAdapter;
    UserSessionManager manager;
    ConnectionDetector detector;
    HashMap<String, String> map;
    String currentdate;
    String datef = null;
    static final int DATE_DIALOG_ID = 0;
    static final int TIME_DIALOG_ID = 1;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Calendar c = Calendar.getInstance();
    private int index;
    Date date1 = null;
    Date date2=null;
String appointment_Id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_appointment);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SetUpView();
        manager = new UserSessionManager(AddNewAppointment.this);
        detector = new ConnectionDetector(AddNewAppointment.this);
        map = manager.getUserDetail();
        mYear  = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay   = c.get(Calendar.DAY_OF_MONTH);

        String AM_PM;
        if (mHour < 12) {
            AM_PM = "AM";

        } else {
            AM_PM = "PM";
            mHour = mHour - 12;
        }
       // currentdate=(new StringBuilder().append(mHour).append(":").append(mMinute) + " " + AM_PM);

currentdate=c.get(Calendar.DAY_OF_MONTH)+"/"+(c.get(Calendar.MONTH)+1)+"/"+c.get(Calendar.YEAR);

        if (detector.isConnectingToInternet()) {
            staff_list = new ArrayList<StaffBean>();
            String params = "branch_id=" + map.get(UserSessionManager.KEY_BRANCHID).toString();
            new GetStaffList().execute(params);
        }
        if (getIntent().hasExtra("edit")) {
            if (getIntent().getExtras().getString("edit").equals("edit")) {
                Webservice.staff_id = getIntent().getExtras().getString("staffid").toString();
                tprofile.setText("Edit Appointment");
                // Toast.makeText(getApplicationContext(),getIntent().getExtras().getString("date").toString(), Toast.LENGTH_SHORT).show();
                //staff_name.setSelection(staff_list.indexOf(getIntent().getExtras().getString("staffid").toString()));
                //  Log.e("name ",""+getIntent().getExtras().getString("staffid").toString());
                // staff_name.setSelection(staffAdapter.(staff_list.indexOf(getIntent().getExtras().getString("staffid").toString())));
                app_date.setText(getIntent().getExtras().getString("date").toString());
                app_time.setText(getIntent().getExtras().getString("time").toString());
                notes.setText(getIntent().getExtras().getString("note").toString());
                appointment_Id=getIntent().getExtras().getString("id").toString();
               /* Log.e("id :",appointment_Id);
                Log.e("id :",getIntent().getExtras().getString("id").toString());
                Log.e("date :",getIntent().getExtras().getString("date").toString());
                Log.e("time :",getIntent().getExtras().getString("time").toString());
                Log.e("note :",getIntent().getExtras().getString("note").toString());
                Log.e("staff id :",getIntent().getExtras().getString("staffid").toString());
                Log.e("staff name :",getIntent().getExtras().getString("staffName").toString());*/
                update_appoint.setVisibility(View.VISIBLE);
                book_appoint.setVisibility(View.GONE);
            }
        }

    }

    private void SetUpView() {

        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) findViewById(R.id.back);
        tprofile.setText("Add Appointment");
        staff_name = (Spinner) findViewById(R.id.staff_name);
        app_date = (Button) findViewById(R.id.app_date);
        notes = (EditText) findViewById(R.id.notes);
        app_time = (Button) findViewById(R.id.app_time);
        book_appoint = (Button) findViewById(R.id.book_appoint);
        update_appoint = (Button) findViewById(R.id.update_appoint);

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);


        app_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        app_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(TIME_DIALOG_ID);
            }
        });
        //updateDisplay();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        book_appoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckStatus();
            }
        });
        update_appoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detector.isConnectingToInternet()) {
                    String param="staff="+Webservice.staff_id+"&date="+Uri.encode(app_date.getText().toString(), Webservice.ALLOWED_URI_CHARS)+"&time="+ Uri.encode(app_time.getText().toString(), Webservice.ALLOWED_URI_CHARS)+"&description="+Uri.encode(notes.getText().toString(), Webservice.ALLOWED_URI_CHARS)+"&id="+appointment_Id;
                    new Updateappointment().execute(param,Webservice.Update_Appointment);
                } else {
                    Toast.makeText(AddNewAppointment.this, "Please check your internet coneection", Toast.LENGTH_LONG).show();
                }
            }
        });

        staff_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String a = staff_list.get(i).getName();
                Webservice.staff_id = staff_list.get(i).getId();
                if (staff_name.getSelectedItemPosition() != 0) {
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void CheckStatus() {

        SimpleDateFormat mSDF = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            datef= mSDF.format(formatter.parse(app_date.getText().toString()));
            date1 = mSDF.parse(datef);
            currentdate=mSDF.format(formatter.parse(Webservice.CurrentDate()));
            date2=mSDF.parse(currentdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (staff_name.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please Select Staff Name...!", Toast.LENGTH_SHORT).show();
        } else if (app_date.getText().toString().equals("Select Date") || app_date.getText().toString().equals("")) {
            Toast.makeText(this, "Please Conform date...!", Toast.LENGTH_SHORT).show();
        } else if (app_time.getText().toString().equals("Select Time") || app_time.getText().toString().equals("")) {
            Toast.makeText(this, "Please Conform time...!", Toast.LENGTH_SHORT).show();
        } else if (notes.getText().toString().equals("")) {
            notes.setError("Please type Note...");
        }else if ((date1.compareTo(date2)<0)){
            Toast.makeText(this, "Please Select Currect Date...!", Toast.LENGTH_SHORT).show();
        }else {
            if (detector.isConnectingToInternet()) {
                new Addappointment().execute(map.get(UserSessionManager.KEY_USERID).toString(), map.get(UserSessionManager.KEY_BRANCHID).toString(),
                       notes.getText().toString(), app_date.getText().toString(), app_time.getText().toString());
                //Toast.makeText(this, "successfull...!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Please check your internet coneection", Toast.LENGTH_LONG).show();
            }
        }
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this,
                        mTimeSetListener, mHour, mMinute, false);
        }
        return null;
    }

    private void updateDisplay() {
        String AM_PM;
        if (mHour < 12) {
            AM_PM = "AM";

        } else {
            AM_PM = "PM";
            mHour = mHour - 12;
        }
        String minut = Integer.toString(mMinute);
        if (minut.length()==1){
            minut="0"+minut;
        }



        app_time.setText(new StringBuilder().append(mHour).append(":").append(minut).append(" ").append(AM_PM));

    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            app_time.setText(hourOfDay + ":" + minute);
            mHour = hourOfDay;

            mMinute = minute;
            updateDisplay();
        }
    };

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            SimpleDateFormat sdf = new SimpleDateFormat("y/M/d");
            app_date.setText(sdf.format(c.getTime()));
            String mnth = Integer.toString(mMonth + 1);
            String d_day = Integer.toString(mDay);

            if (mnth.length() == 1) {
                mnth = "0" + mnth;
            }
            if (d_day.length() == 1) {
                d_day = "0" + d_day;
            }
            app_date.setText(new StringBuilder().append(mYear).append("-").append(mnth).append("-").append(d_day));
        }
    };

    class GetStaffList extends AsyncTask<String, String, String> {
        String responce, data_code;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddNewAppointment.this);
            dialog.setMessage("Please wait");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            responce = JsonParser.GetJsonFromURL("", Webservice.GetStaff + params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (responce != null) {
                StaffBean staffBean1 = new StaffBean();
                staffBean1.setId("0");
                staffBean1.setName("Select Staff Name");
                staff_list.add(staffBean1);
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            StaffBean staffBean = new StaffBean();
                            if (getIntent().hasExtra("edit")) {
                                if (getIntent().getStringExtra("staffid").toString().equals(obj.getString("ID"))) {
                                    index = i;
                                }
                            }

                            staffBean.setId(obj.getString("ID"));
                            staffBean.setName(obj.getString("name"));
                            staff_list.add(staffBean);
                        }
                    }
                    staffAdapter = new StaffAdapter(AddNewAppointment.this, staff_list);
                    staff_name.setAdapter(staffAdapter);
                    if (getIntent().hasExtra("edit")) {
                        //Log.e("getIntent if", index + " ");
                        staff_name.setSelection(index + 1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            dialog.dismiss();
        }
    }

    private class Addappointment extends AsyncTask<String, String, String> {
        String responce, data_code;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddNewAppointment.this);
            dialog.setMessage("Please wait, while we book your appointment");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0] + "&branch_id=" + params[1] + "&staff=" + Webservice.staff_id + "&description=" + params[2] + "&date=" + params[3] + "&time=" + params[4];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Add_Appointment);
            //Log.e("param",param);
            //Log.e("123",responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("data_code");
                    if (data_code.equals("202")) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("202")) {
                        Toast.makeText(AddNewAppointment.this, "Appointment add successfull", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddNewAppointment.this,Appointments.class));
                        finish();
                       // cleardata();
                    }else{
                        Webservice.MakeToast("Please Try Again",AddNewAppointment.this);
                    }
                }
            }else{
                Webservice.MakeToast("Please Try Again",AddNewAppointment.this);
            }

        }
    }

    private class Updateappointment extends AsyncTask<String, String, String> {
        String responce, data_code;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AddNewAppointment.this);
            dialog.setMessage("Please wait, while we book your appointment");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            responce = JsonParser.GetJsonFromURL("", Webservice.Update_Appointment+params[0]);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("data_code");
                    if (data_code.equals("202")) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("202")) {
                        Toast.makeText(AddNewAppointment.this, "Appointment Update successfull", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AddNewAppointment.this, Appointments.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        book_appoint.setVisibility(View.VISIBLE);
                        update_appoint.setVisibility(View.GONE);
                        cleardata();
                    }
                }
            }

        }

    }

    private void cleardata() {
        staff_name.setSelection(0);
        notes.setText("");
        app_date.setText("Select Date");
        app_time.setText("Select Time");
    }
}
