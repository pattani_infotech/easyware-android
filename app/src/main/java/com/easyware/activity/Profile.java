package com.easyware.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.easyware.easyware.R;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class Profile extends AppCompatActivity {
    TextView tprofile,tvname,tvphone,tvaddress,tvcity,tvdob;
    EditText edusername,edemail,edfirst,edlast,eddob;
    ImageButton back,takepic,datepicker;
    private Button update,date,change_password;
    ImageView dp_profile;
    Toolbar toolbar;
    int columnIndex;

    private static final int SELECT_FILE = 1889;
    private static final int REQUEST_CAMERA = 1888;
    private static String filepath;
    private static int PERMISSION_REQUEST_CODE = 1;


    static final int DATE_DIALOG_ID = 0;
    private int mYear,mMonth,mDay;
    Calendar c= Calendar.getInstance();


    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String,String> map;
    private ProgressBar profbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SetUpViews();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(Profile.this,CustomerNavigation.class));
                finish();
            }
        });
        detector=new ConnectionDetector(Profile.this);

        manager=new UserSessionManager(Profile.this);
        map=manager.getUserDetail();

        if(map.get(UserSessionManager.KEY_USERID)!=null) {
            edusername.setText(map.get(UserSessionManager.KEY_USERNAME).toString());
            edfirst.setText(map.get(UserSessionManager.KEY_FIRSTNAME).toString());
            edlast.setText(map.get(UserSessionManager.KEY_LASTNAME).toString());
            edemail.setText(map.get(UserSessionManager.KEY_EMAIL).toString());
            eddob.setText(map.get(UserSessionManager.KEY_DOB).toString());
        }
        //Log.e("Image_Webservice",""+map.get( UserSessionManager.KEY_IMG ));

        Glide.with( this ).load( Webservice.img_path+map.get( UserSessionManager.KEY_IMG ).toString()).asBitmap().centerCrop().into( new BitmapImageViewTarget( dp_profile ) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create( getResources(), resource );
                circularBitmapDrawable.setCircular( true );
                dp_profile.setImageDrawable( circularBitmapDrawable );
                profbar.setVisibility(View.GONE);
            }
        } );

       /* Glide.with( this ).load( Webservice.img_path+map.get( UserSessionManager.KEY_IMG )).centerCrop()
                .listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                profbar.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                profbar.setVisibility(View.GONE);
                return false;
            }
        }).into(dp_profile);*/




















        takepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectImage();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(edusername.getText().toString().length()<=0)
                {
                    edusername.setError("Please Enter Your UserName");
                }
                else if(edfirst.getText().toString().length()<=0)
                {
                    edfirst.setError("Please Enter Your First Name");
                }
                else if(edlast.getText().toString().length()<=0)
                {
                    edlast.setError("Please Enter Your Last Name");
                }
                else if(edemail.getText().toString().length()<=0)
                {
                    edemail.setError("Please Enter Your Email");
                }
                else
                {
                    if (eddob.getText().toString().length() <= 0) {
                        eddob.setError("Please Enter Your Birth Date");
                }
                    else
                    {
                        if (detector.isConnectingToInternet())
                        {
                         new updateProfileData().execute(edusername.getText().toString(),
                                 edfirst.getText().toString(),
                                 edlast.getText().toString(),
                                 edemail.getText().toString(),
                                 eddob.getText().toString()  );


                        }
                        else
                        {
                            Toast.makeText(Profile.this,"Please Check Your Internet",Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Profile.this,Password_change.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });


    }

    public void SetUpViews(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView)toolbar.findViewById(R.id.tprofile);
        datepicker= (ImageButton) findViewById(R.id.datepicker);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        takepic = (ImageButton) findViewById(R.id.takepic);
        dp_profile = (ImageView) findViewById(R.id.dp_profile);

        edusername = (EditText)findViewById(R.id.editusername);
        edemail = (EditText)findViewById(R.id.editemail);
        edfirst = (EditText)findViewById(R.id.editfirst);
        edlast = (EditText)findViewById(R.id.editlname);
        eddob = (EditText)findViewById(R.id.eddob);

        update= (Button) findViewById(R.id.update);
        change_password= (Button) findViewById(R.id.change_password);

        profbar = (ProgressBar) findViewById(R.id.profbar);

        mYear=c.get(Calendar.YEAR);
        mMonth=c.get(Calendar.MONTH);
        mDay=c.get(Calendar.DAY_OF_MONTH);

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        CheckPermission();



    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            eddob.setText( sdf.format(c.getTime()));
            String mnth = Integer.toString(mMonth + 1);
            String d_day = Integer.toString(mDay);

            if (mnth.length() == 1) {
                mnth = "0" + mnth;
            }
            if (d_day.length() == 1) {
                d_day = "0" + d_day;
            }
            eddob.setText(new StringBuilder().append(mYear).append("-").append(mnth).append("-").append(d_day));
        }
    };

    private void requestPermission(){
        ActivityCompat.requestPermissions(Profile.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA
        },PERMISSION_REQUEST_CODE);
    }
    private boolean CheckPermission(){

        columnIndex = ContextCompat.checkSelfPermission(Profile.this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (columnIndex == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            requestPermission();
            return false;
        }
    }

    private void SelectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
        builder.setTitle("Upload Profile Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");

                    startActivityForResult(
                            Intent.createChooser(intent, "Select file"),
                            SELECT_FILE);

                } else if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }


            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                ByCamera(data);
            } else if (requestCode == SELECT_FILE){
                BySdcard(data);
            }
        }
    }
    public void BySdcard(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getContentResolver().query(
                selectedImageUri, projection, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(projection[0]);
        String picturePath = cursor.getString(columnIndex);
        filepath = picturePath;

        File dir=Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        Bitmap b= BitmapFactory.decodeFile(filepath);
        Bitmap out = Bitmap.createScaledBitmap(b, 320, 480, false);

        File file = new File(dir, System.currentTimeMillis()+".png");
        filepath=file.getPath();

        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(file);
            out.compress(Bitmap.CompressFormat.PNG, 70, fOut);
            fOut.flush();
            fOut.close();
            b.recycle();
            out.recycle();
        } catch (Exception e) {}
        if(detector.isConnectingToInternet())
        {
            new UpdateProfilePicture().execute();
        }
        cursor.close();


        Glide.with( this ).load(picturePath).asBitmap().centerCrop().placeholder(R.drawable.profileimage).into( new BitmapImageViewTarget( dp_profile ) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create( getResources(), resource );
                circularBitmapDrawable.setCircular( true );
                dp_profile.setImageDrawable( circularBitmapDrawable );
                profbar.setVisibility(View.GONE);
            }
        });
        //dp_profile.setImageBitmap(BitmapFactory.decodeFile(picturePath));

      /*  Glide.with( this ).load( Webservice.img_path+map.get( UserSessionManager.KEY_IMG ).toString()).centerCrop()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        profbar.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        profbar.setVisibility(View.GONE);
                        return false;
                    }
                }).into(dp_profile);

*/


    }

    public  void ByCamera(Intent data)
    {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
       // System.out.println("Hs");
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 70,
                arrayOutputStream);

        File destination = new File(
                Environment.getExternalStorageDirectory().getAbsoluteFile() ,
                System.currentTimeMillis() + ".jpg");

        filepath = destination.getAbsolutePath().toString();

        FileOutputStream fo;
        try {
            // Log.e("path", filepath);
            try {
                destination.createNewFile();
            }catch (Exception e)
            {
               // Log.e("file",e.toString());
            }

            //  Log.e("path", filepath);
            fo = new FileOutputStream(destination);
            fo.write(arrayOutputStream.toByteArray());
            //  Log.e("destination", destination.toString());
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(detector.isConnectingToInternet())
        {
            new UpdateProfilePicture().execute();
        }
        Glide.with( this ).load(filepath).asBitmap().centerCrop().into( new BitmapImageViewTarget( dp_profile ) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create( getResources(), resource );
                circularBitmapDrawable.setCircular( true );
                dp_profile.setImageDrawable( circularBitmapDrawable );
            }
        } );





    }


    class UpdateProfilePicture extends AsyncTask<String,String,String>
    {
        android.app.AlertDialog progressDialog;
        String responce,data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Profile.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.e("Image_Background",""+filepath);
            String param="?user_id="+map.get(UserSessionManager.KEY_USERID).toString();//+"&avatar="+filepath;
            responce=JsonParser.uploadFile(Webservice.UpdateProfilePic+param,filepath);
            try {
                JSONObject object=new JSONObject(responce);
                data_code=object.getString("error");
                if(data_code!=null)
                {
                    if(data_code.equals("false"))
                    {
                        manager.createUserLoginSession(map.get(UserSessionManager.KEY_USERID).toString(),
                                map.get(UserSessionManager.KEY_USERNAME).toString(),
                                map.get(UserSessionManager.KEY_FIRSTNAME).toString(),
                                map.get(UserSessionManager.KEY_LASTNAME).toString(),
                                map.get(UserSessionManager.KEY_EMAIL).toString(),
                                map.get(UserSessionManager.KEY_PASSWORD).toLowerCase(),
                                map.get(UserSessionManager.KEY_TYPE).toString(),
                                map.get(UserSessionManager.KEY_BRANCHID).toString(),
                                map.get(UserSessionManager.KEY_COMPANYID).toString(),
                                map.get(UserSessionManager.KEY_CREDIT).toString(),
                                map.get(UserSessionManager.KEY_POINTS).toString(),
                                object.getString("aavtar").toString(),
                                map.get(UserSessionManager.KEY_MOBILE).toString(),
                                map.get(UserSessionManager.KEY_CITY).toString(),
                                map.get(UserSessionManager.KEY_DOB).toString(),
                                map.get(UserSessionManager.KEY_BRANCH).toString(),
                                map.get(UserSessionManager.KEY_DAY).toString(),
                                map.get(UserSessionManager.KEY_Curreny).toString(),
                                map.get(UserSessionManager.KEY_GST).toString(),
                                map.get(UserSessionManager.KEY_GSTFORMAT).toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog != null) {
                progressDialog.dismiss();
            }

            if(data_code!=null)
            {
                if(data_code.equals("false"))
                {
                    Toast.makeText(Profile.this,"Profile Picture updated ",Toast.LENGTH_LONG).show();
                }
            }
        }
    }



    class updateProfileData extends AsyncTask<String,String,String>
    {

        android.app.AlertDialog progressDialog;
        String responce,data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Profile.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {

            String param="user_id="+map.get(UserSessionManager.KEY_USERID).toString()+"&first_name="+params[1]+"&last_name="+params[2]+"&email="+params[3]+"&dob="+params[4];
            responce= JsonParser.GetJsonFromURL(param,Webservice.UpdateProfile);
            //Log.e("Param","" + responce);
            try {
                JSONObject object=new JSONObject(responce);
                data_code=object.getString("error").toString();
                if(data_code!=null)
                {
                    if(data_code.equalsIgnoreCase("false"))
                    {
                        manager.createUserLoginSession(map.get(UserSessionManager.KEY_USERID).toString(),map.get(UserSessionManager.KEY_USERNAME).toString(),params[1],params[2],params[3],map.get(UserSessionManager.KEY_PASSWORD).toLowerCase(),map.get(UserSessionManager.KEY_TYPE).toString(),map.get(UserSessionManager.KEY_BRANCHID).toString(),map.get(UserSessionManager.KEY_COMPANYID).toString(),map.get(UserSessionManager.KEY_CREDIT).toString(),map.get(UserSessionManager.KEY_POINTS).toString(),map.get(UserSessionManager.KEY_IMG).toString(),map.get(UserSessionManager.KEY_MOBILE).toString(),map.get(UserSessionManager.KEY_CITY).toString(),params[4],map.get(UserSessionManager.KEY_BRANCH).toString(),map.get(UserSessionManager.KEY_DAY).toString(),map.get(UserSessionManager.KEY_Curreny).toString(),map.get(UserSessionManager.KEY_GST).toString(),map.get(UserSessionManager.KEY_GSTFORMAT).toString());
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(progressDialog != null) {
                progressDialog.dismiss();
            }
            if (responce!=null){

                if(data_code!=null)
                {if(data_code.equalsIgnoreCase("false")) {
                        Toast.makeText(Profile.this, "Update successfull", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Profile.this, CustomerNavigation.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }
    }

 }


