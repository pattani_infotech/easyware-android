package com.easyware.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.PurchaseHistoryAdapter;
import com.easyware.bean.PurchaseHistoryBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductHistory extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton back;
    TextView tprofile;
    RecyclerView purchaseHistory;
    List<PurchaseHistoryBean> unpaidHistoryBean =new ArrayList<>();
    PurchaseHistoryAdapter unpaidAdapter;
    double total=0;

    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private TextView TotalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_history);
        SetUpViews();
        manager=new UserSessionManager(ProductHistory.this);
        map=manager.getUserDetail();
        detector.isConnectingToInternet();
    }

    private void SetUpViews() {


    }
}
