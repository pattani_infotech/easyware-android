package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.HappyHoursAdapter;
import com.easyware.bean.HappyHoursBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HappyHours extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    RecyclerView happyhours;
    ListView happyhoursnew;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;

    private HappyHoursAdapter happyHoursAdapter;
    private List<HappyHoursBean> happyHoursBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_hours);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
        SetUpViews();
        manager = new UserSessionManager( HappyHours.this );
        detector=new ConnectionDetector(HappyHours.this);
        map=manager.getUserDetail();
        happyHoursBean=new ArrayList<>( );
        if(detector.isConnectingToInternet()) {
            new GetHappyHours().execute( map.get( UserSessionManager.KEY_BRANCHID ) );
        }
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        happyhours= (RecyclerView) findViewById(R.id.happyhours);
        happyhoursnew= (ListView) findViewById(R.id.happyhoursnew);
        tprofile.setText("Happy Hours");
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );

    }
    private class GetHappyHours extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(HappyHours.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL("", Webservice.HappyHours + param );
            //Log.e("HappyHours",responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject( responce );
                    data_code = jsonObject.getString( "error" );
                    if (data_code.equals( "200" )) {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            HappyHoursBean bean=new HappyHoursBean();
                            bean.setId(obj.getString( "id" ));
                            bean.setTitle(obj.getString( "title" ));
                            bean.setDescription(obj.getString( "description" ));
                            bean.setImage(obj.getString( "image" ));
                            bean.setExpiry_date(obj.getString( "expiry_date" ));
                            bean.setPrice(obj.getString( "price" ));
                            happyHoursBean.add(bean);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            if (responce != null) {
                if (data_code.equals( "200" )) {
                     happyHoursAdapter = new HappyHoursAdapter(happyHoursBean, HappyHours.this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HappyHours.this);
                    happyhours.setLayoutManager(mLayoutManager);
                    happyhours.setAdapter(happyHoursAdapter);
                }
            }
        }
    }
}
