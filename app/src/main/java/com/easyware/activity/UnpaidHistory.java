package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.UnpaidHistoryAdapter;
import com.easyware.bean.UnpaidHistoryBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UnpaidHistory extends AppCompatActivity {
    Toolbar toolbar;
    ImageButton back;
    TextView tprofile;
    RecyclerView unpaiHistory;
    List<UnpaidHistoryBean> unpaidHistoryBean;// =new ArrayList<>();
    UnpaidHistoryAdapter unpaidAdapter;
    double total=0;

    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private TextView TotalData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpaid_history);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            SetUpViews();
        unpaidHistoryBean =new ArrayList<>();
        manager = new UserSessionManager(UnpaidHistory.this );
        map = manager.getUserDetail();
        detector = new ConnectionDetector(UnpaidHistory.this );

        if(detector.isConnectingToInternet())
        {
                new GetUnpaid_HistoryDetail().execute( map.get( UserSessionManager.KEY_USERID ) );
        }



    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView)findViewById(R.id.tprofile);
        back = (ImageButton)toolbar.findViewById(R.id.back);
        tprofile.setText("Un-Paid History");
        unpaiHistory= (RecyclerView) findViewById(R.id.unpaiHistory);
        TotalData=(TextView)findViewById( R.id.TotalData );
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    class GetUnpaid_HistoryDetail extends AsyncTask<String, String, String>
    {
        AlertDialog progressDialog;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( UnpaidHistory.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {

            String param ="user_id="+params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.unpaid_history );
            //Log.e("unpaid history",responce);
            if (responce != null)
            {
                try {
                    JSONObject jsonObject = new JSONObject( responce );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "202" ))
                    {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            UnpaidHistoryBean bean = new UnpaidHistoryBean();
                            bean.setUnpaid_id( obj.getString( "id" ) );
                            bean.setUnpaid_title( obj.getString( "rec_no" ) );
                            bean.setUnpaid_total( obj.getString( "total" ) );
                            total=total+Double.parseDouble( obj.getString( "total" ) );
                            bean.setUnpaid_detail( obj.getString( "remain" ) );
                            unpaidHistoryBean.add( bean );

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );

            TotalData.setText(map.get(UserSessionManager.KEY_Curreny)+ total );
            unpaidAdapter = new UnpaidHistoryAdapter(unpaidHistoryBean,UnpaidHistory.this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( UnpaidHistory.this );
            unpaiHistory.setLayoutManager( mLayoutManager );
            unpaiHistory.setAdapter( unpaidAdapter );
            if(progressDialog != null) {
                progressDialog.dismiss();
            }

        }
    }
}
