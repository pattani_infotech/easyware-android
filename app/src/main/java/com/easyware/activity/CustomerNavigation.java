package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.easyware.adapter.DayAppoinmentAdapter;
import com.easyware.bean.DayAppointmentBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.SharedPreference;
import com.easyware.easyware.UserSessionManager;
import com.easyware.fragment.Cart;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomerNavigation extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Button home, profile, messages, promotions, appointments, purchasehistory, unpaidhistory,
            producthistory, servicehistory, claimslist, happyhours, video, referralcommission, pointsreports, purchasereports, signout, change_pwd, close,
            depositlist,voucherslist,emallorders;
    TextView titledeshbord, pname, pcredit, point, comp_name,username,bdaytitle,credit,pointp;
    RelativeLayout demo;
    ImageButton cart;
    ImageView pimg,bdayimg;
    LinearLayout l_pro, l_promotion, l_messages, l_appointment, l_pur_hist, l_unpaid_hist;
    Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    View header;
    RecyclerView apoinmentdetail;
    private List<DayAppointmentBean> dayAppointmentBean;
    private DayAppoinmentAdapter dayAppoinmentAdapter;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    SharedPreference sharedPreference;
    private Button course_claimslist;
    private String bday_title,bday_image;
    ProgressBar progbar;
    private Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_navigation);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        detector = new ConnectionDetector(CustomerNavigation.this);
        manager = new UserSessionManager(CustomerNavigation.this);
        map = manager.getUserDetail();
        sharedPreference=new SharedPreference(CustomerNavigation.this);
        SetUpViews();
        dayAppointmentBean=new ArrayList<>( );
        getAppointmentApi();
        /*if(detector.isConnectingToInternet()) {
            Log.e("userid",map.get( UserSessionManager.KEY_USERID ));
            Log.e("branchid",map.get( UserSessionManager.KEY_BRANCHID ));
            new GetAppoinment().execute( map.get( UserSessionManager.KEY_BRANCHID ).toString(),map.get( UserSessionManager.KEY_USERID ).toString() );
        }*/

        pname.setText("Welcome, " + map.get(UserSessionManager.KEY_FIRSTNAME).toString()+" "+map.get(UserSessionManager.KEY_LASTNAME).toString());
        username.setText("Welcome, " + map.get(UserSessionManager.KEY_FIRSTNAME).toString()+" "+map.get(UserSessionManager.KEY_LASTNAME).toString());
        pcredit.setText("Credits : " + map.get(UserSessionManager.KEY_CREDIT).toString());
        point.setText("Points : " + map.get(UserSessionManager.KEY_POINTS).toString());
        comp_name.setText("Branch : " + map.get(UserSessionManager.KEY_BRANCH).toString());

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, Profile.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        l_pro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, Profile.class));
            }
        });

        appointments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, Appointments.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        l_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, Appointments.class));
            }
        });

        messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerNavigation.this, Messaging.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        l_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerNavigation.this, Messaging.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        promotions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerNavigation.this, Promotions.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        l_promotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerNavigation.this, Promotions.class));
            }
        });
        purchasehistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerNavigation.this, PurchaseHistory.class);
                intent.putExtra("title", "purchase");
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        l_pur_hist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerNavigation.this, PurchaseHistory.class);
                intent.putExtra("title", "purchase");
                startActivity(intent);
            }
        });

        unpaidhistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerNavigation.this, UnpaidHistory.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        l_unpaid_hist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerNavigation.this, UnpaidHistory.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        change_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, Password_change.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        producthistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerNavigation.this, PurchaseHistory.class);
                intent.putExtra("title", "product");
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);
                //finish();

            }
        });
        servicehistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, ServiceHistory.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        claimslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, ClaimsList.class).putExtra("title", "claim"));
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        course_claimslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, ClaimsList.class).putExtra("title", "course"));
                drawer.closeDrawer(GravityCompat.START);

            }
        });
        happyhours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, HappyHours.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, Video.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        referralcommission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, ReferralCommission.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        pointsreports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, PointsReports.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        purchasereports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, PurchaseReports.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        depositlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, DepositList.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        voucherslist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, VouchersList.class));
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CustomerNavigation.this, ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        emallorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(CustomerNavigation.this, ProductList.class);
                intent.putExtra("myorder","myorder");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                drawer.closeDrawer(GravityCompat.START);

           /*  try {
                    fragment = (Fragment) MyOrders.class.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.frmly, fragment).commit();*/
            }
        });


        // Log.e("Image",""+map.get( UserSessionManager.KEY_IMG ));
        Glide.with(CustomerNavigation.this).load(Webservice.img_path + map.get(UserSessionManager.KEY_IMG).toString())
                .asBitmap().centerCrop().into(new BitmapImageViewTarget(pimg) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                pimg.setImageDrawable(circularBitmapDrawable);
                progbar.setVisibility(View.GONE);

            }
        });
    }


    public void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        titledeshbord = (TextView) toolbar.findViewById(R.id.titledeshbord);
        cart = (ImageButton) toolbar.findViewById(R.id.cart);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        apoinmentdetail = (RecyclerView) findViewById(R.id.apoinmentdetail);
        l_pro = (LinearLayout) findViewById(R.id.l_pro);
        l_promotion = (LinearLayout) findViewById(R.id.l_promotion);
        l_messages = (LinearLayout) findViewById(R.id.l_messages);
        l_appointment = (LinearLayout) findViewById(R.id.l_appointment);
        l_pur_hist = (LinearLayout) findViewById(R.id.l_pur_hist);
        l_unpaid_hist = (LinearLayout) findViewById(R.id.l_unpaid_hist);
        pimg = (ImageView) findViewById(R.id.pimg);
        bdayimg = (ImageView) findViewById(R.id.bdayimg);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        home = (Button) header.findViewById(R.id.home);
        profile = (Button) header.findViewById(R.id.profile);
        messages = (Button) header.findViewById(R.id.messages);
        promotions = (Button) header.findViewById(R.id.promotions);
        appointments = (Button) header.findViewById(R.id.appointments);
        depositlist = (Button) header.findViewById(R.id.depositlist);
        voucherslist = (Button) header.findViewById(R.id.voucherslist);
        purchasehistory = (Button) header.findViewById(R.id.purchasehistory);
        unpaidhistory = (Button) header.findViewById(R.id.unpaidhistory);
        producthistory = (Button) header.findViewById(R.id.producthistory);
        servicehistory = (Button) header.findViewById(R.id.servicehistory);
        claimslist = (Button) header.findViewById(R.id.claimslist);
        happyhours = (Button) header.findViewById(R.id.happyhours);
        video = (Button) header.findViewById(R.id.video);
        referralcommission = (Button) header.findViewById(R.id.referralcommission);
        pointsreports = (Button) header.findViewById(R.id.pointsreports);
        purchasereports = (Button) header.findViewById(R.id.purchasereports);
        pimg = (ImageView) header.findViewById(R.id.pimg);

        progbar = (ProgressBar) header.findViewById(R.id.progbar);

        pname = (TextView) header.findViewById(R.id.pname);
        signout = (Button) header.findViewById(R.id.signout);
        emallorders = (Button) header.findViewById(R.id.emallorders);
        change_pwd = (Button) header.findViewById(R.id.change_password);
        close = (Button) header.findViewById(R.id.close);
        pcredit = (TextView) header.findViewById(R.id.pcredit);
        point = (TextView) header.findViewById(R.id.point);
        comp_name = (TextView) header.findViewById(R.id.comp_name);
        username = (TextView) findViewById(R.id.username);
        bdaytitle = (TextView) findViewById(R.id.bdaytitle);
        credit = (TextView) findViewById(R.id.credit);
        pointp = (TextView) findViewById(R.id.pointp);
        demo= (RelativeLayout) findViewById(R.id.demo);
        //nodata = (TextView) findViewById(R.id.nodata);
        course_claimslist = (Button) header.findViewById(R.id.course_claimslist);
    }

    public void signout(View v) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CustomerNavigation.this);
        alertDialog.setTitle("LogOut");
        alertDialog.setMessage("Are you sure you want to logout?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                manager.logoutUser();
                Cart.cpBean.clear();
                sharedPreference.SaveLocalStore(CustomerNavigation.this,Cart.cpBean);
                finish();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        alertDialog.show();
    }

    public void proimg(View view) {
        Intent intent = new Intent(CustomerNavigation.this, Profile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.customer_navigation, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_camera) {}
        else if (id == R.id.nav_gallery) {}
        else if (id == R.id.nav_slideshow) {}
        else if (id == R.id.nav_manage) {}
        else if (id == R.id.nav_share) {}
        else if (id == R.id.nav_send) {}
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class GetAppoinment extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CustomerNavigation.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            if(progressDialog == null) {
                progressDialog.show();
            }
            //progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0]+"&user_id="+params[1];
            responce = JsonParser.GetJsonFromURL("", Webservice.Appoinment+param);
            Log.e("Navigation", responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals( "202" )) {
                        if (!jsonObject.isNull("data" )){
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                             if(jsonObject1.has("birthday")) {
                                 JSONObject bday_json=jsonObject1.getJSONObject("birthday");
                                 bday_title=bday_json.getString("title");
                                 bday_image=bday_json.getString("image");
                             }
                            if(jsonObject1.has("appointment")) {
                                JSONArray jsonArray = jsonObject1.getJSONArray( "appointment" );
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject( i );
                                    DayAppointmentBean bean=new DayAppointmentBean();
                                    bean.setStaff(obj.getString( "staff" ));
                                    bean.setNotes(obj.getString( "notes" ));
                                    bean.setDate(obj.getString( "date" ));
                                    bean.setTime(obj.getString( "time" ));
                                    dayAppointmentBean.add(bean);
                                }
                            }
                        }
                    }
                    Log.e("gst",jsonObject.getString("gst"));
                    Log.e("gstformat",jsonObject.getString("gstformat"));
                    Log.e("credit",jsonObject.getString("credit"));
                    Log.e("points",jsonObject.getString("points"));
                    manager.createUserLoginSession(
                            map.get(UserSessionManager.KEY_USERID).toString(),
                            map.get(UserSessionManager.KEY_USERNAME).toString(),
                            map.get(UserSessionManager.KEY_FIRSTNAME).toString(),
                            map.get(UserSessionManager.KEY_LASTNAME).toString(),
                            map.get(UserSessionManager.KEY_EMAIL).toString(),
                            map.get(UserSessionManager.KEY_PASSWORD).toLowerCase(),
                            map.get(UserSessionManager.KEY_TYPE).toString(),
                            map.get(UserSessionManager.KEY_BRANCHID).toString(),
                            map.get(UserSessionManager.KEY_COMPANYID).toString(),
                            jsonObject.getString("credit").toString(),
                            jsonObject.getString("points").toString(),
                            map.get(UserSessionManager.KEY_IMG).toString(),
                            map.get(UserSessionManager.KEY_MOBILE).toString(),
                            map.get(UserSessionManager.KEY_CITY).toString(),
                            map.get(UserSessionManager.KEY_DOB).toString(),
                            map.get(UserSessionManager.KEY_BRANCH).toString(),
                            map.get(UserSessionManager.KEY_DAY).toString(),
                            map.get(UserSessionManager.KEY_Curreny).toString(),
                            jsonObject.getString("gst").toString(),
                            jsonObject.getString("gstformat").toString());
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog != null) {
                progressDialog.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                 //   Log.e("KEY_GSTFORMAT",""+map.get(UserSessionManager.KEY_GSTFORMAT).toString());
                    credit.setText(map.get(UserSessionManager.KEY_CREDIT).toString());
                    pointp.setText(map.get(UserSessionManager.KEY_POINTS).toString());
                    if (data_code.equals("202")) {
                        if(dayAppointmentBean.size()>0) {
                            dayAppoinmentAdapter = new DayAppoinmentAdapter(dayAppointmentBean, CustomerNavigation.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CustomerNavigation.this);
                            apoinmentdetail.setLayoutManager(mLayoutManager);
                            apoinmentdetail.setAdapter(dayAppoinmentAdapter);
                        }else{
                            apoinmentdetail.setVisibility(View.GONE);
                        }

                        if(bday_image!=null && bday_title!=null) {
                            bdayimg.setVisibility(View.VISIBLE);
                            bdaytitle.setVisibility(View.VISIBLE);
                            Glide.with( getApplicationContext() ).load( Webservice.img_path+bday_image ).placeholder( R.drawable.pronoimg ).into(bdayimg  );
                            bdaytitle.setText("Title : "+bday_title);
                        }else{
                            bdayimg.setVisibility(View.GONE);
                            bdaytitle.setVisibility(View.GONE);
                        }
                    }else {
                        apoinmentdetail.setVisibility(View.GONE);
                        demo.setVisibility(View.VISIBLE);
                    }
                }

            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        getAppointmentApi();
    }

    private void getAppointmentApi() {
        if(detector.isConnectingToInternet()) {
            Log.e("userid",map.get( UserSessionManager.KEY_USERID ));
            Log.e("branchid",map.get( UserSessionManager.KEY_BRANCHID ));
            new GetAppoinment().execute( map.get( UserSessionManager.KEY_BRANCHID ).toString(),map.get( UserSessionManager.KEY_USERID ).toString() );
        }
    }
}
