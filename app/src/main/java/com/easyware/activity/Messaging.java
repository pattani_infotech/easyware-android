package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.easyware.easyware.R;
import com.easyware.adapter.NewChatAdapter;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;
import com.easyware.util.ChatBubble;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Messaging extends AppCompatActivity {


    private ListView listView;
    private View btnSend;
    private EditText editText;
    boolean myMessage = true;
    Toolbar toolbar;
    TextView tprofile;
    ImageButton back;
    private ArrayList<ChatBubble> ChatBubbles = new ArrayList<>();

    NewChatAdapter newChatAdapter;

    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    String msgid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        detector = new ConnectionDetector(Messaging.this);
        manager = new UserSessionManager(Messaging.this);
        map = manager.getUserDetail();
        SetUpViews();
        if (detector.isConnectingToInternet()) {
            new Getmessage().execute(map.get(UserSessionManager.KEY_USERID));
        }
    }


    private void SetUpViews() {

        listView = (ListView) findViewById(R.id.list_msg);
        btnSend = findViewById(R.id.btn_chat_send);
        editText = (EditText) findViewById(R.id.msg_type);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().trim().equals("")) {
                    Toast.makeText(Messaging.this, "Please input some text...", Toast.LENGTH_SHORT).show();
                } else {
                    if (detector.isConnectingToInternet()) {
                        ChatBubble ChatBubble = new ChatBubble();
                        ChatBubble.setMe(true);
                        ChatBubble.setMessage(editText.getText().toString());
                        ChatBubble.setId(map.get(UserSessionManager.KEY_USERID).toString());
                        ChatBubble.setDateTime(Webservice.ChatCurrentDate());
                        ChatBubbles.add(ChatBubble);

                        new SendMessage().execute(editText.getText().toString());
                        editText.setText("");

                        if (listView.getAdapter() != null) {
                            newChatAdapter.notifyDataSetChanged();
                        } else {
                            newChatAdapter = new NewChatAdapter(ChatBubbles, Messaging.this);
                            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                            listView.setAdapter(newChatAdapter);
                        }

                    } else {
                        Toast.makeText(Messaging.this, "Please check your internet coneection", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

    }


    private class Getmessage extends AsyncTask<String, String, String> {
        private String response;
        AlertDialog progressDialog;
        String data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(Messaging.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0];
            response = JsonParser.GetJsonFromURL("", Webservice.message + param);
           // Log.e("message",response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    data_code = jsonObject.getString("data_code");

                    if (data_code.equals("202")) {
                        if (jsonObject.has("msgid")) {
                            msgid = jsonObject.getString("msgid");
                        } else {
                            msgid = "na";
                        }
                        if (!jsonObject.isNull("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);

                                ChatBubble ChatBubble = new ChatBubble();
                                ChatBubble.setId(obj.getString("sender_id"));
                                ChatBubble.setMessage(obj.getString("message_text"));
                                ChatBubble.setDateTime(obj.getString("created"));
                                if (obj.getString("sender_id").equals(map.get(UserSessionManager.KEY_USERID))) {
                                    ChatBubble.setMe(true);
                                } else {
                                    ChatBubble.setMe(false);
                                }

                                ChatBubbles.add(ChatBubble);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                progressDialog.dismiss();
                if (data_code != null) {
                    if (data_code.equals("202")) {
                        newChatAdapter = new NewChatAdapter(ChatBubbles, Messaging.this);
                        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                        listView.setAdapter(newChatAdapter);
                    }
                }

            }

        }
    }

    class SendMessage extends AsyncTask<String, String, String> {
        String response, data_code;

        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + map.get(UserSessionManager.KEY_USERID).toString() + "&branch_id=" + map.get(UserSessionManager.KEY_BRANCHID).toString() + "&msg_id=" + msgid + "&msg=" + Uri.encode(params[0], Webservice.ALLOWED_URI_CHARS);
           // Log.e("param",param);
            response = JsonParser.GetJsonFromURL(param, Webservice.SendMessaqe);
            //Log.e("SendMessaqe",response);
            if (response != null) {
                try {
                    JSONObject object = new JSONObject(response);
                    data_code = object.getString("data_code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (data_code != null) {
                if (data_code.equals("202")) {
                    Toast.makeText(Messaging.this, "Your message is sent", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
