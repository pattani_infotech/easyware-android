package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.adapter.DepositAdapter;
import com.easyware.bean.DepositListBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DepositList extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile,nodata;
    ImageButton back;
    RecyclerView deposilist;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    List<DepositListBean> depositListBean=new ArrayList<>( );
    DepositAdapter dAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit_list);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        manager = new UserSessionManager( DepositList.this );
        map = manager.getUserDetail();
        SetUpViews();

        new GetDepositList().execute(map.get(UserSessionManager.KEY_USERID) );
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        tprofile.setText( "Deposit List" );
        back = (ImageButton) findViewById( R.id.back );
        deposilist= (RecyclerView) findViewById(R.id.deposilist);
        nodata=(TextView)findViewById(R.id.nodata);
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }

    private class GetDepositList extends AsyncTask<String, String, String> {
        private String response;
        AlertDialog progressDialog;
        String data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DepositList.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param ="user_id="+params[0];
            response = JsonParser.GetJsonFromURL("", Webservice.Deposit_List+param);
           // Log.e("Deposit List",response);
            if (response != null){
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    data_code = jsonObject.getString( "data_code" );
                    if (data_code.equals( "202" )){
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject obj = jsonArray.getJSONObject( i );
                            DepositListBean bean = new DepositListBean();
                            bean.setId(obj.getString( "id" ));
                            Log.e("amount",obj.getString( "amount" ));
                            bean.setAmount(obj.getString( "amount" ));
                            bean.setCreate(obj.getString( "create" ));
                            depositListBean.add(bean);
                        }
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            if (response!=null){
                if (data_code.equals("202")){
                    dAdapter = new DepositAdapter( depositListBean, DepositList.this );
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( DepositList.this );
                    deposilist.setLayoutManager( mLayoutManager );
                    deposilist.setAdapter(dAdapter);
                }else {
                 deposilist.setVisibility(View.GONE);
                    nodata.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
