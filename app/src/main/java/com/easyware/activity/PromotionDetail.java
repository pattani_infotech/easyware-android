package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.easyware.adapter.CommentAdapter;
import com.easyware.bean.CommentBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyware.service.Webservice.FormatDate;

public class PromotionDetail extends AppCompatActivity {

    private LinearLayout hidden_layout;
    private ImageView promo_img, share;
    private LinearLayout content_layout;
    List<CommentBean> commentBeen ;
    private ListView list_msg;
    ImageButton back,btnSend;
    TextView tprofile, promo_tital2, promo_desc2;
    private Toolbar toolbar;
    private CommentAdapter commentAdapter;
    private EditText msg_type;


    String id,pid;
    String tital;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    int totalcomments;
    String cid[];
    String cuser_id[];
    String comment[];
    String created[];
    private TextView total_comments;
    private TextView total_comments_text;
String filepath,pro_id;
    private String title;
    private String filepath_local;
    private ProgressBar prbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_promotion_detail );
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );

        SetUpViews();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        detector = new ConnectionDetector( PromotionDetail.this );
        manager = new UserSessionManager( PromotionDetail.this );
        map = manager.getUserDetail();

        commentBeen= new ArrayList<>();

        SetData();

        if(detector.isConnectingToInternet())
        {
            new GetPramotionDetail().execute( id, map.get( UserSessionManager.KEY_USERID ) );
        }


    }

    private void SetData() {

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString( "id" );
        tprofile.setText( bundle.getString( "title" ) );
        promo_tital2.setText( bundle.getString( "title" ) );
        promo_desc2.setText( bundle.getString( "des" ) );
        pid=id;
        title=bundle.getString( "title" );
        filepath=bundle.getString( "image" ).toString();
       // Log.e("filepath",filepath);


        Glide.with( PromotionDetail.this )
                .load(filepath ).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                prbar.setVisibility(View.VISIBLE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                prbar.setVisibility(View.GONE);
                return false;
            }
        }).into(promo_img);

       // SaveImage();
        new DownloadFileFromURL().execute(filepath);
    }



    class DownloadFileFromURL extends AsyncTask<String, String, String> {



        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {


                File folder = new File(Environment.getExternalStorageDirectory() +
                        File.separator + "EasywareApp");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdirs();
                }
                if (success) {
                   // Log.e("folderpath",folder.getAbsolutePath()+" "+"");
                } else {
                   // Log.e("folderpath","else");
                }

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(folder.getAbsoluteFile()+"/"
                        + title+".png");
                  filepath_local=folder.getAbsoluteFile()+"/"
                          + title+".png";
                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
               // Log.e("Error: ", e.getMessage());
            }

            return null;
        }

    }

        private void SaveImage() {

    }

    private void SetUpViews() {

        promo_img = (ImageView) findViewById( R.id.promo_img );
        hidden_layout = (LinearLayout) findViewById( R.id.hidden_layout );
        content_layout = (LinearLayout) findViewById( R.id.content_layout );
        list_msg = (ListView) findViewById( R.id.list_msg );
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
        promo_tital2 = (TextView) findViewById( R.id.promo_title2 );
        promo_desc2 = (TextView) findViewById( R.id.promo_desc2 );
        share = (ImageView) findViewById( R.id.share );
        msg_type = (EditText) findViewById( R.id.msg_type );
        total_comments_text=(TextView)findViewById( R.id.total_comments );
        btnSend=(ImageButton)findViewById( R.id.btn_chat_send );

        prbar = (ProgressBar) findViewById( R.id.prbar );



        btnSend.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (msg_type.getText().toString().trim().equals( "" )) {
                    Toast.makeText( PromotionDetail.this, "Please input some comment...", Toast.LENGTH_SHORT ).show();
                } else {
                    if(detector.isConnectingToInternet()) {
                        CommentBean commentBean=new CommentBean();
                        commentBean.setComment(msg_type.getText().toString());
                        commentBeen.add(commentBean);
                        commentBean.setCreated(FormatDate(Webservice.CurrentDate()));
                        commentAdapter.notifyDataSetChanged();

                       new AddsetComment().execute(msg_type.getText().toString());
                        msg_type.setText("");

                    }else{
                        Toast.makeText(PromotionDetail.this,"Please check your internet connection",Toast.LENGTH_LONG).show();
                    }
                }
            }
        } );

    }

    public void AnimTest(View v) {


        switch (v.getId()) {
            case R.id.show_commetns:

                Animation bottomUp = AnimationUtils.loadAnimation( PromotionDetail.this,
                        R.anim.buttom_up );

                hidden_layout.startAnimation( bottomUp );

                hidden_layout.setVisibility( View.VISIBLE );
                content_layout.setVisibility( View.GONE );

                break;  //optional
            case R.id.close_comments:
                Animation bottomDown = AnimationUtils.loadAnimation( PromotionDetail.this,
                        R.anim.buttom_down );

                hidden_layout.startAnimation( bottomDown );
                content_layout.setVisibility( View.VISIBLE );
                hidden_layout.setVisibility( View.GONE );

                total_comments_text.setText("Total Comments: "+totalcomments);
                break;  //optiona
        }


    }

    class GetPramotionDetail extends AsyncTask<String, String, String> {

        AlertDialog progressDialog;
        String responce, data_code;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( PromotionDetail.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
                progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            String param = "id=" + params[0] + "&user_id=" + params[1];
            responce = JsonParser.GetJsonFromURL( param, Webservice.promotion_detail + param );
          //  Log.e("responce",responce+" ");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject( responce );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "200" )) {
                        if(!jsonObject.isNull("comments" )) {
                            JSONArray jsonArray = jsonObject.getJSONArray( "comments" );
                            totalcomments=jsonArray.length();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject( i );
                                CommentBean commentBean=new CommentBean();
                                commentBean.setComment(obj.getString( "comment" ));
                                commentBean.setCreated(obj.getString( "created" ));
                                pro_id=obj.getString( "promotion_id" );
                                commentBeen.add( commentBean);
                        }
                        }else{
                            totalcomments=0;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals( "200" )) {
                        total_comments_text.setText("Total Comments: "+totalcomments);
                        commentAdapter = new CommentAdapter( commentBeen, PromotionDetail.this );
                        list_msg.setAdapter( commentAdapter );
                    }
                }
            }
        }
    }

    public void share(View view) {
        shareBitmap(screenShot(promo_img),"myimage");
    }
    private void shareBitmap (Bitmap bitmap,String fileName) {
        try {
            File file = new File(filepath_local);
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, fOut);
            fOut.flush();
            fOut.close();
            //file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.setType("image/*");
          //  Log.e("share",file.getAbsolutePath().toString());
            startActivity(Intent.createChooser(intent, "send"));
           // Log.e("send image",""+Uri.fromFile(file));
            if(detector.isConnectingToInternet())
            {
                new GetshareAfter().execute( pro_id, map.get( UserSessionManager.KEY_USERID ),map.get(UserSessionManager.KEY_BRANCHID) );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
    class AddsetComment extends AsyncTask<String,String,String> {
        String reponse,data_code;
        @Override
        protected String doInBackground(String... params) {
            String param="user_id="+map.get(UserSessionManager.KEY_USERID).toString()+"&id="+pid.toString()+"&comment="+ Uri.encode(params[0], Webservice.ALLOWED_URI_CHARS);;
            reponse=JsonParser.GetJsonFromURL(param,Webservice.AddComment);
            if(reponse!=null) {
                try {
                    JSONObject object=new JSONObject(reponse);
                    data_code=object.getString("Data_code");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(data_code!=null)
            {
                if(data_code.equals("200"))
                {
                    Toast.makeText(PromotionDetail.this,"Your Comment is sent",Toast.LENGTH_LONG).show();
                    totalcomments=totalcomments+1;
                }
            }
        }
    }
    private class GetshareAfter extends AsyncTask<String,String,String> {
        String reponse, data_code;
        @Override
        protected String doInBackground(String... params) {
            String param = "promotion_id=" + params[0] + "&user_id=" + params[1] + "&branch_id=" + params[2];
            reponse = JsonParser.GetJsonFromURL(param, Webservice.After_share_view);
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

}



