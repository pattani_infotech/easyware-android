package com.easyware.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.adapter.CartProAdapter;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.SharedPreference;
import com.easyware.easyware.UserSessionManager;
import com.easyware.fragment.Cart;
import com.easyware.fragment.MyOrders;
import com.easyware.fragment.Product;

import java.util.HashMap;

import static com.easyware.fragment.Cart.cpBean;
import static com.easyware.fragment.Product.gridview;
import static com.easyware.fragment.Product.listview;

public class ProductList extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    public static TextView counttext;
    ImageButton back, display;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
RelativeLayout cl;
    private TabLayout tabLayout;
    Fragment fragment = null;
    Class fragmentClass = null;
    FragmentManager fragmentManager;
    public static boolean listpress = false;

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    int tipPosition=0;
    public static CartProAdapter cpAdapter;
    SharedPreference sharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        manager = new UserSessionManager(ProductList.this);
        detector = new ConnectionDetector(ProductList.this);
        map = manager.getUserDetail();
        SetUpViews();
        sharedPreference=new SharedPreference(ProductList.this);
        tabLayout.addTab(tabLayout.newTab().setText("Shop"));
        tabLayout.addTab(tabLayout.newTab().setText("Cart"));
        tabLayout.addTab(tabLayout.newTab().setText("My Orders"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        setupTabIcons();
        selecttab();

        /*if (detector.isConnectingToInternet()) {
            //new GetEmallBanner().execute(map.get(UserSessionManager.KEY_BRANCHID));
        } else {
            Webservice.MakeToast("Please Connect your Device to Internet", ProductList.this);
        }
        if (detector.isConnectingToInternet()) {
            new getProduct().execute(map.get(UserSessionManager.KEY_BRANCHID));
        } else {
            Webservice.MakeToast("Please Connect your Device to Internet", ProductList.this);
        }
        SlideImage();*/
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tab.getIcon().setColorFilter(getResources().getColor(R.color.redd,null), PorterDuff.Mode.SRC_IN);
                }else {
                    tab.getIcon().setColorFilter(ContextCompat.getColor(ProductList.this, R.color.redd), PorterDuff.Mode.SRC_IN);
                }


                switch (tab.getPosition()){
                    case 0:
                        tprofile.setText("Shop");
                        fragmentClass = Product.class;
                        display.setVisibility(View.GONE);
                        break;
                    case 1:
                        fragmentClass = Cart.class;
                        tprofile.setText("Cart");
                        display.setVisibility(View.GONE);
                        break;
                    case 2:
                        fragmentClass = MyOrders.class;
                        tprofile.setText("My Orders");
                        display.setVisibility(View.GONE);
                        break;
                }
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabLayout.getTabAt(0).getIcon().clearColorFilter();
                tab.getIcon().clearColorFilter();
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        if (sharedPreference.loadProduct(ProductList.this)!=null) {
            cpBean = sharedPreference.loadProduct(ProductList.this);
            cpAdapter = new CartProAdapter(cpBean, ProductList.this);
            cpAdapter.notifyDataSetChanged();

            if (cpAdapter.getCount() != 0) {
              //Log.e("123", cpAdapter.getCount() + "");
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    counttext.setBackground(getResources().getDrawable(R.drawable.roundc, null));
                }else{
                    counttext.setBackground(ContextCompat.getDrawable(ProductList.this,R.drawable.roundc));
                }
                counttext.setText("" + cpAdapter.getCount());
            } else {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    counttext.setBackgroundColor(getResources().getColor(R.color.trans, null));
                }else {
                    counttext.setBackgroundColor(ContextCompat.getColor(ProductList.this,R.color.trans));
                }

            }
        }

    }



    private void selecttab() {

        if(getIntent().hasExtra("cart"))
        {

            tabLayout.getTabAt(0).getIcon().clearColorFilter();
            tabLayout.getTabAt(1).select();
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_cart_new)
                    .getIcon().setColorFilter(ContextCompat.getColor(ProductList.this, R.color.redd) , PorterDuff.Mode.SRC_IN);
            fragmentClass = Cart.class;
            tprofile.setText("Cart");
            display.setVisibility(View.GONE);


        }else if(getIntent().hasExtra("myorder"))
        {
            tabLayout.getTabAt(0).getIcon().clearColorFilter();
            tabLayout.getTabAt(2).select();
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_account)
            .getIcon().setColorFilter(ContextCompat.getColor(ProductList.this, R.color.redd) , PorterDuff.Mode.SRC_IN);
            fragmentClass = MyOrders.class;
            tprofile.setText("My Orders");
            display.setVisibility(View.GONE);

        }else {

            fragmentClass = Product.class;

        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();

    }





    private void setupTabIcons() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           tabLayout.getTabAt(0).setIcon(R.drawable.ic_product_new).getIcon().setColorFilter(ContextCompat.getColor(ProductList.this, R.color.redd) , PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_cart_new);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_account);
            tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);

        }else{
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_product_new).getIcon().setColorFilter(ContextCompat.getColor(ProductList.this, R.color.redd)
                    , PorterDuff.Mode.SRC_IN);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_cart_new);
            tabLayout.getTabAt(2).setIcon(R.drawable.ic_account);
            tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);
        }

    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        counttext = (TextView) findViewById(R.id.counttext);
        tprofile.setText("Shop");
        back = (ImageButton) findViewById(R.id.back);
        display = (ImageButton) findViewById(R.id.display);
        display.setVisibility(View.GONE);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        cl= (RelativeLayout) findViewById(R.id.cl);
        cl.setVisibility(View.VISIBLE);

        cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                tabLayout.getTabAt(0).getIcon().clearColorFilter();
                tabLayout.getTabAt(1).select();
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_cart_new)
                        .getIcon().setColorFilter(ContextCompat.getColor(ProductList.this, R.color.redd) , PorterDuff.Mode.SRC_IN);
                fragmentClass = Cart.class;
                tprofile.setText("Cart");
                display.setVisibility(View.GONE);

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listpress) {
                    listpress = false;
                    //Log.e("false", "" + listpress);
                    gridview.setVisibility(View.VISIBLE);
                    listview.setVisibility(View.GONE);
                    gridview.setTextFilterEnabled(true);
                } else {
                    listpress = true;
                    //Log.e("true", "" + listpress);
                    gridview.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);
                    listview.setTextFilterEnabled(true);
                }
            }
        });

  /*if(getIntent().hasExtra("myorder"))
  {
      fragmentClass = MyOrders.class;
  }else{
      fragmentClass = Product.class;

  }*/

        fragmentClass = Product.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();
    }
    /*private class getProduct extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ProductList.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Emall_Products);
            Log.e("Emall_Products", responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                ProductBean bean = new ProductBean();
                                bean.setId(obj.getString("id"));
                                bean.setTitle(obj.getString("title"));
                                bean.setDescription(obj.getString("description"));
                                bean.setType(obj.getString("type"));
                                // bean.setStatus(obj.getString("status"));
                                bean.setCreated(obj.getString("created"));
                                bean.setCategory_id(obj.getString("category_id"));
                                bean.setProd_code(obj.getString("prod_code"));
                                bean.setPrice(obj.getString("price"));
                                bean.setProm_price(obj.getString("prom_price"));
                                bean.setQty(obj.getString("qty"));
                                bean.setImage(obj.getString("image"));
                                bean.setVideo(obj.getString("video"));

                                productBean.add(bean);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        pAdapter = new ProductsAdapter(productBean, ProductList.this);
                        *//*listview.setAdapter(pAdapter);
                        gridview.setVisibility(View.VISIBLE);*//*
                        if (listpress) {
                            listview.setVisibility(View.VISIBLE);
                            listview.setAdapter(pAdapter);
                        } else {
                            gridview.setVisibility(View.VISIBLE);
                            gridview.setAdapter(pAdapter);
                        }
                    } else {
                        listview.setVisibility(View.GONE);
                        gridview.setVisibility(View.GONE);
                        nodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }*/
    /*private class GetEmallBanner extends AsyncTask<String, String, String> {
        AlertDialog pd;
        String responce, data_code;
        int tipPosition = 0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ProductList.this);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Emall_Banner);
            Log.e("Emall_Banner", responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("error");
                    if (data_code.equals("200")) {
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                BannerBean bean = new BannerBean();
                                bean.setImage(obj.getString("image"));
                                bean.setTitle(obj.getString("title"));
                                bean.setDescription(obj.getString("description"));
                                bean.setSort(obj.getString("sort"));
                                bannerBean.add(bean);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pd != null) {
                pd.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        if (bannerBean.size() != 0) {
                            for (int i = 0; i < bannerBean.size(); i++) {
                                tipPosition=i;
                                HashMap<String, String> file_maps = new HashMap<String, String>();
                                file_maps.put(String.valueOf(i), Webservice.img_path + bannerBean.get(i).getImage());
                                *//*HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
                                file_maps.put("1", R.drawable.slideimage);*//*
                                for (String name : file_maps.keySet()) {
                                    DefaultSliderView defaultSliderView = new DefaultSliderView(ProductList.this);
                                    defaultSliderView.image(file_maps.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
                                    ebanner.addSlider(defaultSliderView);
                                }
                                ebanner.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                ebanner.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                                ebanner.setCustomAnimation(new DescriptionAnimation());
                                ebanner.setDuration(2500);

                                final Handler tipsHanlder = new Handler();
                                Runnable tipsRunnable = new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        //set number of tip(randon/another way)
                                        title.setText(tipPosition+" : "+bannerBean.get(tipPosition).getTitle());
                                        disc.setText(tipPosition+" : "+bannerBean.get(tipPosition).getDescription());
                                        tipsHanlder.postDelayed(this, 2498);
                                    }
                                };
                                tipsHanlder.post(tipsRunnable);
                            }
                        }
                    }
                }
            }
        }
    }*/
    /*private void SlideImage() {

        for (int i = 0; i < 4; i++) {
            BannerBean bean = new BannerBean();
            bean.setImage("bbd226cb7cf3d64a014d83fd876e4b8d.JPG");
            bean.setTitle("title : "+(i+1));
            bean.setDescription("description : "+(i+1));
            bean.setSort("sort");
            bannerBean.add(bean);
        }
        *//*for (int i = 0; i < bannerBean.size(); i++) {
            tipPosition=i;
            HashMap<String, String> file_maps = new HashMap<String, String>();
            file_maps.put(String.valueOf(i), Webservice.img_path + bannerBean.get(i).getImage());
            for (String name : file_maps.keySet()) {
               DefaultSliderView defaultSliderView = new DefaultSliderView(ProductList.this);
                defaultSliderView.image(file_maps.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
                ebanner.addSlider(defaultSliderView);
            }
            ebanner.setPresetTransformer(SliderLayout.Transformer.Accordion);
            ebanner.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            ebanner.setCustomAnimation(new DescriptionAnimation());
            ebanner.setDuration(2500);
        }*//*
        *//*final Handler tipsHanlder = new Handler();
        Runnable tipsRunnable = new Runnable() {
            @Override
            public void run() {
                //set number of tip(randon/another way)
                title.setText(bannerBean.get(tipPosition).getTitle());
                disc.setText(bannerBean.get(tipPosition).getDescription());
                tipsHanlder.postDelayed(this, 2498);
            }
        };
        tipsHanlder.post(tipsRunnable);*//*
        final MyAdapter adapter=new MyAdapter(ProductList.this,bannerBean);
        ebannerv.setAdapter(adapter);
        abannerv.setAdapter(adapter);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                if(j <adapter.getCount()) {
                    *//*title.setText(bannerBean.get(j).getTitle());
                    disc.setText(bannerBean.get(j).getDescription());*//*
                    ebannerv.setCurrentItem(j);
                    abannerv.setCurrentItem(j);
                    j++;
                } else {
                    j =0;
                    //viewpager.setCurrentItem(j);
                }

                handler.postDelayed(this, 2000);

            }
        }, 200);
    }

    private class MyAdapter extends PagerAdapter {
        private List<BannerBean> bannerBean;
        private LayoutInflater inflater;
        private Context context;

        public MyAdapter(Context context, List<BannerBean> bannerBean) {
            this.context = context;
            this.bannerBean=bannerBean;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return bannerBean.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View myImageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);
            ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);
            TextView title = (TextView) myImageLayout.findViewById(R.id.title);
            TextView disc = (TextView) myImageLayout.findViewById(R.id.disc);
            //myImage.setImageResource(bannerBean.get(position).getImage());
            Glide.with(context).load(Webservice.img_path + bannerBean.get(position).getImage())
                    .thumbnail(0.5f)
                    .into(myImage);
            title.setText(bannerBean.get(position).getTitle());
            disc.setText(bannerBean.get(position).getDescription());
            view.addView(myImageLayout, 0);
            return myImageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }
        @Override
        public Parcelable saveState() {
            return null;
        }
    }*/
}
