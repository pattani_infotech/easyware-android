package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.PointReportListAdapter;
import com.easyware.bean.PointReportListBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PointsReports extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    RecyclerView point_report;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private PointReportListAdapter pointReportListAdapter;
    private List<PointReportListBean> pointReportListBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_reports);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
        SetUpViews();
        manager = new UserSessionManager( PointsReports.this );
        detector=new ConnectionDetector(PointsReports.this);
        map=manager.getUserDetail();
        pointReportListBean=new ArrayList<>( );
        if(detector.isConnectingToInternet()) {
            new GetPoints_Report().execute( map.get( UserSessionManager.KEY_USERID ) );
        }
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        point_report= (RecyclerView) findViewById(R.id.point_report);
        tprofile.setText("Points Reports");
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }
    private class GetPoints_Report extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PointsReports.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0];
            responce = JsonParser.GetJsonFromURL("", Webservice.points_Report + param );
            //Log.e("kkkkkk",responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject( responce );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "200" )) {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            PointReportListBean bean=new PointReportListBean();
                            bean.setId(obj.getString( "id" ));
                            bean.setPoints(obj.getString( "points" ));
                            bean.setName(obj.getString( "name" ));
                            bean.setRemarks(obj.getString( "remarks" ));
                            bean.setCreated(obj.getString( "created" ));
                            pointReportListBean.add(bean);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            if (responce != null) {
                if (data_code.equals( "200" )) {
                    pointReportListAdapter = new PointReportListAdapter(pointReportListBean, PointsReports.this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PointsReports.this);
                    point_report.setLayoutManager(mLayoutManager);
                    point_report.setAdapter(pointReportListAdapter);
                }
            }
        }
    }
}
