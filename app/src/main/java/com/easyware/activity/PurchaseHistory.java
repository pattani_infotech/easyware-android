package com.easyware.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.adapter.PurchaseHistoryAdapter;
import com.easyware.bean.PurchaseHistoryBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyware.easyware.R.id.TotalData;


public class PurchaseHistory extends AppCompatActivity {
Toolbar toolbar;
    ImageButton back;
    TextView tprofile,rec_no_title,Total_tile,nodata;
    RecyclerView purHistory;
    List<PurchaseHistoryBean> purchaseHistoryBean ;//=new ArrayList<>();
    UserSessionManager manager;//=new UserSessionManager(PurchaseHistory.this);
    PurchaseHistoryAdapter purAdapter;
    ConnectionDetector connectionDetector;
    HashMap<String,String> map;
    RelativeLayout datalayout;
    double total=0;
    private TextView purchase_total;
    private RelativeLayout total_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_history);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SetUpViews();

        manager=new UserSessionManager(PurchaseHistory.this);
        connectionDetector=new ConnectionDetector(PurchaseHistory.this);
        map=manager.getUserDetail();
        purchaseHistoryBean =new ArrayList<>();

        if(connectionDetector.isConnectingToInternet())
        {

             String params="user_id="+map.get(UserSessionManager.KEY_USERID).toString();
            if(getIntent().getStringExtra("title").equalsIgnoreCase("purchase"))
            {
                new GetPurchaseHistory().execute(params, Webservice.purchase_history);
            }else{
                new GetPurchaseHistory().execute(params, Webservice.product_history);
            }
        }

       /* */
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView)findViewById(R.id.tprofile);
        back = (ImageButton)toolbar.findViewById(R.id.back);
        total_layout=(RelativeLayout)findViewById(R.id.rl3);
        rec_no_title=(TextView)findViewById(R.id.rec_no);
        Total_tile=(TextView)findViewById(R.id.Total);
        nodata=(TextView)findViewById(R.id.nodata);
        datalayout= (RelativeLayout) findViewById(R.id.datalayout);
        if(getIntent().getStringExtra("title").equalsIgnoreCase("purchase"))
        {
            tprofile.setText("Purchase History");
        }else {
            tprofile.setText("Product History");
            rec_no_title.setText("Product Title");
            Total_tile.setText("Receipt No");
            total_layout.setVisibility(View.GONE);
        }

        purHistory= (RecyclerView) findViewById(R.id.purHistory);
        purchase_total=(TextView)findViewById(TotalData);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    onBackPressed();
            }
        });
    }


    class GetPurchaseHistory extends AsyncTask<String,String,String>{

        String response,code;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=new ProgressDialog(PurchaseHistory.this);
            dialog.setMessage("Please wait..");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            response= JsonParser.GetJsonFromURL(strings[0],strings[1]);
          //  Log.e("purchage history",response);
            if(response!=null)
            {
                try {
                    JSONObject object=new JSONObject(response);
                    code=object.getString("Data_code");
                    if(code.equals("202")){
                        JSONArray jsonArray=object.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject object1=jsonArray.getJSONObject(i);
                            PurchaseHistoryBean bean=new PurchaseHistoryBean();
                            bean.setRec_no(object1.getString("rec_no").toString());
                            bean.setPur_id(object1.getString("id").toString());
                            if(object1.has("total"))
                            {
                                bean.setTotal(object1.getString("total").toString());
                                total=total+Double.parseDouble(object1.getString("total").toString());
                            }
                            if(object1.has("name"))
                            {
                                bean.setName(object1.getString("name").toString());
                            }
                            if(object1.has("title"))
                            {
                                bean.setPur_title(object1.getString("title").toString());
                            }

                            purchaseHistoryBean.add(bean);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();

            if(response!=null)
            {
                if(code!=null)
                {
                    if(code.equals("202"))
                    {

                        purchase_total.setText(map.get(UserSessionManager.KEY_Curreny)+total);
                        purAdapter = new PurchaseHistoryAdapter(purchaseHistoryBean, PurchaseHistory.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(PurchaseHistory.this);
                        purHistory.setLayoutManager(mLayoutManager);
                        purHistory.setAdapter(purAdapter);
                    }else {
                        datalayout.setVisibility(View.GONE);
                        nodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

}
