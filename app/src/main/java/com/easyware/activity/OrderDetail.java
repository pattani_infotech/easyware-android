package com.easyware.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class OrderDetail extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    LinearLayout addOrderLayout;
    TextView reference,product,qty,unitprice,totalprice,totalproduct,recno;
    double tprice = 0;
    private RelativeLayout gstlayout;
    TextView gstrate,amount,tax;
    private String gst_amt;
    private String gst_tax;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        manager = new UserSessionManager( OrderDetail.this );
        map = manager.getUserDetail();
        SetUpViews();
        if(map.get(UserSessionManager.KEY_GSTFORMAT).toString().equalsIgnoreCase("inc")){
            gstlayout.setVisibility(View.VISIBLE);
        }else {
            gstlayout.setVisibility(View.GONE);
        }
        if (getIntent().hasExtra("data")){
            if (!getIntent().getStringExtra("data").equals("")){
                GetOrderDetail(getIntent().getStringExtra("data"));
            }else {}
        }
    }

    private void GetOrderDetail(String data) {
        if (data!=null){
            try {
                JSONArray array=new JSONArray(data);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj = array.getJSONObject(i);
                    RelativeLayout layout = (RelativeLayout) View.inflate(OrderDetail.this, R.layout.new_order_layout, null);
                    reference = ((TextView) layout.findViewById(R.id.reference));
                    product = ((TextView) layout.findViewById(R.id.product));
                    qty = ((TextView) layout.findViewById(R.id.qty));
                    unitprice = ((TextView) layout.findViewById(R.id.unitprice));
                    totalprice = ((TextView) layout.findViewById(R.id.totalprice));
                    recno.setText("Reference No. : "+getIntent().getStringExtra("orderno"));
                    String order_ref="<font color='#ad013d'>"+getIntent().getStringExtra("orderno")+ "</font>";
                    if (Build.VERSION.SDK_INT >= 24) {
                        reference.setText(Html.fromHtml(order_ref, Html.FROM_HTML_MODE_LEGACY));
                    }else {
                        reference.setText(Html.fromHtml(order_ref));
                    }

                    product.setText(obj.getString("title"));
                    qty.setText(obj.getString("qty"));
                    unitprice.setText(map.get(UserSessionManager.KEY_Curreny)+ Webservice.GetDecimalFormate(obj.getString("price")));
                    totalprice.setText(map.get(UserSessionManager.KEY_Curreny)+Webservice.GetDecimalFormate(Calculation(obj.getString("price"),obj.getString("qty"))));

                    tprice = tprice + (Double.parseDouble(Calculation(obj.getString("price"),obj.getString("qty"))));
                    totalproduct.setText( map.get(UserSessionManager.KEY_Curreny)+Webservice.GetDecimalFormate(String.valueOf(tprice)));
                    addOrderLayout.addView(layout, i);
                    gstrate.setText("SR"+" "+map.get(UserSessionManager.KEY_GST).toString()+"%");

                    double s_total=0;
                    s_total=(tprice*Double.parseDouble(map.get(UserSessionManager.KEY_GST)))/100;
                    gst_tax=map.get(UserSessionManager.KEY_Curreny).toString()+s_total;
                    double _gst_amt=tprice-s_total;
                    gst_amt=map.get(UserSessionManager.KEY_Curreny).toString()+_gst_amt;
                    amount.setText(gst_amt);
                    tax.setText(gst_tax);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String Calculation(String price, String qty) {
        double Itotal;
        Itotal=(Double.parseDouble(price)*Double.parseDouble(qty));
        String total= String.valueOf(Itotal);
        return total;
    }

    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
        tprofile.setText("Order Details");
        addOrderLayout = (LinearLayout) findViewById(R.id.addOrderLayout);
        totalproduct = (TextView) findViewById(R.id.totalproduct);
        recno = (TextView) findViewById(R.id.recno);

        gstlayout=(RelativeLayout)findViewById(R.id.gstlayout);
        gstrate=(TextView)findViewById(R.id.gstrate);
        amount=(TextView)findViewById(R.id.amount);
        tax=(TextView)findViewById(R.id.tax);
    }
}
