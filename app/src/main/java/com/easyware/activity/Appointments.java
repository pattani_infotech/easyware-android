package com.easyware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.AppointMentAdapter;
import com.easyware.bean.AppointMentBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Appointments extends AppCompatActivity {
    TextView tappointments,date;
    ImageButton back;
    EditText notes,time;
    Spinner spinner;
    ConnectionDetector detector;

    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    Toolbar toolbar;
     SimpleDateFormat formatter;
    UserSessionManager manager;
    HashMap<String,String> map;
    List<AppointMentBean> appointMentBeen;
    private RecyclerView apppointment_data;
    private AppointMentAdapter appoint_Adapter;


    private void setCustomResourceForDates(int date,int month,int year) {

        Calendar cal = Calendar.getInstance();
        cal.set(year,month,date);
        Date blueDate = cal.getTime();
        if (caldroidFragment != null) {
            ColorDrawable blue = new ColorDrawable(getResources().getColor(R.color.red));
            caldroidFragment.setBackgroundDrawableForDate(blue, blueDate);
          caldroidFragment.setTextColorForDate(R.color.white, blueDate);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        tappointments = (TextView)toolbar.findViewById(R.id.tappointments);
        back = (ImageButton) toolbar.findViewById(R.id.back);

        detector=new ConnectionDetector(Appointments.this);
        manager=new UserSessionManager(Appointments.this);
        map=manager.getUserDetail();
        appointMentBeen =new ArrayList<>();
        apppointment_data = (RecyclerView) findViewById( R.id.apppointment_data );

        if(detector.isConnectingToInternet())
        {
            String params="user_id="+map.get(UserSessionManager.KEY_USERID).toString();
            new GetAppointMent().execute(params,Webservice.GetCurrentMonthAppointMent);
        }

      formatter = new SimpleDateFormat("yyyy/MM/dd");
        caldroidFragment = new CaldroidFragment();
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            caldroidFragment.setArguments(args);
        }

        final CaldroidListener listener = new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {
                String selecteddate=formatter.format(date);
               // Log.e("2106",selecteddate+"");
                if(detector.isConnectingToInternet()) {
                     String params="user_id="+map.get(UserSessionManager.KEY_USERID).toString()+"&date="+selecteddate;
                    new GetAppmoinmentByDate().execute(params,Webservice.GetAppointmentBuyDate);
                    appointMentBeen.clear();
                }else{
                    Webservice.MakeToast(getResources().getString(R.string.checkInternet),Appointments.this);
                }
                }
            @Override
            public void onChangeMonth(int month, int year) {
                String text = "month: " + month + " year: " + year;
            }
            @Override
            public void onLongClickDate(Date date, View view) {}
            @Override
            public void onCaldroidViewCreated() {
                if (caldroidFragment.getLeftArrowButton() != null) {
                }
            }
        };
        caldroidFragment.setCaldroidListener(listener);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }
        if (dialogCaldroidFragment != null) {
            dialogCaldroidFragment.saveStatesToKey(outState,
                    "DIALOG_CALDROID_SAVED_STATE");
        }
    }


    class GetAppointMent extends AsyncTask<String,String,String> {
        String params,response,code;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=new ProgressDialog(Appointments.this);
            dialog.setMessage("PLease wait while we load your appointments..");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            response= JsonParser.GetJsonFromURL(params[0],params[1]);
            if(response!=null)
            {
                try {
                    JSONObject object=new JSONObject(response);
                    code=object.getString("data_code").toString();
                    if(object.getString("data_code").equals("200")){
                        JSONArray array=object.getJSONArray("data");
                        for(int i=0;i<array.length();i++)
                        {
                            JSONObject object1=array.getJSONObject(i);
                            setCustomResourceForDates(Integer.parseInt(object1.getString("day").toString()),Integer.parseInt(object1.getString("month").toString())-1,Integer.parseInt(object1.getString("year").toString()));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            FragmentTransaction t1 = getSupportFragmentManager().beginTransaction();
            t1.replace(R.id.calendar1, caldroidFragment);
            t1.commit();
          if(code!=null) {
              if(code.equals("200")) {
                 // Toast.makeText()
              }
          }
            dialog.dismiss();
        }
    }

    class GetAppmoinmentByDate extends AsyncTask<String,String,String> {
        String response;
        private String data_code;
        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=new ProgressDialog(Appointments.this);
            dialog.setMessage("Please wait");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            response=JsonParser.GetJsonFromURL(params[0],params[1]);
            //Log.e("appoint_date",response+" ");
            if(appointMentBeen.size()>0) {
                appointMentBeen.clear();
            }
            if(response!=null){
                //Log.e("general",response);
                try {
                    JSONObject object=new JSONObject(response);
                    data_code=object.getString("data_code");
                    if(data_code.equals("200")) {
                        JSONArray array=object.getJSONArray("data");
                        for(int i=0;i<array.length();i++) {
                            JSONObject object1=array.getJSONObject(i);
                            AppointMentBean bean=new AppointMentBean();
                            bean.setDate(object1.getString("date"));
                            bean.setDescrption(object1.getString("notes"));
                            bean.setStaffid(object1.getString("staff_id"));
                            bean.setTime(object1.getString("time"));
                            bean.setId(object1.getString("id"));
                            bean.setStaff(object1.getString("staff"));
                            bean.setStatus(object1.getString("status"));
                            appointMentBeen.add(bean);
                           /* Log.e("bean",object1.getString("date"));
                            Log.e("bean",object1.getString("notes"));
                            Log.e("bean",object1.getString("staff_id"));
                            Log.e("bean",object1.getString("time"));
                            Log.e("bean",object1.getString("staff"));
                            Log.e("bean",object1.getString("status"));*/
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            if(response!=null) {
                if(data_code!=null) {
                    if(data_code.equals("200")) {
                        apppointment_data.setVisibility(View.VISIBLE);
                            appoint_Adapter = new AppointMentAdapter(appointMentBeen, Appointments.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Appointments.this);
                            apppointment_data.setLayoutManager(mLayoutManager);
                            apppointment_data.setAdapter(appoint_Adapter);


                    }else{
                        Webservice.MakeToast("No Booking found selected date",Appointments.this);
                        apppointment_data.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public void AddAppointment(View v) {
            startActivity(new Intent(Appointments.this,AddNewAppointment.class));
             finish();
    }
}
