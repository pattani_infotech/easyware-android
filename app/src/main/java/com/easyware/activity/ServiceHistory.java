package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.SHAdapter;
import com.easyware.bean.ServiceHistoryBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServiceHistory extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    RecyclerView servicehistory;
    ListView servicehistorynew;
    private List<ServiceHistoryBean> serviceHistoryBeen=new ArrayList<>( );
    private SHAdapter serviceHistoryAdapter;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    public static boolean isLoad=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_history);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        manager = new UserSessionManager( ServiceHistory.this );
        map = manager.getUserDetail();
        SetUpViews();
        new GetServiceHistory().execute(map.get(UserSessionManager.KEY_USERID) );
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        tprofile.setText( "Service History" );
        back = (ImageButton) findViewById( R.id.back );
        servicehistory= (RecyclerView) findViewById(R.id.servicehistory);
        servicehistorynew= (ListView) findViewById(R.id.servicehistorynew);
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }
    class GetServiceHistory extends AsyncTask<String, String, String> {
        private String response;
        AlertDialog progressDialog;
        String data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(ServiceHistory.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param ="user_id="+params[0];
            response = JsonParser.GetJsonFromURL(param, Webservice.Service_history);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "202" )) {
                        if (!jsonObject.isNull("data" )) {
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                            JSONArray jsonArray = jsonObject1.getJSONArray( "servlst" );
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                ServiceHistoryBean bean = new ServiceHistoryBean();
                                bean.setId(obj.getString("id"));
                                bean.setRec_no(obj.getString("rec_no"));
                                bean.setTitle(obj.getString("title"));
                                bean.setDate(obj.getString("date"));
                                bean.setUsage(obj.getString("usage"));
                                bean.setUsed(obj.getString("used"));
                                bean.setLoad(false);
                                serviceHistoryBeen.add(bean);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            serviceHistoryAdapter = new SHAdapter( serviceHistoryBeen, ServiceHistory.this );
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( ServiceHistory.this );
            servicehistory.setLayoutManager( mLayoutManager );
            servicehistorynew.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            servicehistorynew.setAdapter( serviceHistoryAdapter );
        }
    }
}
