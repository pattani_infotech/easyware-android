package com.easyware.activity;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyware.easyware.R;
import com.easyware.service.Webservice;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeThumbnailLoader;

public class Test extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
     TextView title, desc;
    ImageView Thumbnail;
    RelativeLayout prmo_container;
    YouTubePlayerView youTubePlayerView;
    ImageButton play;
    private YouTubeThumbnailLoader youTubeThumbnailLoader;
    YouTubePlayer.OnInitializedListener onInitializedListener;
    public static final String VIDEO_ID = "aB-UKwO367s";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        SetUpView();
        /*//youTubePlayerView.initialize(Webservice.API_KEY, onInitializedListener);
        onInitializedListener=new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer.loadVideo(VIDEO_ID);
                    youTubePlayer.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(Test.this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
            }
        };*/


    }

    private void SetUpView() {
        /*title = (TextView) findViewById(R.id.titlet);
        desc = (TextView) findViewById(R.id.desct);
        Thumbnail = (ImageView) findViewById(R.id.Thumbnailt);
        play= (ImageButton) findViewById(R.id.playt);
        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_playert);
        prmo_container = (RelativeLayout) findViewById(R.id.prmo_containert);
        youTubePlayerView.initialize(Webservice.API_KEY, this);*/
         youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_playert);
        youTubePlayerView.initialize(Webservice.API_KEY, this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        if (!wasRestored) {
            player.cueVideo(VIDEO_ID);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
    }
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onPlaying() {
        }

        @Override
        public void onSeekTo(int arg0) {
        }

        @Override
        public void onStopped() {
        }

    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
        }
    };
}