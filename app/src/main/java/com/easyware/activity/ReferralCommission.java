package com.easyware.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.bean.ReferralBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;

import java.util.HashMap;
import java.util.List;

public class ReferralCommission extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    RecyclerView ref_commition;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    private List<ReferralBean> referralBeen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_commission);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
        manager = new UserSessionManager( ReferralCommission.this );
        SetUpViews();
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        ref_commition= (RecyclerView) findViewById(R.id.ref_commition);
        tprofile.setText("Referral Commission");
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }
}
