package com.easyware.activity;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.adapter.PromotionAdapter;
import com.easyware.bean.PromotionBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Promotions extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView promot_data;
    ImageButton back;
    private List<PromotionBean> promotionBeen=new ArrayList<>( );
    private PromotionAdapter promo_Adapter;
    private TextView tprofile;


    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_promotions2 );
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );


        manager = new UserSessionManager( Promotions.this );
        map = manager.getUserDetail();

        SetUpViews();
        new getPromotions().execute(map.get(UserSessionManager.KEY_BRANCHID) );
    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        tprofile.setText( "Promotions" );
        back = (ImageButton) findViewById( R.id.back );
        promot_data = (RecyclerView) findViewById( R.id.promot_data );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }

    class getPromotions extends AsyncTask<String, String, String> {
        private String response;
        AlertDialog progressDialog;
        String data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Promotions.this );
            progressDialog.setMessage( "Loading..." );
            progressDialog.setCancelable( false );
                progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
           String param ="branch_id="+params[0];
            response = JsonParser.GetJsonFromURL( "", Webservice.promotion + param );
         //   Log.e("response",response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "200" )) {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            PromotionBean bean = new PromotionBean();
                            bean.setPromotion_id( obj.getString( "id" ) );
                            bean.setTitle( obj.getString( "title" ) );
                            bean.setDescription( obj.getString( "description" ) );
                            bean.setCreated( obj.getString( "created" ) );
                            bean.setPromotion_img( obj.getString( "image" ) );
                            promotionBeen.add( bean );

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );

            promo_Adapter = new PromotionAdapter( promotionBeen, Promotions.this );
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( Promotions.this );
            promot_data.setLayoutManager( mLayoutManager );
            promot_data.setAdapter( promo_Adapter );
            if(progressDialog != null) {
                progressDialog.dismiss();
            }

        }
    }
}
