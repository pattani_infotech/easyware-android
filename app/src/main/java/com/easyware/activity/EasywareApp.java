package com.easyware.activity;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by user on 23-Mar-18.
 */

public class EasywareApp extends Application {
    private static EasywareApp mInstance;
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=(this); //initialize other plugins

    }
}
