package com.easyware.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Login extends AppCompatActivity {
    EditText username, password;
    Button login,register;
    Spinner spinner;

String fname,lname;

    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String,String> map;
     List<String> type;
    private String day_appoint;
    private int columnIndex;
    private static int PERMISSION_REQUEST_CODE = 1;
    //SpinnerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        SetUpViews();

        detector=new ConnectionDetector(Login.this);
        manager=new UserSessionManager(Login.this);
        map=manager.getUserDetail();

        type = new ArrayList<String>();
        type.add("Select Login Type");
        type.add("Email");
        type.add("Username");
        CheckPermission();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinnner, type);
        adapter.setDropDownViewResource(R.layout.spinnner);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String a = type.get(i).toString();

                int b = spinner.getSelectedItemPosition();
                if (spinner.getSelectedItemPosition() != 0) {
                    //Toast.makeText(getApplicationContext(), "" + b + ":" + a.toString(), Toast.LENGTH_SHORT).show();
                    username.setHint(a.toString());
                    Webservice.type=a.toString();
                }else {

                    Webservice.type="username";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    loginprocess();
                }
                return false;
            }
        });



        password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                    if (motionEvent.getRawX() >= (password.getRight() - password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){
                        password.setTransformationMethod( HideReturnsTransformationMethod.getInstance());
                        password.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_eye,0);
                        return true;
                    }
                    password.setTransformationMethod( PasswordTransformationMethod.getInstance());
                    password.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_open_eye,0);
                }
                return false;
            }
        });


        /*if (map.get(UserSessionManager.KEY_USERID)!=null){
            //startActivity(new Intent(Login.this, CustomerNavigation.class));
            Intent intent=new Intent( Login.this,CustomerNavigation.class );
           // intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
            startActivity( intent );
            finish();
        }*/
    }

    private void SetUpViews() {
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);
        spinner = (Spinner) findViewById(R.id.spinner);
    }
    public void loginprocess() {
        String email = username.getText().toString();
        String pwd = password.getText().toString();
        if (spinner.getSelectedItemPosition() == 0){
            Toast.makeText(Login.this, "Please select Login type...", Toast.LENGTH_SHORT).show();
        }else {
            if (email.isEmpty()) {
                username.setError("Enter a Valid Email Address or UserName");
            } else if (pwd.isEmpty()) {
                password.setError("Pleases Enter Password");
            } else if (detector.isConnectingToInternet()) {
                new getLogin().execute(username.getText().toString(), password.getText().toString(), Webservice.type.toString().toLowerCase());
            } else {
                Toast.makeText(Login.this, "Please Connect your Device to Internet", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void login(View v) {
        loginprocess();
    }

    public void register(View v){

        startActivity(new Intent(Login.this, RegisterScreen.class));
    }

    class getLogin extends AsyncTask<String,String,String> {

        AlertDialog progressDialog;
        String responce,data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog( Login.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();

                        }

                        @Override
                        protected String doInBackground(String... params) {
                            String param="email="+params[0]+"&password="+params[1]+"&type="+params[2]+"&device_id="+Webservice.gcm+"&device_type=Android";
                            responce= JsonParser.GetJsonFromURL(param,Webservice.login);
                            Log.e("login responce",responce+" ");
                            if (responce!=null) {
                                try {
                                    JSONObject jsonObject = new JSONObject( responce );
                                    data_code = jsonObject.getString( "Data_code" );
                                    if(data_code.equals("200")) {
                                        if(!jsonObject.isNull("dayappnt")) {
                                            day_appoint=jsonObject.getString( "dayappnt" );
                                        }else{
                            day_appoint="NA";
                        }
                        String gstformat="na";
                        if(jsonObject.has("gstformat"))
                        {
                            gstformat=jsonObject.getString("gstformat");
                        }
                           manager.createUserLoginSession(
                                   jsonObject.getString( "uid" ),
                                   jsonObject.getString( "username" ),
                                   jsonObject.getString( "fname" ),
                                   jsonObject.getString( "lname" ),
                                   jsonObject.getString( "email" ),
                                   params[1],
                                   params[2],
                                   jsonObject.getString( "branch_id" ),
                                   jsonObject.getString( "company_id" ),
                                   jsonObject.getString( "credit" ),
                                   jsonObject.getString( "points" ),
                                   jsonObject.getString( "avatar" ),
                                   jsonObject.getString( "mobile" ),
                                   jsonObject.getString( "city" ),
                                   jsonObject.getString( "dob" ),
                                   jsonObject.getString( "branch_name" ),
                                   day_appoint,
                                   jsonObject.getString( "currency" ),
                                   jsonObject.getString( "gst" ),
                                   gstformat);
                               fname=jsonObject.getString( "fname" );
                        lname=jsonObject.getString( "lname" );
                        Webservice.currency=jsonObject.getString( "currency" );

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if(progressDialog != null) {
                progressDialog.dismiss();
            }

            if (responce!=null){
                if (data_code!=null){
                    if (data_code.equals("200")){
                        Toast.makeText(Login.this,"WelCome "+fname.toString()+" "+lname.toString(),Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent( Login.this,CustomerNavigation.class );
                      //  intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        startActivity( intent );
                        finish();
                    }else{
                        Toast.makeText(Login.this,"Invalid Credentials",Toast.LENGTH_LONG).show();
                    }
                }
            }
        }


    }

    private boolean CheckPermission(){

        columnIndex = ContextCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (columnIndex == PackageManager.PERMISSION_GRANTED){
            return true;
        }else {
            requestPermission();
            return false;
        }
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(Login.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA
        },PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
