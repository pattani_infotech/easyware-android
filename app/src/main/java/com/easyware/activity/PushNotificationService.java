package com.easyware.activity;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.easyware.easyware.BadgePref;
import com.easyware.easyware.R;
import com.easyware.service.Webservice;
import com.github.arturogutierrez.Badges;
import com.github.arturogutierrez.BadgesNotSupportedException;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Random;

/**
 * Created by Pattani InfoTech & Fitzroya Solutions on 06/02/2016.
 */
public class PushNotificationService extends GcmListenerService {
    Bitmap remote_picture=null;
    BadgePref badgePref;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        //Log.e("onMessageReceived",data.getString("general")+" ");
        //Noti(data.getString("message"));
        badgePref=new BadgePref(PushNotificationService.this);
        int count=badgePref.returnCount();
        //Log.e("onMessageReceived",count+" ");
        badgePref.createUserLoginSession(count+1);
        try {

            Badges.setBadge(this, count+1);

        } catch (BadgesNotSupportedException badgesNotSupportedException) {
        //  Log.e("badge", badgesNotSupportedException.getMessage());
        }
        if(data.getString("general")!=null)
        {
            try {
                JSONObject object=new JSONObject(data.getString("general").toString());
                Noti(object.getString("message"),object.getString("title"),object.getString("type"),object.getString("img"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if(data.getString("appointment")!=null)
        {
            try {
                JSONObject object=new JSONObject(data.getString("general").toString());
                Noti(object.getString("message"),object.getString("title"),object.getString("type"),"img");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
       // Noti(data.getString("general"));
    }
    public void Noti(String message,String title,String type,String image) {
        //Log.e("notification called", "noti");
        Intent intent;
        if(type.equalsIgnoreCase("promotion"))
        {
            intent = new Intent(this, Promotion.class);
        }else if (type.equalsIgnoreCase("message")){
            intent = new Intent(this, Messaging.class);
        }else{
            intent = new Intent(this, CustomerNavigation.class);
        }
        try {
            URL url = new URL(Webservice.img_path+image);
            remote_picture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(IOException e) {
            System.out.println(e);
        }
        //Intent intent = new Intent(this, CustomerNavigation.class);
        //intent.putExtra("msg",message);
        // startActivity(intent);
        // use System.currentTimeMillis() to have a unique ID for the pending intent
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        notiStyle.bigPicture(remote_picture);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder n;
        n = (NotificationCompat.Builder) new  NotificationCompat.Builder(this)
                .setContentTitle("Easyware-"+title)
                .setContentText(message)
                .setStyle(notiStyle)
                .setSmallIcon(R.drawable.logonew)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .setLargeIcon(remote_picture);

        n.setDefaults(new NotificationCompat().DEFAULT_VIBRATE);
        Random rand = new Random();

        int  n1 = rand.nextInt(50) + 1;

        Uri notificationsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        n.setSound(notificationsound);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(n1, n.build());

        /*try {
            URL url = new URL(Webservice.img_path+image);
             remote_picture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch(IOException e) {
            System.out.println(e);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //Bitmap remote_picture = BitmapFactory.decodeResource(filepath);
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();

        notiStyle.bigPicture(remote_picture);
        PendingIntent contentIntent = PendingIntent.getActivity(this,(int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logonew)
                .setContentTitle("Easyware-"+title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Big View Styles"))
                .setContentText(message)
                .setStyle(notiStyle);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        mBuilder.setAutoCancel(true);
        Random rand = new Random();
        int  n1 = rand.nextInt(50) + 1;
        notificationManager.notify(n1, mBuilder.build());*/

    }

}

/*
*  NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Bitmap remote_picture = BitmapFactory.decodeResource(filepath);
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();

        notiStyle.bigPicture(remote_picture);
        PendingIntent contentIntent = PendingIntent.getActivity(this,(int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logonew)
                .setContentTitle("Easyware-"+title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText("Big View Styles"))
                .setContentText(message)
                .setStyle(notiStyle);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        mBuilder.setAutoCancel(true);
        Random rand = new Random();
        int  n1 = rand.nextInt(50) + 1;
        notificationManager.notify(n1, mBuilder.build());*/