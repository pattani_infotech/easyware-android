package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.ClaimsListAdapter;
import com.easyware.bean.ClaimsBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ClaimsList extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    RecyclerView claimlist;
    private List<ClaimsBean> claimsBean;//=new ArrayList<>( );
    private ClaimsListAdapter claimslistAdapter;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    public static String activity_title;
    private TextView Total_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claims_list);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );
        manager = new UserSessionManager( ClaimsList.this );
        map = manager.getUserDetail();
        claimsBean=new ArrayList<>( );
        SetUpViews();
        if(getIntent().getStringExtra("title").equals("claim"))
        {
            activity_title="Claim History";
            String param ="user_id="+map.get(UserSessionManager.KEY_USERID).toString();
            new GetClaimList().execute(param, Webservice.ClaimsList);
        }else{
            activity_title="Course Claim History";
            String param ="user_id="+map.get(UserSessionManager.KEY_USERID).toString();
            new GetClaimList().execute(param, Webservice.course_ClaimsList);
        }

    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        Total_title=(TextView)findViewById(R.id.Total);
        if(getIntent().getStringExtra("title").equals("claim")) {
            activity_title = "Claim History";
            Total_title.setText("Total");

        }else{
            activity_title="Course Claim History";
            Total_title.setText("Date");
        }
        tprofile.setText(activity_title);
        back = (ImageButton) findViewById( R.id.back );
        claimlist= (RecyclerView) findViewById(R.id.claimlist);
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }

    class GetClaimList extends AsyncTask<String, String, String> {
        private String response;
        AlertDialog progressDialog;
        String data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(ClaimsList.this );
            progressDialog.setMessage( "Please wait..." );
            progressDialog.setCancelable( false );
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {

            response = JsonParser.GetJsonFromURL(params[0], params[1]);
            //Log.e("123",response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "202" )) {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            ClaimsBean bean = new ClaimsBean();
                            bean.setId( obj.getString( "id" ) );
                            bean.setRec_no( obj.getString( "rec_no" ) );
                            if(obj.has("total")){
                                bean.setTotal( obj.getString( "total" ) );
                            }else{
                                bean.setDate( obj.getString( "date" ) );
                            }
                            claimsBean.add( bean );
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute( s );
            progressDialog.dismiss();
            claimslistAdapter = new ClaimsListAdapter( claimsBean, ClaimsList.this );
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( ClaimsList.this );
            claimlist.setLayoutManager( mLayoutManager );
            claimlist.setAdapter( claimslistAdapter );
        }
    }

}
