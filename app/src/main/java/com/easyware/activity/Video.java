package com.easyware.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.VideoAdapter;
import com.easyware.bean.VideoBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;
import com.google.android.youtube.player.YouTubeBaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Video extends YouTubeBaseActivity {
    private Toolbar toolbar;
    private TextView tprofile;
    ImageButton back;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    RecyclerView urlvideo;
        private List<VideoBean> videoBean;
    private VideoAdapter videoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN );
        SetUpViews();
        videoBean=new ArrayList<>( );

        manager = new UserSessionManager( Video.this );
        detector=new ConnectionDetector(Video.this);
        map=manager.getUserDetail();

        if(detector.isConnectingToInternet()) {
            new GetVideo().execute( map.get( UserSessionManager.KEY_BRANCHID ) );
        }

    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        tprofile = (TextView) findViewById( R.id.tprofile );
        tprofile.setText("Video");
        urlvideo= (RecyclerView) findViewById(R.id.urlvideo);
        back = (ImageButton) findViewById( R.id.back );
        back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        } );
    }
    private class GetVideo extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Video.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.VideoList);
            //Log.e("VideoList",responce+"");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject( responce );
                    data_code = jsonObject.getString( "Data_code" );
                    if (data_code.equals( "200" )) {
                        JSONArray jsonArray = jsonObject.getJSONArray( "data" );
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject( i );
                            VideoBean bean=new VideoBean();
                            bean.setId(obj.getString( "id" ));
                            bean.setTitle(obj.getString( "title" ));
                            bean.setDescription(obj.getString( "description" ));
                            bean.setUrl(obj.getString( "url" ));
                            videoBean.add(bean);

                        }
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                progressDialog.dismiss();

            if (responce != null) {
                if (data_code.equals("200")) {
                    videoAdapter = new VideoAdapter(videoBean, Video.this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Video.this);
                    urlvideo.setLayoutManager(mLayoutManager);
                    urlvideo.setAdapter(videoAdapter);
                }
            }

        }
    }



}
