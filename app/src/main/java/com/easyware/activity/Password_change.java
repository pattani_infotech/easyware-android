package com.easyware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Password_change extends AppCompatActivity {
    Toolbar toolbar;
    TextView tprofile;
    EditText cur_pwd, new_pwd,conf_pwd;
    UserSessionManager manager;
    HashMap<String,String> map;
    ConnectionDetector connectionDetector;
    ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        pwdcheck();
        manager=new UserSessionManager(Password_change.this);
        map=manager.getUserDetail();
        connectionDetector=new ConnectionDetector(Password_change.this);
        cur_pwd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP){

                    if (motionEvent.getRawX() >= (cur_pwd.getRight() - cur_pwd.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){


                        cur_pwd.setTransformationMethod( HideReturnsTransformationMethod.getInstance());
                        cur_pwd.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_eye,0);

                        return true;
                    }

                    cur_pwd.setTransformationMethod( PasswordTransformationMethod.getInstance());
                    cur_pwd.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_open_eye,0);

                }



                return false;
            }
        });


        new_pwd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP){

                    if (motionEvent.getRawX() >= (new_pwd.getRight() - new_pwd.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){


                        new_pwd.setTransformationMethod( HideReturnsTransformationMethod.getInstance());
                        new_pwd.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_eye,0);

                        return true;
                    }

                    new_pwd.setTransformationMethod( PasswordTransformationMethod.getInstance());
                    new_pwd.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_open_eye,0);

                }



                return false;
            }
        });

        conf_pwd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP){

                    if (motionEvent.getRawX() >= (conf_pwd.getRight() - conf_pwd.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())){


                        conf_pwd.setTransformationMethod( HideReturnsTransformationMethod.getInstance());
                        conf_pwd.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_close_eye,0);

                        return true;
                    }

                    conf_pwd.setTransformationMethod( PasswordTransformationMethod.getInstance());
                    conf_pwd.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_open_eye,0);

                }



                return false;
            }
        });


    }

    private void pwdcheck() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        cur_pwd=(EditText)findViewById(R.id.currentpassword);
        new_pwd=(EditText)findViewById(R.id.password);
        conf_pwd=(EditText)findViewById(R.id.confpassword);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void ChangePwd(View v)
    {
        if(!cur_pwd.getText().toString().equals(map.get(UserSessionManager.KEY_PASSWORD).toString()))
        {
            cur_pwd.setError("Please enter current Password");
        }else if(new_pwd.getText().toString().length()<6)
        {
            new_pwd.setError("Password length should be of 6 or more characters");
        }else if(!new_pwd.getText().toString().equals(conf_pwd.getText().toString()))
        {

            conf_pwd.setError("Password does not match");
        }else{


            if(connectionDetector.isConnectingToInternet())
            {
                String params="user_id="+map.get(UserSessionManager.KEY_USERID).toString()+"&password="+new_pwd.getText().toString();
                //Log.e("ChangePwd",params+" ");
                new ChangePwd().execute(params, Webservice.ChangePwd);
            }else{
                //Log.e("ChangePwd","internt not connectes ");
            }
        }
    }



    class ChangePwd extends AsyncTask<String,String,String>{

        String response,data_code;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=new ProgressDialog(Password_change.this);

            progressDialog.setMessage("Please wait");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            response= JsonParser.GetJsonFromURL(params[0],Webservice.ChangePwd);

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(response!=null)
            {
                try {
                    JSONObject object=new JSONObject(response);
                    if(object.getString("data_code").equals("200")){

                        Webservice.MakeToast("Password Change",Password_change.this);
                        manager.createUserLoginSession(
                                map.get(UserSessionManager.KEY_USERID).toString(),
                                map.get(UserSessionManager.KEY_USERNAME).toString(),
                                map.get(UserSessionManager.KEY_FIRSTNAME).toString(),
                                map.get(UserSessionManager.KEY_LASTNAME).toString(),
                                map.get(UserSessionManager.KEY_EMAIL).toString(),
                                new_pwd.getText().toString(),
                                map.get(UserSessionManager.KEY_TYPE).toString(),
                                map.get(UserSessionManager.KEY_BRANCHID).toString(),
                                map.get(UserSessionManager.KEY_COMPANYID).toString(),
                                map.get(UserSessionManager.KEY_CREDIT).toString(),
                                map.get(UserSessionManager.KEY_POINTS).toString(),
                                map.get(UserSessionManager.KEY_IMG).toString(),
                                map.get(UserSessionManager.KEY_MOBILE).toString(),
                                map.get(UserSessionManager.KEY_CITY).toString(),
                                map.get(UserSessionManager.KEY_DOB).toString(),
                                map.get(UserSessionManager.KEY_BRANCH).toString(),
                                map.get(UserSessionManager.KEY_DAY).toString(),
                                map.get(UserSessionManager.KEY_Curreny).toString(),
                                map.get(UserSessionManager.KEY_GST).toString(),
                                map.get(UserSessionManager.KEY_GSTFORMAT).toString());
                        startActivity(new Intent(Password_change.this,CustomerNavigation.class));
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
