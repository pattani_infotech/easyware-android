package com.easyware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.easyware.R;
import com.easyware.adapter.UnpaidByIDAdapter;
import com.easyware.bean.UnpaidByIDBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyware.easyware.R.id.extradta;

public class UnpaidByID extends AppCompatActivity {
    Toolbar toolbar;
    ImageButton back;
    TextView tprofile;
    RecyclerView unpaidbyidlist;
    List<UnpaidByIDBean> unpaidByIDBeen =new ArrayList<>();
    UnpaidByIDAdapter unpaidByIDAdapter;
    RelativeLayout datalayout;
    TextView tvvoucher,voucher_text,nodata,subtotal,gst,total,discount,deposit,remain_Balance,order_date,tvDiscount_text,tvDeposit_text,tvRemain_Balance_text,recno;
    TextView gstrate,amount,tax;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    String unpaidid;
    private String rec_no_text;
    public static String ssub_total;
    private static String stax;
    private static String smain_total;
    private static String sdeposit;
    private static String sdiscount;
    private static String sremain;
    private String orderdate,rec_no;
    private RelativeLayout tax_layout_contain;
    private String voucher;
    private String gst_amt;
    private String gst_tax;
    private RelativeLayout gstlayout;
    private LinearLayout gst_linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unpaid_by_id);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        manager = new UserSessionManager( UnpaidByID.this );
        map = manager.getUserDetail();
        SetUpViews();
        detector = new ConnectionDetector( UnpaidByID.this );
        Intent intent=getIntent();
        unpaidid=intent.getStringExtra("id");
        if(detector.isConnectingToInternet()) {
            String param = "id=" + unpaidid;
            //Log.e("unpaidid",unpaidid+" ");
            if(getIntent().hasExtra("claim"))
            {
                if(getIntent().getStringExtra("claim").toString().equals("Claim History"))
                {
                    if(detector.isConnectingToInternet()) {
                        new GetUnpaid_ByID().execute(param, Webservice.claims_details);
                        tvDeposit_text.setVisibility(View.GONE);
                        deposit.setVisibility(View.GONE);
                        tvRemain_Balance_text.setVisibility(View.GONE);
                        remain_Balance.setVisibility(View.GONE);
                        tvvoucher.setVisibility(View.GONE);
                        voucher_text.setVisibility(View.GONE);

                    }
                }else{
                    if(detector.isConnectingToInternet()) {
                        tax_layout_contain.setVisibility(View.GONE);
                        gstlayout.setVisibility(View.GONE);

                        new GetUnpaid_ByID().execute(param, Webservice.course_claimsdetails);
                    }
                }

            }else {
                if(detector.isConnectingToInternet())
                {
                    new GetUnpaid_ByID().execute( param, Webservice.unpaid_ByID);
                }

            }

        }

    }
    private void SetUpViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tprofile = (TextView) findViewById(R.id.tprofile);
        back = (ImageButton) toolbar.findViewById(R.id.back);
        tax_layout_contain=(RelativeLayout)findViewById(R.id.tax_layout);
       // voucher_text=(TextView)findViewById(R.id.voucher_text_data);
        if(getIntent().hasExtra("claim")) {
            tprofile.setText(getIntent().getStringExtra("claim").toString());

        }else {
            tprofile.setText("Order Details");
        }

        unpaidbyidlist= (RecyclerView) findViewById(R.id.unpaidbyidlist);
        subtotal=(TextView)findViewById(R.id.subtotal);
        gst=(TextView)findViewById(R.id.gst);
        total=(TextView)findViewById(R.id.total);
        discount=(TextView)findViewById(R.id.discount);
        deposit=(TextView)findViewById(R.id.deposit);
        tvvoucher=(TextView)findViewById(R.id.tvvoucher);
        voucher_text=(TextView)findViewById(extradta);
        remain_Balance=(TextView)findViewById(R.id.remain_Balance);
        order_date=(TextView)findViewById(R.id.order_date);
        recno=(TextView)findViewById(R.id.recno);
        tvDiscount_text=(TextView)findViewById(R.id.tvDiscount);
        tvDeposit_text=(TextView)findViewById(R.id.tvDeposit);
        nodata=(TextView)findViewById(R.id.nodata);
        datalayout= (RelativeLayout) findViewById(R.id.datalayout);
        tvRemain_Balance_text=(TextView)findViewById(R.id.tvRemain_Balance);

        gstrate=(TextView)findViewById(R.id.gstrate);
        amount=(TextView)findViewById(R.id.amount);
        tax=(TextView)findViewById(R.id.tax);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        gstlayout=(RelativeLayout)findViewById(R.id.gstlayout);
        gst_linear=(LinearLayout)findViewById(R.id.gst_linear);

        if(map.get(UserSessionManager.KEY_GSTFORMAT).toString().equalsIgnoreCase("inc"))
        {
            gstlayout.setVisibility(View.VISIBLE);
            gst_linear.setVisibility(View.GONE);
        }else{
            gstlayout.setVisibility(View.GONE);
            gst_linear.setVisibility(View.VISIBLE);
        }



    }
    class GetUnpaid_ByID extends AsyncTask<String, String, String> {
        ProgressDialog dialog;
        String responce, data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(UnpaidByID.this);
            dialog.setMessage("Please wait...!");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {

            responce = JsonParser.GetJsonFromURL(params[0], params[1]);
           // Log.e("1230",responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200") || data_code.equals("202")) {
                        JSONObject object=jsonObject.getJSONObject("data");
                        if(object.has("voucher")) {
                            voucher=object.getString("voucher");
                        }
                        if(object.has("total")) {
                            ssub_total=object.getString("total");
                            double s_total=Double.parseDouble(ssub_total);
                            s_total=(s_total*Double.parseDouble(map.get(UserSessionManager.KEY_GST)))/100;
                            gst_tax=map.get(UserSessionManager.KEY_Curreny).toString()+s_total;
                            double _gst_amt=Double.parseDouble(ssub_total)-s_total;
                            gst_amt=map.get(UserSessionManager.KEY_Curreny).toString()+_gst_amt;


                        }
                        if(object.has("tax")) {
                            stax=object.getString("tax");
                            double s_tax=Double.parseDouble(stax);

                        }
                        if(object.has("main_total")) {
                            smain_total=object.getString("main_total");
                        }
                        if(object.has("deposit")) {
                            sdeposit=object.getString("deposit");
                        }
                        if(object.has("discount")) {
                            sdiscount=object.getString("discount");
                        }
                            if(object.has("remain")) {
                            sremain=object.getString("remain");
                        }
                        if(object.has("recdate")) {
                            orderdate=object.getString("recdate");
                        }
                        if(object.has("rec_no")) {
                            rec_no=object.getString("rec_no");
                        }
                        if(object.has("date")) {
                            orderdate=object.getString("date");
                        }


                        if(!object.isNull("items")){
                            JSONArray jsonArray = object.getJSONArray( "items" );
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                               // Log.e("testtest ",obj.getString( "prod_name" ));
                                UnpaidByIDBean bean = new UnpaidByIDBean();
                                if(obj.has("prod_name")) {
                                    bean.setProd_name(obj.getString( "prod_name" ));
                                }
                                if(obj.has("prod_price")) {
                                    bean.setProd_price(obj.getString( "prod_price" ));
                                }
                                if(obj.has("prod_code")) {
                                    bean.setBrand(obj.getString( "prod_code" ));
                                }
                                if(obj.has("beautician")) {
                                    bean.setBeautician(obj.getString( "beautician" ));
                                    //Log.e("123456789   ",obj.getString( "beautician" ));
                                }
                                if(obj.has("prod_qty")) {
                                    bean.setQty(obj.getString( "prod_qty" ));
                                }
                                unpaidByIDBeen.add(bean);
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();

            if(responce!=null) {
                if(data_code!=null) {
                    if(data_code.equals("200") || data_code.equals("202")) {
                        amount.setText(gst_amt);
                        tax.setText(gst_tax);
                                if(!getIntent().hasExtra("claim")) {
                                    order_date.setText("Order Date & Time "+orderdate);
                                    recno.setText("Rec No. : "+rec_no);
                                }else{
                                    if(getIntent().getStringExtra("claim").toString().contains("Course")) {
                                        order_date.setText("Course Claim Date & Time "+orderdate);
                                        recno.setText("Rec No. : "+rec_no);
                                    }else{
                                        order_date.setText("Claim Date & Time "+orderdate);
                                        recno.setText("Rec No. : "+rec_no);
                                    }
                                }
                                Log.e("gstrate",""+map.get(UserSessionManager.KEY_GST).toString());
                        subtotal.setText(map.get(UserSessionManager.KEY_Curreny)+ssub_total);
                        gst.setText(map.get(UserSessionManager.KEY_Curreny)+stax);
                        total.setText(map.get(UserSessionManager.KEY_Curreny)+smain_total);
                        discount.setText(map.get(UserSessionManager.KEY_Curreny)+sdiscount);
                        voucher_text.setText(map.get(UserSessionManager.KEY_Curreny)+voucher);
                        deposit.setText(map.get(UserSessionManager.KEY_Curreny)+sdeposit);
                        remain_Balance.setText(map.get(UserSessionManager.KEY_Curreny)+sremain);

                        gstrate.setText("SR"+" "+map.get(UserSessionManager.KEY_GST).toString()+"%");

                        unpaidByIDAdapter = new UnpaidByIDAdapter(unpaidByIDBeen,UnpaidByID.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( UnpaidByID.this );
                        unpaidbyidlist.setLayoutManager( mLayoutManager );
                        unpaidbyidlist.setAdapter( unpaidByIDAdapter );
                    }else {
                        datalayout.setVisibility(View.GONE);
                        nodata.setVisibility(View.VISIBLE);
                    }
                }
            }

        }
    }
}
