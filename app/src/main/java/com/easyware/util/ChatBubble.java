package com.easyware.util;

/**
 * Created by Pattani Infoway Serviced Pvt Ltd on 7/22/2017.
 */

public class ChatBubble {

    private boolean isMe;

    private String id;
    private String message;
    private String userId;
    private String dateTime;

    public ChatBubble(boolean isMe, String s) {
        super();
        this.isMe = isMe;
        this.message = s;
    }

    public ChatBubble() {}

    public boolean getIsme() {
        return isMe;
    }
    public void setMe(boolean isMe) {
        this.isMe = isMe;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

}
