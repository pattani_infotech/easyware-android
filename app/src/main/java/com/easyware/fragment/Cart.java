package com.easyware.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyware.activity.ProductList;
import com.easyware.adapter.CartProAdapter;
import com.easyware.bean.CartProductBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.SharedPreference;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Cart extends Fragment {
    private View v;
    public static RelativeLayout withshipping;
    public static RelativeLayout gst_layout;
    public static TextView ordertotal1, otnew1, gstcost, ordertotal,gstratec,amountc,taxc;
    ConnectionDetector cd;
    UserSessionManager manager;
    HashMap<String, String> map;
    public static List<CartProductBean> cpBean = new ArrayList<>();
    public static CartProAdapter cpAdapter;
    public static ListView list;
    public static ImageView nodata;
    public static Button cancel, confirm;
    LinearLayout layouttwo, layoutone;
    RelativeLayout pricelayout;

    SharedPreference sharedPreference;
    Gson gson;

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    //private static final String CONFIG_CLIENT_ID = "AZOXVhKXVQ6mDtmAO_npRcKuNFJulIWJ4Xi_L642s1yNZWIXdip-dZ79TirBDHlQ-AP-6SkzPdFqDu5t";
   // private static final String CONFIG_CLIENT_ID = "Ab0cRI7pftF0wtgMnSECI4mrub22atffVf2m0tOp9iO8UFiZ1O9x9F4mRSdQNE_lLY0T28EzOtrcCa3_";
    private static final String CONFIG_CLIENT_ID = "ARD-JHeO6BRTM1N5LVCnMSn5vjzV0ET6BvKKH0Dycm6J-2MFKmihezjLs37zXEkZhT3YA4XX1ShJtpsu";

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            .merchantName("Easyware")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
    static PayPalPayment thingToBuy;
    String Pr = "8";
    private String gst_tax;
    private String gst_amt;
    private RelativeLayout gsttextlayout;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.cart_frag, container, false);
        cd = new ConnectionDetector(getActivity());
        manager = new UserSessionManager(getActivity());
        map = manager.getUserDetail();
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);
        sharedPreference = new SharedPreference(getActivity());
        gson = new Gson();
        SetupView();

        //dataset();
        if (sharedPreference.loadProduct(getActivity()) != null) {

            String jsonCPB = gson.toJson(sharedPreference.loadProduct(getActivity()));
         //   Log.e("cart detail", jsonCPB);
            cpBean = sharedPreference.loadProduct(getActivity());
            //Log.e("sharedPreference",sharedPreference.loadProduct(getActivity())+"");
            cpAdapter = new CartProAdapter(cpBean, getActivity());
            list.setAdapter(cpAdapter);
            cpAdapter.notifyDataSetChanged();
        } else {

        }

        if (cpBean.size() == 0) {
            Webservice.comanid = "";
            nodata.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            withshipping.setVisibility(View.GONE);
            gst_layout.setVisibility(View.GONE);

            otnew1.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
            confirm.setVisibility(View.GONE);
            ordertotal.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");
            gstcost.setText(map.get(UserSessionManager.KEY_Curreny) + "0.00");


            cancel.setText("Your cart is empty...");
            confirm.setVisibility(View.GONE);
            //ContextCompat.getColor(ProductList.this, R.color.redd)
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ProductList.counttext.setBackgroundColor(getActivity().getResources().getColor(R.color.trans, null));
            } else {
                ProductList.counttext.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.trans));
            }

        } else {
            list.setVisibility(View.VISIBLE);
            withshipping.setVisibility(View.VISIBLE);
            gst_layout.setVisibility(View.VISIBLE);

            nodata.setVisibility(View.GONE);
            cancel.setText("cancel Order");
            confirm.setVisibility(View.VISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ProductList.counttext.setBackground(getActivity().getResources().getDrawable(R.drawable.roundc, null));
            } else {
                ProductList.counttext.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.roundc));
            }

            ProductList.counttext.setText("" + cpBean.size());
        }


        return v;
    }

    private void dataset() {
        /*if (cpBean.size() == 0) {
            nodata.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            withshipping.setVisibility(View.GONE);
            Cart.otnew1.setText(map.get(UserSessionManager.KEY_Curreny)+"0.00");
        } else {
            list.setVisibility(View.VISIBLE);
            withshipping.setVisibility(View.VISIBLE);
            nodata.setVisibility(View.GONE);
        }
        cpAdapter = new CartProAdapter(cpBean, getActivity());
        list.setAdapter(cpAdapter);
        cpAdapter.notifyDataSetChanged();
        //Navigation.counttext.setVisibility(View.VISIBLE);
        Log.e("size", "" + cpAdapter.getCount());
        //Navigation.counttext.setText("" + cpAdapter.getCount());
        if (cpAdapter.getCount() != 0) {
            cancel.setText("cancel Order");
            confirm.setVisibility(View.VISIBLE);
           // Navigation.counttext.setBackground(getResources().getDrawable(R.drawable.round,null));
        } else {
            confirm.setVisibility(View.GONE);
            ordertotal.setText(map.get(UserSessionManager.KEY_Curreny)+"0.00");
            cancel.setText("Your cart is empty...");
            confirm.setVisibility(View.GONE);
        }*/
    }


    private void SetupView() {
        list = (ListView) v.findViewById(R.id.list);
        nodata = (ImageView) v.findViewById(R.id.nodata);
        gstcost = (TextView) v.findViewById(R.id.gstcost);
        ordertotal1 = (TextView) v.findViewById(R.id.ot);
        otnew1 = (TextView) v.findViewById(R.id.otnew);
        withshipping = (RelativeLayout) v.findViewById(R.id.withshipping);


        cancel = (Button) v.findViewById(R.id.cancel);
        confirm = (Button) v.findViewById(R.id.confirm);
        layouttwo = (LinearLayout) v.findViewById(R.id.layouttwo);
        pricelayout = (RelativeLayout) v.findViewById(R.id.pricelayout);
        ordertotal = (TextView) v.findViewById(R.id.ordertotal);

        gstratec = (TextView) v.findViewById(R.id.gstratec);
        amountc = (TextView) v.findViewById(R.id.amountc);
        taxc = (TextView) v.findViewById(R.id.taxc);

        gst_layout = (RelativeLayout) v.findViewById(R.id.gst_layout);
        gsttextlayout = (RelativeLayout) v.findViewById(R.id.gsttextlayout);


        if(map.get(UserSessionManager.KEY_GSTFORMAT).toString().equalsIgnoreCase("inc"))
        {
            gst_layout.setVisibility(View.VISIBLE);
            gsttextlayout.setVisibility(View.GONE);
        }else{
            gst_layout.setVisibility(View.GONE);
            gsttextlayout.setVisibility(View.VISIBLE);
        }




        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cancel.getText().toString().equals("cancel Order")) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Cancel Order");
                    alertDialog.setMessage("Are you sure you want to cancel order?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Cart.RemoveData("cancel");
                            Webservice.comanid = "";
                            /*cpBean.clear();
                            sharedPreference.SaveLocalStore(getActivity(),cpBean);*/
                            Webservice.MakeToast("Order cancel sucessfully", getActivity());
                            startActivity(new Intent(getActivity(), ProductList.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                } else {
                    Webservice.MakeToast("Your cart is empty...", getActivity());
                }
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*String jsonCPB = gson.toJson(cpBean);
                //Log.e("confirm string json",jsonCPB);
                String sss = String.valueOf(sharedPreference.loadProduct(getActivity()));
                //Log.e("confirm string",sss);
                //JSONObject object=new JSONObject();
                JSONObject object1=new JSONObject();
                try {
                    //object.put("data",sss);
                    //contactsObj.put("ContactTokens", new JSONArray(tokens));
                    object1.put("data",new JSONArray(jsonCPB));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String data=object1.toString();
                Log.e("object1",object1.toString());*/
                /*try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.has("data")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            Log.e("product_id",obj.getString("product_id"));
                            Log.e("qty",obj.getString("qty"));
                            Log.e("price",obj.getString("price"));
                            Log.e("name",obj.getString("name"));
                            Log.e("id_default_image",obj.getString("id_default_image"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }*/
                //Log.e("confirm sss",object.toString());*/
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Confirm Order");
                alertDialog.setMessage("Are you sure you want to confirm order?");
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String jsonCPB = gson.toJson(cpBean);
                        JSONObject object = new JSONObject();
                        try {
                            object.put("data", new JSONArray(jsonCPB));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("object", object.toString());
                        //initiatePopupWindow(11);
                        new ConfirmOrder().execute(map.get(UserSessionManager.KEY_USERID), map.get(UserSessionManager.KEY_BRANCHID), object.toString());
                        //Cart.RemoveData("confirm");
                        //Webservice.comanid="";
                        //GetPayment(WebService.paypalAmount);
                        //Webservice.MakeToast("Order confirm sucessfully",getActivity());
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

    }

    public static void RemoveData(String rm) {
        if (rm.equals("confirm")) {
//GetPayment(WebService.paypalAmount);
            cpAdapter.Remove();
        } else {
            cpAdapter.Remove();
            //newdataset();
        }
    }


    private class ConfirmOrder extends AsyncTask<String, String, String> {
        android.app.AlertDialog pd;
        String responce, data_code;
        int reqPoints;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0] + "&branch_id=" + params[1] + "&data=" + params[2];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Emall_Geterate_Order);
           // Log.e("Emall_Generate_Order", responce + "");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {
                        if (!jsonObject.isNull("data")) {
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("data"));
                           // Log.e("126556", "" + jsonObject1.getString("reqpoints"));

                            Webservice.OrderId = jsonObject1.getString("ordrid");
                            reqPoints = Integer.parseInt(jsonObject1.getString("reqpoints"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pd != null) {
                pd.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        /*Cart.RemoveData("confirm");
                        Webservice.comanid="";
                        //GetPayment(WebService.paypalAmount);
                        Webservice.MakeToast("Order generate sucessfully",getActivity());*/
                        initiatePopupWindow(reqPoints);
                    } else {
                        Webservice.MakeToast("Something went wrong. Please try again.", getActivity());
                    }
                }
            }
        }
    }

    private void initiatePopupWindow(final int reqPoints) {
        try {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = inflater.inflate(R.layout.payment_option_layout, null);
            final PopupWindow mPopupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mPopupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
            mPopupWindow.update();
            TextView points1 = (TextView) popupView.findViewById(R.id.points1);
            TextView points2 = (TextView) popupView.findViewById(R.id.points2);
            ImageView id_dismiss = (ImageView) popupView.findViewById(R.id.id_dismiss);
            Button paypal = (Button) popupView.findViewById(R.id.paypalb);
            Button submit = (Button) popupView.findViewById(R.id.submit);
            points1.setText("" + map.get(UserSessionManager.KEY_POINTS));
            points2.setText("" + reqPoints);
            id_dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPopupWindow.dismiss();
                }
            });
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (map.get(UserSessionManager.KEY_POINTS) != null) {
                        if (reqPoints > Integer.parseInt(map.get(UserSessionManager.KEY_POINTS))) {
                            //if (reqPoints>10 ) {
                            Webservice.MakeToast("Please pay paypal...!", getActivity());
                        } else {
                            /*Cart.RemoveData("confirm");
                            Webservice.comanid = "";
                            //GetPayment(WebService.paypalAmount);*/
                            String type = "points";
                            String param = "user_id=" + map.get(UserSessionManager.KEY_USERID) +"&order_id=" + Webservice.OrderId + "&type=" + type+"&points_amount="+reqPoints;
                            new CheckOut().execute(param);
                            Webservice.MakeToast("Order generate sucessfully", getActivity());
                            mPopupWindow.dismiss();
                        }
                    }
                }
            });
            paypal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GetPayment(Webservice.paypalAmount);
                    mPopupWindow.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetPayment(String payment) {
       // Log.e("payment123", payment);
        String currency;
        if (map.get(UserSessionManager.KEY_Curreny).equals("RM")) {
            currency = "MYR";
        } else {
            currency = map.get(UserSessionManager.KEY_Curreny);
        }
       // Log.e("currency", currency);
        thingToBuy = new PayPalPayment(new BigDecimal(payment), currency,
                "Easyware", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
               // Log.e("payment log :", "" + confirm);
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject().toString(4));
                  //      Log.e("123456 : ", "" + confirm.toJSONObject().toString(4));
                    //    Log.e("666666 : ", "" + confirm.getPayment().toJSONObject().toString(4));

                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        JSONObject jsonObject1 = new JSONObject(jsonObject.getString("response"));
                        String transactionID = jsonObject1.getString("id");
                        String type = "paypal";
                        String param = "user_id=" + map.get(UserSessionManager.KEY_USERID) + "&transaction_id=" + transactionID + "&order_id=" + Webservice.OrderId + "&type=" + type;
                        new CheckOut().execute(param);
                        Cart.RemoveData("confirm");
                        Webservice.comanid = "";
                        Webservice.MakeToast("Order generate sucessfully", getActivity());
                        Webservice.MakeToast("Order placed", getActivity());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private class CheckOut extends AsyncTask<String, String, String> {
        android.app.AlertDialog pdc;
        String responce, data_code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdc = new ProgressDialog(getActivity());
            pdc.setMessage("Please wait...");
            pdc.setCancelable(false);
            pdc.show();
        }

        @Override
        protected String doInBackground(String... params) {
            responce = JsonParser.GetJsonFromURL(params[0], Webservice.Emall_CheckOut);
            Log.e("Emall_CheckOut", responce + "");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pdc != null) {
                pdc.dismiss();
            }
            if (responce != null) {
                if (data_code.equals("200")) {
                    Cart.RemoveData("confirm");
                    Webservice.comanid = "";
                    Webservice.MakeToast("Your order has been placed successfully", getActivity());
                    /*Webservice.MakeToast("Order generate sucessfully", getActivity());
                    Webservice.MakeToast("Order placed", getActivity());*/
                } else if (data_code.equals("404")) {
                    Webservice.MakeToast("Error occured in no Emall Checkout Order List", getActivity());
                } else {
                    Webservice.MakeToast("something went wrong...!", getActivity());
                }
            }
        }
    }
}