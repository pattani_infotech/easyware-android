package com.easyware.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.easyware.adapter.MyOrderAdapter;
import com.easyware.bean.MyOrderBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MyOrders extends Fragment {
    private View v;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    RecyclerView order_history;
    ImageView nodata;
    List<MyOrderBean> myOrderBeen=new ArrayList<>();
    private MyOrderAdapter myorderadapte;
    public static RelativeLayout framly;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.myorders_frag, container, false);
        manager = new UserSessionManager(getActivity());
        detector = new ConnectionDetector(getActivity());
        map = manager.getUserDetail();
        SetupView();
        if (detector.isConnectingToInternet()) {
            new GetUserOrder().execute(map.get(UserSessionManager.KEY_USERID));
        }else {
            Webservice.MakeToast("Please Connect your Device to Internet", getActivity());
        }

        return v;
    }


    private void SetupView() {
        order_history= (RecyclerView) v.findViewById(R.id.order_history);
        nodata= (ImageView) v.findViewById(R.id.nodata);

        framly= (RelativeLayout) v.findViewById(R.id.framly);
    }


    private class GetUserOrder extends AsyncTask<String, String, String> {
        android.app.AlertDialog pd;
        String responce, data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "user_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Emall_User_Order);
            //Log.e("Emall_User_Order",responce);
            if(Webservice.isJSONValid(responce)) {
                if (responce != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(responce);
                        data_code = jsonObject.getString("Data_code");
                        if (data_code.equals("200")) {
                            if (jsonObject.has("data")) {
                                if (!jsonObject.isNull("data")) {
                                    JSONArray array = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject obj = array.getJSONObject(i);
                                        MyOrderBean bean = new MyOrderBean();
                                        bean.setId(obj.getString("id"));
                                        bean.setOrder_no(obj.getString("order_no"));
                                        bean.setTotal( obj.getString("total"));
                                        bean.setDate(obj.getString("date"));
                                        bean.setPoint_used(obj.getString("point_used"));
                                        bean.setGst(obj.getString("gst"));
                                        bean.setSubtotal(obj.getString("subtotal"));
                                        bean.setStatus(obj.getString("status"));
                                        bean.setItem(obj.getString("item"));
                                        myOrderBeen.add(bean);
                                    /*if (obj.has("item")) {
                                        if (!obj.isNull("item")) {
                                            JSONArray arraya = obj.getJSONArray("item");
                                            for (int j = 0; j < arraya.length(); j++) {
                                                JSONObject obja = arraya.getJSONObject(j);
                                                Log.e("item = id", obja.getString("id"));
                                                Log.e("item = order_id", obja.getString("order_id"));
                                                Log.e("item = product_id", obja.getString("product_id"));
                                                Log.e("item = price", obja.getString("price"));
                                                Log.e("item = qty", obja.getString("qty"));
                                                Log.e("item = created", obja.getString("created"));
                                            }
                                        }
                                    }*/
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
            }

            }else{
                responce="invalid";
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pd != null) {
                pd.dismiss();
            }
            if (responce != null) {
                if(!responce.equalsIgnoreCase("invalid")) {
                    if (data_code.equals("200")) {
                        nodata.setVisibility(View.GONE);
                        order_history.setVisibility(View.VISIBLE);
                        myorderadapte = new MyOrderAdapter(myOrderBeen, getActivity());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        order_history.setLayoutManager(mLayoutManager);
                        order_history.setItemAnimator(new DefaultItemAnimator());
                        order_history.setAdapter(myorderadapte);
                    }else {
                        nodata.setVisibility(View.VISIBLE);
                        order_history.setVisibility(View.GONE);
                    }
                }else {
                    nodata.setVisibility(View.VISIBLE);
                    order_history.setVisibility(View.GONE);
                }

            }
        }
    }
}