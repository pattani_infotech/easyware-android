package com.easyware.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.easyware.activity.AddNewAppointment;
import com.easyware.activity.Appointments;
import com.easyware.adapter.MyAdapter;
import com.easyware.adapter.ProductsAdapter;
import com.easyware.bean.AdminBannerBean;
import com.easyware.bean.BannerBean;
import com.easyware.bean.ProductBean;
import com.easyware.easyware.ConnectionDetector;
import com.easyware.easyware.R;
import com.easyware.easyware.UserSessionManager;
import com.easyware.parcer.JsonParser;
import com.easyware.service.Webservice;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.easyware.activity.ProductList.listpress;


public class Product extends Fragment /* implements PopupMenu.OnMenuItemClickListener */ {
    private TextView nodata;
    EditText search;
    Button sort, sortn, filtern;
    Button filter;
    ImageView displayn;
    ConnectionDetector detector;
    UserSessionManager manager;
    HashMap<String, String> map;
    ViewPager ebannerv;
    public static GridView gridview;
    public static ListView listview;
    private PopupWindow mPopupWindow;
    private PopupMenu popup;
    List<ProductBean> productBean = new ArrayList<>();
    ProductsAdapter pAdapter;
    List<BannerBean> bannerBean = new ArrayList<>();
    List<AdminBannerBean> adminbannerBean = new ArrayList<>();
    int j = 0;
    int k = 0;
    ImageView ebannerimg;
    private Context mContext;
    private RelativeLayout mRelativeLayout;
    LinearLayout sld;
    MyAdapter adapter;
    private View v;
    Spinner sortby;
    List<String> type;
    private String filter_string;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.product_frag, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mContext = getActivity();
        manager = new UserSessionManager(getActivity());
        detector = new ConnectionDetector(getActivity());
        map = manager.getUserDetail();
        SetUpViews();
        if (detector.isConnectingToInternet()) {
            //Log.e("id branch",map.get(UserSessionManager.KEY_BRANCHID));
            new GetFilter().execute(map.get(UserSessionManager.KEY_BRANCHID));
        } else {
            Webservice.MakeToast("Please Connect your Device to Internet", getActivity());
        }
        type = new ArrayList<String>();
        type.add("Sort by");
        type.add("Sort By Lowest first");
        type.add("Sort By Highest first");
        type.add("Sort By A to Z");
        type.add("Sort By Z to A");
        type.add("Products");
        type.add("Services");
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinnner_filter, type);
        adapter.setDropDownViewResource(R.layout.spinnner_filter);
        sortby.setAdapter(adapter);
        sortby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("position ", sortby.getSelectedItemPosition() + "");
                if (detector.isConnectingToInternet()) {
                    if (sortby.getSelectedItemPosition() == 0) {
                        search.setText("");
                        GetFilterProduct("default");
                    } else if (sortby.getSelectedItemPosition() == 1) {
                        search.setText("");
                        GetFilterProduct("lowestfirst");
                    } else if (sortby.getSelectedItemPosition() == 2) {
                        GetFilterProduct("highetfirst");
                    } else if (sortby.getSelectedItemPosition() == 3) {
                        search.setText("");
                        GetFilterProduct("atoz");
                    } else if (sortby.getSelectedItemPosition() == 4) {
                        search.setText("");
                        GetFilterProduct("ztoa");
                    } else if (sortby.getSelectedItemPosition() == 5) {
                        search.setText("");
                        GetFilterProduct("products");
                    } else if (sortby.getSelectedItemPosition() == 6) {
                        search.setText("");
                        GetFilterProduct("services");
                    }
                } else {
                    Webservice.MakeToast("Please Connect your Device to Internet", getActivity());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (detector.isConnectingToInternet()) {
            //Log.e("id branch",map.get(UserSessionManager.KEY_BRANCHID));
            new GetEmallBanner().execute(map.get(UserSessionManager.KEY_BRANCHID));
        } else {
            Webservice.MakeToast("Please Connect your Device to Internet", getActivity());
        }
        if (detector.isConnectingToInternet()) {
           // new getProduct().execute(map.get(UserSessionManager.KEY_BRANCHID));
            GetFilterProduct("default");
        } else {
            Webservice.MakeToast("Please Connect your Device to Internet", getActivity());
        }
       //Log.e("listpress123",listpress+"");
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void SetUpViews() {
        gridview = (GridView) v.findViewById(R.id.gridview);
        listview = (ListView) v.findViewById(R.id.listview);
        search = (EditText) v.findViewById(R.id.search);
        //search.setVisibility(View.GONE);
        //sort = (Button) v.findViewById(R.id.sort);
        sortn = (Button) v.findViewById(R.id.sortn);
        filter = (Button) v.findViewById(R.id.filter);
        filtern = (Button) v.findViewById(R.id.filtern);
        nodata = (TextView) v.findViewById(R.id.nodata);
        ebannerv = (ViewPager) v.findViewById(R.id.ebannerv);
        ebannerimg = (ImageView) v.findViewById(R.id.ebannerimg);
        displayn = (ImageView) v.findViewById(R.id.displayn);
        mRelativeLayout = (RelativeLayout) v.findViewById(R.id.mRelativeLayout);
        sld = (LinearLayout) v.findViewById(R.id.sld);
        sortby = (Spinner) v.findViewById(R.id.sortby);
         search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.equals("")) {
                    if (pAdapter!=null) {
                        pAdapter.getFilter().filter(s.toString());
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
         });
        sortn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiatePopupWindow();
            }
        });
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*position=0;
                GetProduct(String.valueOf(position),"productList");
                proGridBean.clear();*/
                search.setText("");
                sort.setText("Sort By");
                GetFilterProduct("default");
                filter.setVisibility(View.GONE);
            }
        });
        displayn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listpress) {
                    listpress = false;
                    //Log.e("false", "" + listpress);
                    gridview.setVisibility(View.VISIBLE);
                    listview.setVisibility(View.GONE);
                    gridview.setTextFilterEnabled(true);
                } else {
                    listpress = true;
                    //Log.e("true", "" + listpress);
                    gridview.setVisibility(View.GONE);
                    listview.setVisibility(View.VISIBLE);
                    listview.setTextFilterEnabled(true);


                }
            }
        });



        filtern.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {

                                           popup = new PopupMenu(getActivity(), v);
                                           if(filter_string!=null)
                                           {
                                               try {
                                                   JSONArray filtes = new JSONArray(filter_string);
                                                   for(int i=0;i<filtes.length();i++)
                                                   {
                                                       JSONObject jsonObject=filtes.getJSONObject(i);
                                                       int id=Integer.parseInt(jsonObject.getString("id"));
                                                       popup.getMenu().add(1, id, 1, jsonObject.getString("title"));
                                                   }
                                               } catch (JSONException e) {
                                                   e.printStackTrace();
                                               }

                                           }
                                           //popup.inflate(R.menu.filter_popup);


                                           popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                               @Override
                                               public boolean onMenuItemClick(MenuItem item) {
                                                   //Toast.makeText(getContext().getApplicationContext(), item.getItemId()+" ", Toast.LENGTH_LONG).show();


                                                   String param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&brand="+item.getItemId();
                                                   new getProduct().execute(param);
                                                   //getProduct
                                                   /*switch (item.getItemId()) {
                                                       case R.id.titlea:
                                                           //SessionManager = item.getTitle().toString();
                                                           Toast.makeText(getContext().getApplicationContext(), "titlea ", Toast.LENGTH_LONG).show();
                                                           return true;
                                                       case R.id.titleb:
                                                           Toast.makeText(getContext().getApplicationContext(), "titleb ", Toast.LENGTH_LONG).show();
                                                           return true;
                                                       default:
                                                           return false;
                                                   }*/
                                                   return false;
                                               }
                                           });
                                           popup.show();


                                       }
                                       });


        //gridview.setVerticalScrollBarEnabled(false);
        //gridview.setNestedScrollingEnabled(false);
    }

    private class GetFilter extends AsyncTask<String, String, String>{

        String responce, data_code;
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setCancelable(false);
            //dialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.emall_brands);
            Log.e("GetFilter responce", responce + " ");
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {
                        filter_string=jsonObject.getString("data");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        //Toast.makeText(getActivity(), " successfull", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        }

    }



    private void initiatePopupWindow() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.custome_layout, null);
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //final PopupWindow mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.update();
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);
        final TextView tv = (TextView) customView.findViewById(R.id.tv);
        final TextView tv1 = (TextView) customView.findViewById(R.id.tv1);
        final TextView tv2 = (TextView) customView.findViewById(R.id.tv2);
        final TextView tv3 = (TextView) customView.findViewById(R.id.tv3);
        final TextView tv4 = (TextView) customView.findViewById(R.id.tv4);
        final TextView tv5 = (TextView) customView.findViewById(R.id.tv5);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPopupWindow.dismiss();
            }
        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*GetProduct(String.valueOf(position),"lowestfirst");
                proGridBean.clear();*/
                search.setText("");
                mPopupWindow.dismiss();
                //filter.setVisibility(View.VISIBLE);
                sortn.setText("Sort By Lowest first");
                GetFilterProduct("lowestfirst");
            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*GetProduct(String.valueOf(position),"highestfirst");
                proGridBean.clear();*/
                search.setText("");
                mPopupWindow.dismiss();
                //filter.setVisibility(View.VISIBLE);
                sortn.setText("Sort By Highest first");
                GetFilterProduct("highetfirst");
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*GetProduct(String.valueOf(position),"atoz");
                proGridBean.clear();*/
                search.setText("");
                mPopupWindow.dismiss();
                //filter.setVisibility(View.VISIBLE);
                sortn.setText("Sort By A to Z");
                GetFilterProduct("atoz");
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* GetProduct(String.valueOf(position),"ztoa");
                proGridBean.clear();*/
                search.setText("");
                mPopupWindow.dismiss();
                filter.setVisibility(View.VISIBLE);
                sortn.setText("Sort By Z to A");
                GetFilterProduct("ztoa");
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*GetProduct(String.valueOf(position),"instock");
                proGridBean.clear();*/
                search.setText("");
                mPopupWindow.dismiss();
                //filter.setVisibility(View.VISIBLE);
                sortn.setText("Products");
                GetFilterProduct("products");
            }
        });
        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*GetProduct(String.valueOf(position),"instock");
                proGridBean.clear();*/
                search.setText("");
                mPopupWindow.dismiss();
                //filter.setVisibility(View.VISIBLE);
                sortn.setText("Services");
                GetFilterProduct("services");
            }
        });
    }

    private void GetFilterProduct(String filterproduct) {
        String param;
        if (filterproduct.equals("lowestfirst")){
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&price=asc";
        }else if (filterproduct.equals("highetfirst")){
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&price=desc";
        }else if (filterproduct.equals("atoz")){
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&title=asc";
        }else if (filterproduct.equals("ztoa")){
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&title=desc";
        }else if (filterproduct.equals("products")){
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&type=products";
        }else if (filterproduct.equals("services")){
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID)+"&type=services";
        }else {
            param="branch_id="+map.get(UserSessionManager.KEY_BRANCHID);
        }
        if (detector.isConnectingToInternet()) {
            new getProduct().execute(param);
        } else {
            Webservice.MakeToast("Please Connect your Device to Internet", getActivity());
        }
    }



    private class GetEmallBanner extends AsyncTask<String, String, String> {
        AlertDialog pd;
        String responce, data_code;
        int tipPosition = 0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.show();
        }
        @Override
        protected String doInBackground(String... params) {
            String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(param, Webservice.Emall_Banner);
           Log.e("Emall_Banner", responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("error");
                    if (data_code.equals("200")) {
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                BannerBean bean = new BannerBean();
                                bean.setImage(obj.getString("image"));
                                //bean.setImage("na");
                                bean.setTitle(obj.getString("title"));
                                bean.setDescription(obj.getString("description"));
                                bean.setSort(obj.getString("sort"));
                                bannerBean.add(bean);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pd != null) {
                pd.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        sld.setVisibility(View.VISIBLE);
                        if (bannerBean.size() != 0) {
                            /*BannerBean bean = new BannerBean();
                            bean.setImage("bbd226cb7cf3d64a014d83fd876e4b8d.JPG");
                            bean.setTitle("title : 2");
                            bean.setDescription("description : 2");
                            bean.setSort("sort 2");
                            bannerBean.add(bean);*/
                            EmallBanner(bannerBean);
                        }else {
                           // ebannerimg.setVisibility(View.GONE);
                           // ebannerv.setVisibility(View.GONE);
                          sld.setVisibility(View.GONE);
                        }
                    }else {
                        //ebannerimg.setVisibility(View.GONE);
                       // ebannerv.setVisibility(View.GONE);
                       sld.setVisibility(View.GONE);
                    }
                }else{
                    sld.setVisibility(View.GONE);
                   // ebannerimg.setVisibility(View.GONE);
                   // ebannerv.setVisibility(View.GONE);
                }
            }
        }
    }
    /*private class GetAdminBanner extends AsyncTask<String, String, String> {
        AlertDialog pdadmin;
        String responce, data_code;
        int tipPosition = 0;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pdadmin = new ProgressDialog(getActivity());
            pdadmin.setMessage("Please wait...");
            pdadmin.setCancelable(false);
            pdadmin.show();
        }
        @Override
        protected String doInBackground(String... params) {
            responce = JsonParser.GetJsonFromURL("", Webservice.Admin_Banner);
            Log.e("Admin_Banner", responce);
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("error");
                    if (data_code.equals("200")) {
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                AdminBannerBean bean = new AdminBannerBean();
                                bean.setImage(obj.getString("image"));
                                //bean.setImage("na");
                                bean.setTitle(obj.getString("title"));
                                bean.setDescription(obj.getString("description"));
                                bean.setSort(obj.getString("sort"));
                                adminbannerBean.add(bean);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (pdadmin != null) {
                pdadmin.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        Log.e("adminbannerBean.size",""+adminbannerBean.size());
                        if (adminbannerBean.size() != 0) {                                                                               *//*BannerBean bean = new BannerBean();
                            bean.setImage("bbd226cb7cf3d64a014d83fd876e4b8d.JPG");
                            bean.setTitle("title : 2");
                            bean.setDescription("description : 2");
                            bean.setSort("sort 2");
                            bannerBean.add(bean);*//*
                            AdminBanner(adminbannerBean);
                        }else {

                        }
                    }else {
                        Log.e("adminbanner else",""+adminbannerBean.size());
                    }
                }
            }
        }
    }*/
    private class getProduct extends AsyncTask<String, String, String> {
        AlertDialog progressDialog;
        String responce, data_code;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            //String param = "branch_id=" + params[0];
            responce = JsonParser.GetJsonFromURL(params[0], Webservice.Emall_Products);
           // Log.e("Emall_Products", responce);
            productBean.clear();
            if (responce != null) {
                try {
                    JSONObject jsonObject = new JSONObject(responce);
                    data_code = jsonObject.getString("Data_code");
                    if (data_code.equals("200")) {
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                ProductBean bean = new ProductBean();
                                bean.setId(obj.getString("id"));
                                bean.setTitle(obj.getString("title"));
                                bean.setDescription(obj.getString("description"));
                                bean.setType(obj.getString("type"));
                                // bean.setStatus(obj.getString("status"));
                                bean.setCreated(obj.getString("created"));
                                bean.setCategory_id(obj.getString("category_id"));
                                bean.setProd_code(obj.getString("prod_code"));
                                bean.setPrice(obj.getString("price"));
                                bean.setProm_price(obj.getString("prom_price"));
                                bean.setQty(obj.getString("qty"));
                                bean.setImage(obj.getString("image"));
                                bean.setVideo(obj.getString("video"));
                                productBean.add(bean);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (responce != null) {
                if (data_code != null) {
                    if (data_code.equals("200")) {
                        nodata.setVisibility(View.GONE);
                        pAdapter = new ProductsAdapter(productBean,mContext);
                        listview.setAdapter(pAdapter);
                        gridview.setAdapter(pAdapter);
                        setGridViewHeightBasedOnChildren(gridview,2);
                        if (listpress) {
                            listview.setVisibility(View.VISIBLE);
                            gridview.setVisibility(View.GONE);
                        } else {
                            gridview.setVisibility(View.VISIBLE);
                            listview.setVisibility(View.GONE);
                        }
                    } else {
                        listview.setVisibility(View.GONE);
                        gridview.setVisibility(View.GONE);
                        nodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }
    private void EmallBanner(List<BannerBean> bannerBean) {
        /*for (int i = 0; i < 4; i++) {
            BannerBean bean = new BannerBean();
            bean.setImage("bbd226cb7cf3d64a014d83fd876e4b8d.JPG");
            bean.setTitle("title : "+(i+1));
            bean.setDescription("description : "+(i+1));
            bean.setSort("sort");
            bannerBean.add(bean);
        }*/
        /*for (int i = 0; i < bannerBean.size(); i++) {
            tipPosition=i;
            HashMap<String, String> file_maps = new HashMap<String, String>();
            file_maps.put(String.valueOf(i), Webservice.img_path + bannerBean.get(i).getImage());
            for (String name : file_maps.keySet()) {
               DefaultSliderView defaultSliderView = new DefaultSliderView(ProductList.this);
                defaultSliderView.image(file_maps.get(name)).setScaleType(BaseSliderView.ScaleType.Fit);
                ebanner.addSlider(defaultSliderView);
            }
            ebanner.setPresetTransformer(SliderLayout.Transformer.Accordion);
            ebanner.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            ebanner.setCustomAnimation(new DescriptionAnimation());
            ebanner.setDuration(2500);
        }*/
        /*final Handler tipsHanlder = new Handler();
        Runnable tipsRunnable = new Runnable() {
            @Override
            public void run() {
                //set number of tip(randon/another way)
                title.setText(bannerBean.get(tipPosition).getTitle());
                disc.setText(bannerBean.get(tipPosition).getDescription());
                tipsHanlder.postDelayed(this, 2498);
            }
        };
        tipsHanlder.post(tipsRunnable);*/
          adapter=new MyAdapter(mContext,bannerBean);
        ebannerv.setAdapter(adapter);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                if(j <adapter.getCount()) {
                    ebannerv.setCurrentItem(j);
                    j++;
                } else {
                    j =0;
                }
                handler.postDelayed(this, 2000);
            }
        }, 200);
    }
   /* private void AdminBanner(List<AdminBannerBean> adminbannerBean) {

       final AdminAdapter adminadapter=new AdminAdapter(getActivity(),adminbannerBean);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                if(k <adminadapter.getCount()) {
                    //abannerv.setCurrentItem(k);
                    k++;
                } else {
                    k =0;
                }
                handler.postDelayed(this, 2000);
            }
        }, 200);
    }*/



    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }
}