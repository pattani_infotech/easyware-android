package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class PurchaseHistoryBean {

    public String pur_id;
    public String pur_title;
    public String pur_detail;
    public String pur_total;

    public String getRec_no() {
        return rec_no;
    }

    public void setRec_no(String rec_no) {
        this.rec_no = rec_no;
    }

    public  String rec_no;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String total;
    public String name;

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String branch;


    public String getPur_id() {
        return pur_id;
    }

    public void setPur_id(String pur_id) {
        this.pur_id = pur_id;
    }

    public String getPur_title() {
        return pur_title;
    }

    public void setPur_title(String pur_title) {
        this.pur_title = pur_title;
    }

    public String getPur_detail() {
        return pur_detail;
    }

    public void setPur_detail(String pur_detail) {
        this.pur_detail = pur_detail;
    }

    public String getPur_total() {
        return pur_total;
    }

    public void setPur_total(String pur_total) {
        this.pur_total = pur_total;
    }

    public PurchaseHistoryBean() {

    }



}
