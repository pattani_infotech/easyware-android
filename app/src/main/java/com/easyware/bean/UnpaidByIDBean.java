package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class UnpaidByIDBean {

    public String prod_name,qty,brand,beautician,prod_price,subtotal,gst,total,discount,deposit,remainbalance;



    public UnpaidByIDBean() {}
    public String getProd_name() {
        return prod_name;
    }
    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getQty() {
        return qty;
    }
    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBeautician() {
        return beautician;
    }
    public void setBeautician(String beautician) {
        this.beautician = beautician;
    }

    public String getProd_price() {
        return prod_price;
    }
    public void setProd_price(String prod_price) {
        this.prod_price = prod_price;
    }

    public String getSubtotal() {
        return subtotal;
    }
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getGst() {
        return gst;
    }
    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getTotal() {
        return total;
    }
    public void setTotal(String total) {
        this.total = total;
    }

    public String getDiscount() {
        return discount;
    }
    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDeposit() {
        return deposit;
    }
    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getRemainbalance() {return remainbalance;}
    public void setRemainbalance(String remainbalance) {this.remainbalance = remainbalance;}

    public UnpaidByIDBean(String prod_name,String qty,String brand,String beautician,String prod_price,String subtotal,String gst,
                          String total,String discount,String deposit,String remainbalance) {
        this.prod_name = prod_name;
        this.qty = qty;
        this.brand = brand;
        this.beautician = beautician;
        this.prod_price = prod_price;
        this.subtotal = subtotal;
        this.gst = gst;
        this.total = total;
        this.discount = discount;
        this.deposit = deposit;
        this.remainbalance = remainbalance;
    }


}
