package com.easyware.bean;

/**
 * Created by user on 7/24/2017.
 */

public class VideoBean {

    public String id,title,description,url;

    public VideoBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public VideoBean(String id, String title, String description,String url) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.url = url;


    }


}
