package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class ReferralBean {

    public String id;
    public String rec_no;
    public String total;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String date;

    public ReferralBean() {

    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getRec_no() {
        return rec_no;
    }

    public void setRec_no(String rec_no) {
        this.rec_no = rec_no;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }



    public ReferralBean(String id, String rec_no, String total) {
        this.id = id;
        this.rec_no = rec_no;
        this.total = total;

    }


}
