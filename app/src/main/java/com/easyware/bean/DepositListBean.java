package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class DepositListBean {

    public String id;
    public String amount;
    public String create;

    public DepositListBean() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }


    public DepositListBean(String id, String amount, String create) {
        this.id = id;
        this.amount = amount;
        this.create = create;
    }


}
