package com.easyware.bean;

/**
 * Created by user on 16-Dec-17.
 */

public class MyOrderBean {

    private String id,order_no,total,date,point_used,gst,subtotal,status,item;

    public MyOrderBean() {

    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOrder_no() {
        return order_no;
    }
    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }
    public String getTotal() {
        return total;
    }
    public void setTotal(String total) {
        this.total = total;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getPoint_used() {
        return point_used;
    }
    public void setPoint_used(String point_used) {
        this.point_used = point_used;
    }
    public String getGst() {
        return gst;
    }
    public void setGst(String gst) {
        this.gst = gst;
    }
    public String getSubtotal() {
        return subtotal;
    }
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getItem() {
        return item;
    }
    public void setItem(String item) {
        this.item = item;
    }




    public MyOrderBean(String id,String order_no,String total,String date,String point_used,String gst,String subtotal,String item){
        this.id = id;
        this.order_no = order_no;
        this.total = total;
        this.date = date;
        this.point_used = point_used;
        this.gst = gst;
        this.subtotal = subtotal;
        this.item = item;
    }

}
