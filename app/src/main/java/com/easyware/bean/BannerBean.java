package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class BannerBean {

    public String image,title,description,sort;

    public BannerBean() {

    }


    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }

    public BannerBean(String image, String title, String description) {
        this.image = image;
        this.title = title;
        this.description = description;

    }


}
