package com.easyware.bean;

/**
 * Created by user on 7/24/2017.
 */

public class DayAppointmentBean {

    public String staff,notes,date,time;

    public DayAppointmentBean() {

    }


    public String getStaff() {
        return staff;
    }
    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

    public DayAppointmentBean(String staff, String notes, String date, String time) {
        this.staff = staff;
        this.notes = notes;
        this.date = date;
        this.time = time;


    }


}
