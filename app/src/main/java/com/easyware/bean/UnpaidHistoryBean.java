package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class UnpaidHistoryBean {

    public String unpaid_id;
    public String unpaid_title;
    public String unpaid_detail;
    public String unpaid_total;

    public UnpaidHistoryBean() {

    }


    public String getUnpaid_id() {
        return unpaid_id;
    }

    public void setUnpaid_id(String unpaid_id) {
        this.unpaid_id = unpaid_id;
    }

    public String getUnpaid_title() {
        return unpaid_title;
    }

    public void setUnpaid_title(String unpaid_title) {
        this.unpaid_title = unpaid_title;
    }

    public String getUnpaid_detail() {
        return unpaid_detail;
    }

    public void setUnpaid_detail(String unpaid_detail) {
        this.unpaid_detail = unpaid_detail;
    }

    public String getUnpaid_total() {
        return unpaid_total;
    }

    public void setUnpaid_total(String unpaid_total) {
        this.unpaid_total = unpaid_total;
    }

    public UnpaidHistoryBean(String unpaid_id, String unpaid_title, String unpaid_detail, String unpaid_total) {
        this.unpaid_id = unpaid_id;
        this.unpaid_title = unpaid_title;
        this.unpaid_detail = unpaid_detail;
        this.unpaid_total = unpaid_total;
    }


}
