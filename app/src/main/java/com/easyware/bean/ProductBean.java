package com.easyware.bean;

/**
 * Created by user on 7/10/2017.
 */

public class ProductBean {
    private String id,title,description,type,status,created,category_id,prod_code,price,prom_price,qty,image,video;

    public ProductBean() {}

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }

    public String getCategory_id() {
        return category_id;
    }
    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getProd_code() {
        return prod_code;
    }
    public void setProd_code(String prod_code) {
        this.prod_code = prod_code;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getProm_price() {
        return prom_price;
    }
    public void setProm_price(String prom_price) {
        this.prom_price = prom_price;
    }

    public String getQty() {
        return qty;
    }
    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }
    public void setVideo(String video) {
        this.video = video;
    }

    public ProductBean(String id,String title,String description,String type,String status,String created,String category_id
            ,String prod_code,String price,String prom_price,String qty,String image,String video) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.type = type;
        this.status = status;
        this.created = created;
        this.category_id = category_id;
        this.prod_code = prod_code;
        this.price = price;
        this.prom_price = prom_price;
        this.qty = qty;
        this.image = image;
        this.video = video;

    }
}
