package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class Pur_Report_Bean {

    public String id,title,txn_id,price,payment_status,created;

    public Pur_Report_Bean() {

    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTxn_id() {return txn_id;}
    public void setTxn_id(String txn_id) {this.txn_id = txn_id;}

    public String getPrice() {return price;}
    public void setPrice(String price) {this.price = price;}

    public String getPayment_status() {return payment_status;}
    public void setPayment_status(String payment_status) {this.payment_status = payment_status;}

    public String getCreated() {return created;}
    public void setCreated(String created) {this.created = created;}




    public Pur_Report_Bean(String id, String title, String txn_id,String price,String payment_status,String created) {
        this.id = id;
        this.title = title;
        this.txn_id = txn_id;
        this.price = price;
        this.payment_status = payment_status;
        this.created = created;

    }


}
