package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class PointReportListBean {

    public String id,points,name,remarks,created;

    public PointReportListBean() {

    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getPoints() {
        return points;
    }
    public void setPoints(String points) {
        this.points = points;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {return remarks;}
    public void setRemarks(String remarks) {this.remarks = remarks;}

    public String getCreated() {return created;}
    public void setCreated(String created) {this.created = created;}




    public PointReportListBean(String id, String points, String name, String remarks, String created) {
        this.id = id;
        this.points = points;
        this.name = name;
        this.remarks = remarks;
        this.created = created;

    }


}
