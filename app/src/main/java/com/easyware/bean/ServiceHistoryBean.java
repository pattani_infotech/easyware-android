package com.easyware.bean;

/**
 * Created by user on 8/15/2017.
 */

public class ServiceHistoryBean {

    private String id,rec_no,title,date,usage,used;

    public boolean isLoad() {
        return isLoad;
    }

    public void setLoad(boolean load) {
        isLoad = load;
    }

    private boolean isLoad;
    public ServiceHistoryBean() {}
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getRec_no() {
        return rec_no;
    }
    public void setRec_no(String rec_no) {
        this.rec_no = rec_no;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getUsage() {
        return usage;
    }
    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getUsed() {
        return used;
    }
    public void setUsed(String used) {
        this.used = used;
    }

}
