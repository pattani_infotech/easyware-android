package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class HappyHoursBean {

    public String id,title,description,image,expiry_date,price;

    public HappyHoursBean() {

    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpiry_date() {return expiry_date;}
    public void setExpiry_date(String expiry_date) {this.expiry_date = expiry_date;}

    public String getPrice() {return price;}
    public void setPrice(String price) {this.price = price;}




    public HappyHoursBean(String id, String title, String description, String image,String price, String expiry_date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
        this.price = price;
        this.expiry_date = expiry_date;

    }

}
