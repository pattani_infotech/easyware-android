package com.easyware.bean;

/**
 * Created by user on 7/10/2017.
 */

public class CartProductBean {
    private String product_id;
    private String price;
    private String name;
    private String qty;
    private String id_default_image;

    public String getId_default_image() {

        return id_default_image;
    }

    public void setId_default_image(String id_default_image) {
        this.id_default_image = id_default_image;
    }

    public CartProductBean() {
super();
    }

    public String getQty() {
        return qty;
    }
    public void setQty(String qty) {
        this.qty = qty;
    }
    public String getId() {
        return product_id;
    }
    public void setId(String product_id) {
        this.product_id = product_id;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public CartProductBean(String product_id, String price, String name, String qty, String id_default_image) {
        this.product_id = product_id;
        this.price = price;
        this.name = name;
        this.qty = qty;
        this.id_default_image = id_default_image;

    }

   /* @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Integer.parseInt(id);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CartProductBean other = (CartProductBean) obj;
        if (id != other.id)
            return false;
        return true;
    }*/

    @Override
    public String toString() {
        /*JSONObject obj = new JSONObject();
        try {
            obj.put("product_id",product_id);
            obj.put("price",price);
            obj.put("name",name);
            obj.put("qty",qty);
            obj.put("id_default_image",id_default_image);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj.toString();*/
        return "product_id="+product_id+",price=" + price + ", name=" + name + ", qty="+qty+", id_default_image="+id_default_image+"";
        //return "Product [id=" + id + ", name=" + name + ", price=" + price + "]";
    }
}
