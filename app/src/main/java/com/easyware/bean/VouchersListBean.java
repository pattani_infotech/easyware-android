package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class VouchersListBean {

    public String id;
    public String title;
    public String description;
    public String redeem;
    public String redeem_date;

    public VouchersListBean() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRedeem() {
        return redeem;
    }
    public void setRedeem(String redeem) {
        this.redeem = redeem;
    }

    public String getRedeem_date() {
        return redeem_date;
    }
    public void setRedeem_date(String redeem_date) {
        this.redeem_date = redeem_date;
    }

    public VouchersListBean(String id, String title, String description, String redeem, String redeem_date) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.redeem = redeem;
        this.redeem_date = redeem_date;
    }


}
