package com.easyware.bean;

/**
 * Created by user on 7/22/2017.
 */

public class PromotionBean {


    public String promotion_id;
    public String promotion_img;
    public String title;
    public String description;
    public String created;
    public String branch_id;
    public String company_id;
    public String image;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    public String getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(String promotion_id) {
        this.promotion_id = promotion_id;
    }



    public String getPromotion_img() {
        return promotion_img;
    }

    public void setPromotion_img(String promotion_img) {
        this.promotion_img = promotion_img;
    }


}
