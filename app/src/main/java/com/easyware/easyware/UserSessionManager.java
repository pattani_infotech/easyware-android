package com.easyware.easyware;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.easyware.activity.Login;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vimalkumar on 3/7/2017.
 */

public class UserSessionManager {

    SharedPreferences pref;

    //Editor reference for Shared preferences
    SharedPreferences.Editor editor,editor1;

    private  static  final String IS_USER_LOGIN="IsUserLogIn";
    public static  final String REG_AS="reg_as";

    //Context
    Context _contex;

    //Shared Prefernce Mode
    int PRIVATE_MODE=0;


    private  static final String PREFER_NAME="EasywareSession";

    //PHP Field Name
    public static final String FAVORITES = "Product_Favorite";
    public static final String KEY_USERID="uid";
    public static final String KEY_USERNAME="username";
    public static final String KEY_FIRSTNAME="fname";
    public static final String KEY_LASTNAME="lname";
    public static final String KEY_EMAIL="email";
    public static final String KEY_PASSWORD="password";
    public static final String KEY_TYPE="type";
    public static final String KEY_BRANCHID="branch_id";
    public static final String KEY_COMPANYID="company_id";
    public static final String KEY_CREDIT="credit";
    public static final String KEY_POINTS="points";
    public static final String KEY_IMG="avatar";
    public static final String KEY_MOBILE="mobile";
    public static final String KEY_CITY="city";
    public static final String KEY_DOB="dob";
    public static final String KEY_BRANCH="branch_name";
    public static final String KEY_DAY="dayappnt";
    public static final String KEY_Curreny="currency";
    public static final String KEY_GST="gst";
    public static final String KEY_GSTFORMAT="gstformat";

public static ArrayList arrayList;


    // Create Constructore

    public UserSessionManager(Context context) {
        this._contex=context;
        // STORE DATA IN PREFER_NAME,PRIVATE_MODE
        pref=_contex.getSharedPreferences(PREFER_NAME,PRIVATE_MODE);
        editor=pref.edit();
        editor1=pref.edit();
    }
    public void createUserLoginSession( String id, String user_name, String f_name, String l_name, String email,String pass,String type, String branch_id,
                                        String comp_id, String credi,String pont,String img,String mobile,String city,String dob,String branch,String day,
                                        String currency,String gst,String gstformat) {

        Log.e("createUserLoginSession",gstformat+" ");
        editor.putBoolean(IS_USER_LOGIN,true);


        // Store All Data in Preference
        editor.putString(KEY_USERID,id);
        editor.putString(KEY_USERNAME,user_name);
        editor.putString(KEY_FIRSTNAME,f_name);
        editor.putString(KEY_LASTNAME,l_name);
        editor.putString(KEY_EMAIL,email);
        editor.putString(KEY_PASSWORD,pass);
        editor.putString(KEY_TYPE, type);
        editor.putString(KEY_BRANCHID,branch_id);
        editor.putString(KEY_COMPANYID,comp_id);
        editor.putString(KEY_CREDIT,credi);
        editor.putString(KEY_POINTS,pont);
        editor.putString(KEY_IMG,img);
        editor.putString(KEY_MOBILE,mobile);
        editor.putString(KEY_CITY,city);
        editor.putString(KEY_DOB,dob);
        editor.putString(KEY_BRANCH,branch);
        editor.putString(KEY_DAY,day);
        editor.putString(KEY_Curreny,currency);
        editor.putString(KEY_GST,gst);
        editor.putString(KEY_GSTFORMAT,gstformat);
        editor.commit();
    }
    // Data Check and Store
    public HashMap<String,String>getUserDetail() {
        HashMap<String,String> user=new HashMap<String, String>();

        user.put(KEY_USERID,pref.getString(KEY_USERID,null));
        user.put(KEY_USERNAME,pref.getString(KEY_USERNAME,null));
        user.put(KEY_FIRSTNAME,pref.getString(KEY_FIRSTNAME,null));
        user.put(KEY_LASTNAME,pref.getString(KEY_LASTNAME,null));
        user.put(KEY_EMAIL,pref.getString(KEY_EMAIL,null));
        user.put(KEY_PASSWORD,pref.getString(KEY_PASSWORD,null));
        user.put(KEY_TYPE,pref.getString(KEY_TYPE,null));
        user.put(KEY_BRANCHID,pref.getString(KEY_BRANCHID,null));
        user.put(KEY_COMPANYID,pref.getString(KEY_COMPANYID,null));
        user.put(KEY_CREDIT,pref.getString(KEY_CREDIT,null));
        user.put(KEY_POINTS,pref.getString(KEY_POINTS,null));
        user.put(KEY_IMG,pref.getString(KEY_IMG,null));
        user.put(KEY_MOBILE,pref.getString(KEY_MOBILE,null));
        user.put(KEY_CITY,pref.getString(KEY_CITY,null));
        user.put(KEY_DOB,pref.getString(KEY_DOB,null));
        user.put(KEY_BRANCH,pref.getString(KEY_BRANCH,null));
        user.put(KEY_DAY,pref.getString(KEY_DAY,null));
        user.put(KEY_Curreny,pref.getString(KEY_Curreny,null));
        user.put(KEY_GST,pref.getString(KEY_GST,null));
        user.put(KEY_GSTFORMAT,pref.getString(KEY_GSTFORMAT,null));
            return user;
    }
    // Log Out

    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent intent=new Intent(_contex, Login.class);
        //close all activity
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // add new flag to start activity
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _contex.startActivity(intent);
    }
    // check whether user login or not
    public boolean isUserLogin()
    {
        return pref.getBoolean(IS_USER_LOGIN,false);
    }


    /*public void saveFavorites(Context context, List<CartProductBean> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor1;

        settings = context.getSharedPreferences(PREFER_NAME,
                Context.MODE_PRIVATE);
        editor1 = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor1.commit();
    }
    public void addFavorite(Context context, CartProductBean bean) {
        List<CartProductBean> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<CartProductBean>();
        favorites.add(bean);
        saveFavorites(context, favorites);
    }
    public ArrayList<CartProductBean> getFavorites(Context context) {
        SharedPreferences settings;
        List<CartProductBean> favorites;

        settings = context.getSharedPreferences(PREFER_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            CartProductBean[] favoriteItems = gson.fromJson(jsonFavorites,
                    CartProductBean[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<CartProductBean>(favorites);
        } else
            return null;
        return (ArrayList<CartProductBean>) favorites;
    }*/
}
