package com.easyware.easyware;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by user on 12-Oct-17.
 */

public class BadgePref {

    SharedPreferences pref;

    //Editor reference for Shared preferences
    SharedPreferences.Editor editor;


    //Context
    Context _contex;

    //Shared Prefernce Mode
    int PRIVATE_MODE=0;




    //PHP Field Name



    private static final String PREFER_NAME = "EasywareBadge";

    //PHP Field Name

    public static final String KEY_BADGE = "badge";


    public BadgePref(Context context) {

        this._contex = context;
        // STORE DATA IN PREFER_NAME,PRIVATE_MODE
        pref = _contex.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createUserLoginSession(int count) {

        editor.putInt(KEY_BADGE,count);
        editor.commit();
    }

    public int returnCount()
    {
        return pref.getInt(KEY_BADGE,0);
    }

}
