package com.easyware.easyware;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.easyware.bean.CartProductBean;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



/**
 * Created by user on 09-Mar-18.
 */

public class SharedPreference {
   /* public static final String PREFS_NAME = "PRODUCT_LIST";
    public static final String FAVORITES = "Product_cart";
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    public SharedPreference() {
        super();
    }
    public void saveFavorites(Context context, List<CartProductBean> favorites) {

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);
Log.e("jsonFavorites",jsonFavorites.toString()+"");
        editor.putString(FAVORITES, jsonFavorites);
        editor.commit();
    }
    public void addFavorite(Context context, CartProductBean bean) {
        Log.e("addFavorite",bean.toString()+"");
        List<CartProductBean> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<CartProductBean>();
        favorites.add(bean);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, CartProductBean product) {

        Log.e("removeFavorite",product.toString()+"");
        ArrayList<CartProductBean> favorites = getFavorites(context);
        Log.e("favorites 123 ",favorites.toString()+"");
        System.out.println("Removing favorite");
        System.out.println("new favs" + favorites);
        if (favorites != null) {
            System.out.println("object from click" + product);

            favorites.remove(product);
            Log.e("remove 123 ",favorites.toString()+" 123");
            System.out.println(context);

            saveFavorites(context, favorites);

        }
    }
    public ArrayList<CartProductBean> getFavorites(Context context) {
        SharedPreferences settings;
        List<CartProductBean> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            CartProductBean[] favoriteItems = gson.fromJson(jsonFavorites,
                    CartProductBean[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<CartProductBean>(favorites);
        } else {
            return null;
        }
        return (ArrayList<CartProductBean>) favorites;
    }*/

    private Context context;
    public static final String PREFS_NAME = "Easyware";
    public static final String CARTPRODUCTS = "Cartproducts";

    public  SharedPreference(Context context){

        super();
    }


    public void SaveLocalStore(Context context, List<CartProductBean> cpb){
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonCPB = gson.toJson(cpb);
        Log.e("jsonCPB",jsonCPB);
        editor.putString(CARTPRODUCTS, jsonCPB);
        editor.apply();

        /*JSONObject object=new JSONObject();
        try {
            object.put("data",cpb);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("object",object.toString());*/
    }
    public ArrayList<CartProductBean> loadProduct(Context context) {
        SharedPreferences settings;
        List<CartProductBean> cartProduct;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (settings.contains(CARTPRODUCTS)) {
            String jsonFavorites = settings.getString(CARTPRODUCTS, null);
           // Log.e("jsonFavorites",jsonFavorites+"");
            Gson gson = new Gson();
            CartProductBean[] product = gson.fromJson(jsonFavorites,CartProductBean[].class);
            cartProduct = Arrays.asList(product);
            cartProduct = new ArrayList<>(cartProduct);
        } else
            return null;
        return (ArrayList<CartProductBean>) cartProduct;
    }

    /*public void addFavorite(Context context, CartProductBean vocabCatModel) {
        List<CartProductBean> favorites = loadFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<>();
        favorites.add(vocabCatModel);
        storeFavorites(context, favorites);
    }

    public void removeFavorite(Context context, CartProductBean vocabCatModel) {
        Log.e("vocabCatModel",vocabCatModel.toString());
        ArrayList<CartProductBean> favorites = loadFavorites(context);
        if (favorites != null) {
            Log.e("size ",favorites.size()+"");
            favorites.remove(vocabCatModel);
            Log.e("after size ",favorites.size()+"");
            Log.e("2121 ",favorites.toString()+"");

            storeFavorites(context, favorites);

        }
    }*/


}
