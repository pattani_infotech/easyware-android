package com.easyware.parcer;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Pattani InfoTech & Fitzroya Solutions on 23-07-2016.
 */
public class JsonParser {


static URL url;
static HttpURLConnection httpURLConnection;


static URLConnection conn;

    public JsonParser()
    {

    }

static String status;
    public static String GetJsonFromURL(String data,String weburl) {
Log.e("GetJsonFromURL",weburl+"?"+data);
        BufferedReader reader=null;
        try {

            url=new URL(weburl);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(data);
            wr.flush();
            // Get the server response

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;
            // Read Server Response
            while((line = reader.readLine()) != null)
            {
                // Append server response in string
                sb.append(line + "\n");
            }
            status = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return  status;
    }

    public static String uploadFile(String url1,String sourceFileUri)
    {

        String data = null;
        StringBuilder sb = new StringBuilder();
        String line = null;
        BufferedReader reader = null;
        int code = 0;
        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            url = new URL(url1);
           // Log.e("uploadFile uploadFile",url.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("avatar", fileName);
            //conn.setRequestProperty("U_id",user_id);
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"avatar\";filename=\""
                    + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            //Log.e("uploadFile","bytesRead dfgdg");
            while (bytesRead > 0) {
               // Log.e("uploadFile",bytesRead+"");
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "");
                //Log.e("uploadFile",sb.toString());
            }
            data = sb.toString();
            //Log.e("fileName",data);
            code = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
         //   Log.e("fileName 1",serverResponseMessage);
           // Log.e("File uploading done","Done");
            fileInputStream.close();
            dos.flush();
            dos.close();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return data;
    }




    public static String uploadFileNew(String url1,String sourceFileUri)
    {
       // Log.e("abc-file path", url1);
        String data = null;
        StringBuilder sb = new StringBuilder();
        String line = null;
        BufferedReader reader = null;
        int code = 0;
        String fileName = sourceFileUri;
       // Log.e("uploadFile uploadFile",fileName);
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        if(sourceFile.exists())
        {
           // Log.e("sourceFile","yes");
        }else{
            //Log.e("sourceFile","No");
        }
        try {
           // Log.e("test",url1);
            FileInputStream fileInputStream = new FileInputStream(sourceFile);

           //' Log.e("test1",url.toString());
            url = new URL(url1);
            //Log.e("test2",url.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("img", fileName);
            //conn.setRequestProperty("U_id",user_id);
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"img\";filename=\""
                    + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            //Log.e("uploadFile","bytesRead dfgdg");
            while (bytesRead > 0) {
                //Log.e("uploadFile",bytesRead+"");
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "");
                //Log.e("uploadFile",sb.toString());
            }
            data = sb.toString();
            //Log.e("fileName",data);
            code = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
           // Log.e("fileName 1",serverResponseMessage);
            //Log.e("File uploading done","Done");
            fileInputStream.close();
            dos.flush();
            dos.close();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return data;
    }


    public static String CommentFileNew(String url1,String sourceFileUri)
    {
        //Log.e("abc-file path", url1);
        String data = null;
        StringBuilder sb = new StringBuilder();
        String line = null;
        BufferedReader reader = null;
        int code = 0;
        String fileName = sourceFileUri;
        //Log.e("uploadFile uploadFile",fileName);
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        if(sourceFile.exists())
        {
            //Log.e("sourceFile","yes");
        }else{
            //Log.e("sourceFile","No");
        }
        try {
           // Log.e("test",url1);
            FileInputStream fileInputStream = new FileInputStream(sourceFile);

           // Log.e("test1",url.toString());
            url = new URL(url1);
           // Log.e("test2",url.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("image", fileName);
            //conn.setRequestProperty("U_id",user_id);
            dos = new DataOutputStream(conn.getOutputStream());
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                    + fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            //Log.e("uploadFile","bytesRead dfgdg");
            while (bytesRead > 0) {
               // Log.e("uploadFile",bytesRead+"");
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            }
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // Responses from the server (code and message)
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "");
                //Log.e("uploadFile",sb.toString());
            }
            data = sb.toString();
           // Log.e("fileName",data);
            code = conn.getResponseCode();
            String serverResponseMessage = conn.getResponseMessage();
           // Log.e("fileName 1",serverResponseMessage);
           // Log.e("File uploading done","Done");
            fileInputStream.close();
            dos.flush();
            dos.close();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return data;
    }

}
